@extends('layouts.app')


@section('content')

<div class="grid-post-page">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="create-post">
          <h2>
            @lang('post.post-header')
          </h2>
          @lang('post.post-hint-title', [
            'title' => $category->title,
            'body' => $category->body,
          ])
          @lang('post.post-hint-body', [
            'ethices-url' => route('ethices')
          ])

          {{-- @include('posts.includes.post-box') --}}

        </div>
      </div>
    </div>
  </div>
</div>
@endsection

