<?php

namespace Thaiissue\Enums;

class Status
{
    const OPEN  = 'open';
    const LOCKED  = 'locked';
    const ARCHIVED  = 'archived';

    public static function all()
    {
        $array = [
            self::OPEN,
            self::LOCKED,
            self::ARCHIVED,
        ];
        return array_combine(
            range(1, count($array)),
            array_values($array)
        );
    }

    public static function options()
    {
        return [
            self::OPEN => self::OPEN,
            self::LOCKED => self::LOCKED,
            self::ARCHIVED => self::ARCHIVED,
        ];
    }
}
