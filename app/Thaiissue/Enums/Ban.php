<?php

namespace Thaiissue\Enums;

class Ban
{
    const LEVEL1  = '1'; // lvl1 ban for 30 minutes
    const LEVEL2  = '2'; // lvl2 ban for 2 hours
    const LEVEL3  = '3'; // lvl3 ban for 24 hours
    const LEVEL4  = '4'; // lvl4 ban for 3 days

    public static function all()
    {
        $array = [
            self::LEVEL1,
            self::LEVEL2,
            self::LEVEL3,
            self::LEVEL4,
        ];
        return array_combine(
            range(1, count($array)),
            array_values($array)
        );
    }

    public static function options()
    {
        return [
            self::LEVEL1 => self::LEVEL1,
            self::LEVEL2 => self::LEVEL2,
            self::LEVEL3 => self::LEVEL3,
            self::LEVEL4 => self::LEVEL4,
        ];
    }
}
