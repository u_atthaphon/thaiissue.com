<?php

namespace Thaiissue\Enums;

class Provider
{
    const EMAIL  = 'email';
    const GOOGLE  = 'google';
    const FACEBOOK  = 'facebook';


    public static function all()
    {
        $array = [
            self::EMAIL,
            self::GOOGLE,
            self::FACEBOOK,
        ];
        return array_combine(
            range(1, count($array)),
            array_values($array)
        );
    }

    public static function options()
    {
        return [
            self::EMAIL => self::EMAIL,
            self::GOOGLE => self::GOOGLE,
            self::FACEBOOK => self::FACEBOOK,
        ];
    }
}
