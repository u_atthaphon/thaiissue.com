<?php

namespace Thaiissue\Enums;

class Role
{
    const ADMIN  = 'admin';
    const USER   = 'user';
    const MASTER = 'master';

    public static function all()
    {
        $array = [
            self::ADMIN,
            self::USER,
            self::MASTER,
        ];
        return array_combine(
            range(1, count($array)),
            array_values($array)
        );
    }

    public static function options()
    {
        return [
            self::ADMIN => self::ADMIN,
            self::USER => self::USER,
            self::MASTER => self::MASTER,
        ];
    }
}
