<?php

namespace Thaiissue\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPassword extends FormRequest
{
    protected $redirect = 'users/me';

    public function __construct()
    {
        //
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required|old_password:'.\Auth::user()->password,
            'new_password' => 'required|confirmed|min:6|different:current_password',
        ];
    }

    public function messages()
    {
        return [
            'current_password.required' => 'กรุณากรอกรหัสผ่านเดิม',
            'current_password.old_password' => 'รหัสผ่านเดิมของคุณไม่ถูกต้อง',
            'new_password.required' => 'กรุณากรอกรหัสผ่านใหม่',
            'new_password.confirmed' => 'ยืนยันรหัสผ่านไม่ตรงกับรหัสผ่านใหม่',
            'new_password.different' => 'กรุณกรอกรหัสผ่านใหม่ที่ไม่เหมือนกับรหัสผ่านปัจจุบัน',
            'new_password.min' => [
                'string' => 'รหัสผ่านต้องไม่น้อยกว่า :min ตัวอักษร',
            ],
        ];
    }
}
