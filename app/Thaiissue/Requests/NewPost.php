<?php

namespace Thaiissue\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewPost extends FormRequest
{
    protected $redirect = '';

    public function __construct()
    {
        parent::__construct();
        $this->redirect = \URL::previous()."#postTitle";
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoryId'           => 'required|integer',
            'postTitle'            => 'required|max:255',
            'postBody'             => 'nullable|max:65535',
            'image'                => 'sometimes|mimes:jpeg,jpg,png,gif|max:5120',
        ];
    }

    public function messages()
    {
        return [
            'postTitle.required' => 'กรุณากรอกหัวข้อ',
            'postTitle.max'      => [
                'string' => 'หัวข้อยาวเกินไป ขอให้กระชับกว่านี้',
            ],
            'postBody.max'       => [
                'string' => 'อนุญาติให้เขียนเนื้อหาของประเด็นได้ไม่เกิน :max ตัวอักษร',
            ],
            'image.mimes'        => 'อนุญาติให้อัพโหลดได้เฉพาะไฟล์รูปที่มีนามสกุล :values เท่านั้น',
            'image.max'          => [
                'file' => 'กรุณาเลือกรูปที่มีขนาดไม่เกิน 5 MB',
            ],
        ];
    }
}
