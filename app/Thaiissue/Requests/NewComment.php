<?php

namespace Thaiissue\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewComment extends FormRequest
{
    protected $redirect = '';

    public function __construct()
    {
        $this->redirect = \URL::previous()."#commentTitle";
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'categoryId' => 'required|integer',
            'postId' => 'required|integer',
            'body' => 'required_without:image|max:65535',
            'image' => 'required_without:body|mimes:jpeg,jpg,png,gif|max:5120',
        ];
    }

    public function messages()
    {
        return [
            'body.required' => 'กรุณากรอกคอมเมนต์',
            'body.max' => [
                'string' => 'อนุญาติให้เขียนคอมเมนต์ได้ไม่เกิน :max ตัวอักษร',
            ],
            'body.required_without' => 'กรุณคอมเมนต์ข้อความถ้าคุณไม่ต้องการเพิ่มรูป',
            'image.required_without' => 'กรุณคอมเมนต์ด้วยรูปภาพถ้าคุณไม่ต้องการเพิ่มข้อความ',
            'image.mimes' => 'อนุญาติให้อัพโหลดได้เฉพาะไฟล์รูปที่มีนามสกุล :values เท่านั้น',
            'image.max' => [
                'file' => 'กรุณาเลือกรูปที่มีขนาดไม่เกิน 5 MB',
            ],
        ];
    }
}
