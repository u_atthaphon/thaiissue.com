<?php

namespace Thaiissue\Newsapi;

use Illuminate\Support\ServiceProvider;

class NewsapiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Newsapi', Newsapi::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['Newsapi'];
    }
}
