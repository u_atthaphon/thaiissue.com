<?php

namespace Thaiissue\Enums;

class Newsapi
{
    const MAIN_URL = 'https://newsapi.org/v2/top-headlines';

    const BUSINESS = 'business';
    const ENTERTAINMENT = 'entertainment';
    const HEALTH = 'health';
    const SCIENCE = 'science';
    const SPORTS = 'sports';
    const TECHNOLOGY = 'technology';


    public static function all()
    {
        $array = [
            self::BUSINESS,
            self::ENTERTAINMENT,
            self::HEALTH,
            self::SCIENCE,
            self::SPORTS,
            self::TECHNOLOGY,
        ];
        return array_combine(
            range(1, count($array)),
            array_values($array)
        );
    }

    public static function options()
    {
        return [
            self::BUSINESS => self::BUSINESS,
            self::ENTERTAINMENT => self::ENTERTAINMENT,
            self::HEALTH => self::HEALTH,
            self::SCIENCE => self::SCIENCE,
            self::SPORTS => self::SPORTS,
            self::TECHNOLOGY => self::TECHNOLOGY,
        ];
    }
}
