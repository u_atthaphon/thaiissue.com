<?php

namespace Thaiissue\Newsapi\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * This is the authorizer facade class.
 */
class Newsapi extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Newsapi';
    }
}
