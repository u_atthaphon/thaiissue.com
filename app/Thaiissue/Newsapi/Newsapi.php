<?php

namespace Thaiissue\Newsapi;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Thaiissue\Models\NewsLink;
use Thaiissue\Enums\Newsapi as NewsapiEnum;
use Thaiissue\QueryBuilder\Facades\QueryBuilder;

class Newsapi
{
    protected $apiKey = '51d1abf1a73d4df9a83eef137ef090a4';
    protected $country = 'country=th';
    protected $category = 'category=';
    protected $callable = [];
    protected $client;

    public function __construct()
    {
        $this->callable = NewsapiEnum::all();
        $this->client = new Client([
            'base_uri' => NewsapiEnum::MAIN_URL,
            'verify' => true
        ]);
    }

    public function fetchData($category = null)
    {
        $path = storage_path().'/app/public/newsapi_content.json';
        return$json = json_decode(file_get_contents($path));


        if ($category == null || !in_array($category, $this->callable)) {
            $message = "Not allowed: category {$category} to fetchData";
            \Log::warning($message);
            throw new \Exception($message, 403);
        }

        $headers = [
            'headers' => [
                'Authorization' => "Basic {$this->apiKey}",
            ],
        ];
        $queryString = "?{$this->country}&{$this->category}{$category}";
        $response = $this->client->get($queryString, $headers);

        return json_decode($response->getBody()->getContents());
    }

    public function fetchBusiness()
    {
        $category = NewsapiEnum::BUSINESS;
        $data = $this->fetchData($category);
        if ($data->status != "ok") {
            $message = "Can not fetch category {$category}!!";
            \Log::warning($message);
            throw new \Exception($message, 403);
        }
        $articles = $this->onlyNewArticles($data->articles);
        $inputs = $this->prepareData($articles, $category);
        $result = null;
        if (!empty(array_filter($inputs))) {
            $result = QueryBuilder::table('news_links')->insertOrUpdate($inputs);
        }

        // todo: save to news_links table then upload images
    }

    protected function onlyNewArticles($articles)
    {
        $articles = collect($articles)->each(function ($item, $key) {
            $item->url = $this->sortQueryStringParams($item->url);
        });
        $urls = $articles->pluck('url')->toArray();
        $newsLinks = NewsLink::InUrls($urls)->get();
        $articles = $articles->reject(function ($item) use ($newsLinks) {
            return in_array($item->url, $newsLinks->pluck('url')->toArray());
        });
        return $articles;
    }

    protected function prepareData($articles, $category)
    {
        $result = [];
        foreach ($articles as $article) {
            $result[] = [
                'category'     => $category,
                'domain'       => $article->source->name,
                'author'       => $article->author,
                'title'        => $article->title,
                'description'  => $article->description,
                'url'          => $this->sortQueryStringParams($article->url),
                'image'        => $article->urlToImage,
                'content'      => $this->excludeBrackets($article->content),
                'added_by'           => 'newsapi',
                'published_at' => Carbon::parse($article->publishedAt, 'Asia/Bangkok'),
                'created_at'   => Carbon::now(),
                'updated_at'   => Carbon::now(),
            ];
        }

        return $result;
    }

    // Note: Don't Check http or https yet
    protected function sortQueryStringParams($fullUrl)
    {
        $urlComponent = parse_url($fullUrl);
        $params = [];
        if (isset($urlComponent['query'])) {
            parse_str($urlComponent['query'], $params);
            //Sorting query params by key (acts by reference)
            ksort($params);
        }
        //Transforming the query array to query string
        $queryString = http_build_query($params);
        $scheme = isset($urlComponent['scheme']) ? $urlComponent['scheme'] : '';
        $host = isset($urlComponent['host']) ? $urlComponent['host'] : '';
        $path = isset($urlComponent['path']) ? $urlComponent['path'] : '';

        $url = "{$scheme}://{$host}{$path}";
        if (!empty($queryString)) {
            $url .= "?{$queryString}";
        }
        return $url;
    }

    protected function uploadImages($imageUrl)
    {
        if (empty($imageUrl)) {
            return null;
        }

        ini_set('memory_limit', '1500M');
        $filenaemUUID = Str::uuid();
        $imgName = $filenaemUUID.'.jpg';
        $img = \Image::make($imageUrl);
        $width = 800;
        if ($img->width() > $img->height()) {
            $width = 1200;
        }
        $img->widen($width, function ($constraint) {
            $constraint->upsize();
        });
        $filename = "news/{$imgName}";
        \Storage::put($filename, (string) $img->encode('jpg', 75), 'public');
        return $filename;
    }

    protected function excludeBrackets($text)
    {
        $pattern = '/\[+[^\]]*\]/';
        return preg_replace($pattern, '', $text);
    }

    protected function putImagesToDOSpace($inputs)
    {
        // foreach($inputs)
        // 'image'        => $this->uploadImages($article->urlToImage),
    }
}
