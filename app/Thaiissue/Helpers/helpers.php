<?php

// Surpass cloud flare ip if it set up
// $_SERVER["HTTP_CF_CONNECTING_IP"] real visitor ip address, this is what you want
// $_SERVER["HTTP_CF_IPCOUNTRY"] country of visitor
// $_SERVER["HTTP_CF_RAY"] see description here
// $_SERVER["HTTP_CF_VISITOR"] this can help you know if its http or https
if (!function_exists('real_ip')) {
    function real_ip()
    {
        $realIp = $_SERVER['REMOTE_ADDR'];
        // Check Cloudflare
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $realIp = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        return $realIp;
    }
}

if (!function_exists('is_admin')) {
    function is_admin($user)
    {
        if ($user == null) {
            return false;
        }

        return $user->inRole('admin');
    }
}

if (!function_exists('client_token')) {
    function client_token()
    {
        return \Session::get('client_access_token');
    }
}

if (!function_exists('fake_ip')) {
    function fake_ip()
    {
        $faker = app(Faker\Generator::class);
        return $faker->ipv4;
    }
}

if (!function_exists('thaiissue_get_ip')) {
    function thaiissue_get_ip()
    {
        if (!\Auth::check()) {
            return htmlspecialchars(real_ip());
            ;
        }

        if (\Auth::user()->inRole(\Thaiissue\Enums\Role::MASTER)) {
            return htmlspecialchars(fake_ip());
            ;
        }

        return htmlspecialchars(real_ip());
        ;
    }
}

if (!function_exists('ban_expired_by_level')) {
    function ban_expired_by_level($level)
    {
        $expiredAt = null;
        switch ($level) {
            case 1:
                $expiredAt = \Carbon\Carbon::now()->addMinutes(30);
                break;
            case 2:
                $expiredAt = \Carbon\Carbon::now()->addHours(2);
                break;
            case 3:
                $expiredAt = \Carbon\Carbon::now()->addHours(24);
                break;
            case 4:
                $expiredAt = \Carbon\Carbon::now()->addDays(30);
                break;
        }

        return $expiredAt;
    }
}


/*
    Use with artesaos/seotools
 */
if (!function_exists('meta_tags')) {
    function meta_tags($metaTags = [])
    {
        // Grap this $metaTages to your funciton
        // $metaTags = [
        //     'title' => '',
        //     'description' => '',
        //     'keywords' => [],
        //     'url' => '',
        //     'property' => '',
        //     'image' => '',
        //     'images' => [],
        //     'twitter_site' => '',
        //     'prev' => '',
        //     'next' => '',
        // ];

        // SEO
        if (isset($metaTags['title']) && !empty($metaTags['title'])) {
            \SEOMeta::setTitle($metaTags['title'], false);
        }
        if (isset($metaTags['description']) && !empty($metaTags['description'])) {
            \SEOMeta::setDescription($metaTags['description']);
        }
        if (isset($metaTags['keywords']) && !empty(array_filter($metaTags['keywords']))) {
            \SEOMeta::setKeywords($metaTags['keywords']);
        }
        if (isset($metaTags['prev']) && !empty($metaTags['prev'])) {
            \SEOMeta::setPrev($metaTags['prev']);
        }
        if (isset($metaTags['next']) && !empty($metaTags['next'])) {
            \SEOMeta::setNext($metaTags['next']);
        }

        // OPENGRAPH
        if (isset($metaTags['title']) && !empty($metaTags['title'])) {
            \OpenGraph::setTitle($metaTags['title']);
        }
        if (isset($metaTags['description']) && !empty($metaTags['description'])) {
            \OpenGraph::setDescription($metaTags['description']);
        }
        if (isset($metaTags['url']) && !empty($metaTags['url'])) {
            \OpenGraph::setUrl($metaTags['url']);
        }
        if (isset($metaTags['image']) && !empty($metaTags['image'])) {
            \OpenGraph::addImage($metaTags['image']);
        }
        if (isset($metaTags['images']) && !empty(array_filter($metaTags['images']))) {
            \OpenGraph::addImages($metaTags['images']);
        }

        // TWITTER
        if (isset($metaTags['title']) && !empty($metaTags['title'])) {
            \Twitter::setTitle($metaTags['title']);
        }
        if (isset($metaTags['description']) && !empty($metaTags['description'])) {
            \Twitter::setDescription($metaTags['description']);
        }
        if (isset($metaTags['url']) && !empty($metaTags['url'])) {
            \Twitter::setUrl($metaTags['url']);
        }
        if (isset($metaTags['image']) && !empty($metaTags['image'])) {
            \Twitter::setImage($metaTags['image']);
        }
        if (isset($metaTags['images']) && !empty(array_filter($metaTags['images']))) {
            \Twitter::setImages($metaTags['images']);
        }
        if (isset($metaTags['twitter_site']) && !empty($metaTags['twitter_site'])) {
            \Twitter::setSite($metaTags['twitter_site']);
        }
        if (isset($metaTags['twitter_Large_image']) && $metaTags['twitter_Large_image'] == true) {
            \Twitter::setType('summary_large_image');
        } else {
            \Twitter::setType('summary');
        }
    }

    if (!function_exists('is_animated_gif')) {
        function is_animated_gif($filename)
        {
            $raw = @file_get_contents($filename);

            $offset = 0;
            $frames = 0;
            while ($frames < 2) {
                $where1 = strpos($raw, "\x00\x21\xF9\x04", $offset);
                if ($where1 === false) {
                    break;
                } else {
                    $offset = $where1 + 1;
                    $where2 = strpos($raw, "\x00\x2C", $offset);
                    if ($where2 === false) {
                        break;
                    } else {
                        if ($where1 + 8 == $where2) {
                            $frames ++;
                        }
                        $offset = $where2 + 1;
                    }
                }
            }

            return $frames > 1;
        }
    }
}

if (!function_exists('only_number')) {
    function only_number($value)
    {
        return preg_replace('/[^0-9]/', '', $value);
    }
}
