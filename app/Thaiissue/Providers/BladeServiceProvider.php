<?php

namespace Thaiissue\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->extendEnv();
        $this->extendShelves();
        $this->extendShelvesMultipleLine();
        $this->extendShelvesWithImage();
        $this->extendShelvesWithLeftImage();
        $this->extendShelvesNavPill();
    }

    // example
    private function extendDatetime()
    {
        Blade::directive('datetime', function ($expression) {
            return "<?php echo (expression)->format('d/m/Y H:i')";
        });
    }

    private function extendEnv()
    {
        Blade::if('env', function ($environment) {
            return app()->environment($environment);
        });
    }

    private function extendShelves()
    {
        Blade::directive('shelves', function ($count) {
            if (!isset($count) || empty($count)) {
                $count = 1;
            }
            $shelfSkeleton = '<div class="shelf-skeleton">
                <div class="shelf-card">
                  <div class="shelf-title w-25"></div>
                  <div class="shelf-content"></div>
                </div>
              </div>';
            $shelfSkeletons = '';
            for ($i = 0; $i < $count; $i++) {
                $shelfSkeletons .= $shelfSkeleton;
                if ($i < $count - 1) {
                    $shelfSkeletons .= '<hr>';
                }
            }
            $shelves = "<div class='shelves'>{$shelfSkeletons}</div>";

            return $shelves;
        });
    }

    private function extendShelvesMultipleLine()
    {
        Blade::directive('shelvesMultipleLine', function ($count) {
            if (!isset($count) || empty($count)) {
                $count = 1;
            }
            $shelfSkeleton = '<div class="shelf-content"></div>';
            $shelfSkeletons = '';
            for ($i = 0; $i < $count; $i++) {
                $shelfSkeletons .= $shelfSkeleton;
            }
            $shelves = "<div class='shelves'><div class='shelf-skeleton'><div class='shelf-card'><div class='shelf-title w-25'></div>{$shelfSkeletons}</div></div></div>";

            return $shelves;
        });
    }

    private function extendShelvesWithImage()
    {
        Blade::directive('shelvesWithImage', function ($count) {
            if (!isset($count) || empty($count)) {
                $count = 1;
            }
            $shelfSkeleton = '<div class="shelf-skeleton">
                <div class="shelf-with-image">
                  <div class="shelf-card">
                    <div class="shelf-title"></div>
                    <div class="shelf-content"></div>
                    <div class="shelf-content"></div>
                  </div>
                  <div class="shelf-image"></div>
                </div>
              </div>';
            $shelfSkeletons = '';
            for ($i = 0; $i < $count; $i++) {
                $shelfSkeletons .= $shelfSkeleton;
                if ($i < $count - 1) {
                    $shelfSkeletons .= '<hr>';
                }
            }
            $shelves = "<div class='shelves'>{$shelfSkeletons}</div>";

            return $shelves;
        });
    }

    private function extendShelvesWithLeftImage()
    {
        Blade::directive('shelvesWithLeftImage', function ($count) {
            if (!isset($count) || empty($count)) {
                $count = 1;
            }
            $shelfSkeleton = '<div class="shelf-skeleton">
                <div class="shelf-with-image">
                  <div class="shelf-image-left"></div>
                  <div class="shelf-card">
                    <div class="shelf-title"></div>
                    <div class="shelf-content"></div>
                    <div class="shelf-content"></div>
                  </div>
                </div>
              </div>';
            $shelfSkeletons = '';
            for ($i = 0; $i < $count; $i++) {
                $shelfSkeletons .= $shelfSkeleton;
                if ($i < $count - 1) {
                    $shelfSkeletons .= '<hr>';
                }
            }
            $shelves = "<div class='shelves'>{$shelfSkeletons}</div>";

            return $shelves;
        });
    }

    private function extendShelvesNavPill()
    {
        Blade::directive('shelvesNavPill', function ($count) {
            if (!isset($count) || empty($count)) {
                $count = 1;
            }

            $shelfSkeleton = '<div class="shelf-nav-pill"></div>';
            $shelfSkeletons = '';
            for ($i = 0; $i < $count; $i++) {
                $shelfSkeletons .= $shelfSkeleton;
            }
            $shelves = "<div class='shelves'>
                <div class='shelf-skeleton'>
                    <div class='shelf-nav'>
                        {$shelfSkeletons}
                    </div>
                </div>
            </div>";

            return $shelves;
        });
    }
}
