<?php

namespace Thaiissue\QueryBuilder;

use Illuminate\Support\Facades\DB;

class QueryBuilder
{
    protected $connection;
    protected $table;

    public function __construct()
    {
        $this->connection = DB::connection(env('DB_CONNECTION', 'mysql'));
    }

    public function connection($connection)
    {
        $this->connection = DB::connection($connection);
        return $this;
    }

    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    /**
    * Mass (bulk) insert or update on duplicate for Laravel 4/5
    *
    * insertOrUpdate([
    *   ['id'=>1,'value'=>10],
    *   ['id'=>2,'value'=>60]
    * ]);
    *
    * @param array $rows
    */
    public function insertOrUpdate(array $rows)
    {
        $table = $this->table;
        $first = reset($rows);
        $columns = implode(
            ',',
            array_map(function ($value) {
                return "$value";
            }, array_keys($first))
        );
        $values = implode(
            ',',
            array_map(function ($row) {
                return '('.implode(
                    ',',
                    array_map(function ($value) {
                        return '"'.str_replace('"', '""', $value).'"';
                    }, $row)
                ).')';
            }, $rows)
        );
        $updates = implode(
            ',',
            array_map(function ($value) {
                return "$value = VALUES($value)";
            }, array_keys($first))
        );
        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values} ON DUPLICATE KEY UPDATE {$updates}";

        return $this->connection->statement($sql);
    }
}
