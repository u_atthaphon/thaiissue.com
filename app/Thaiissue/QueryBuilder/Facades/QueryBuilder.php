<?php

namespace Thaiissue\QueryBuilder\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * This is the authorizer facade class.
 */
class QueryBuilder extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'QueryBuilder';
    }
}
