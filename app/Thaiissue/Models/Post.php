<?php

namespace Thaiissue\Models;

use Thaiissue\Enums\Status as StatusEnum;
use Thaiissue\Presenters\Post\UrlPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Thaiissue\Presenters\Post\TransformPresenter;

class Post extends MyModel
{
    use AccessorsDateTrait, FavoriteableTrait, SoftDeletes;

    public $fillable = [
        'category_id', 'user_id', 'title', 'body',
        'thumbnail', 'image', 'nsfw',
        'active', 'meta', 'ip', 'hash_ip',
        'status', 'status_at',
    ];

    protected $casts = [
        'meta' => 'array',
    ];

    protected $appends = [
        'url', 'transform',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function visits()
    {
        return visits($this);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeStatusOpen($query)
    {
        return $query->where('status', StatusEnum::OPEN);
    }

    public function scopeById($query, $id)
    {
        return $query->where('id', $id);
    }

    public function scopeByCategoryId($query, $categoryId)
    {
        return $query->where('category_id', $categoryId);
    }

    public function scopeImageNotNull($query)
    {
        return $query->whereNotNull('image');
    }

    public function scopeOrderById($query, $sort = 'desc')
    {
        return $query->orderBy('id', $sort);
    }

    public function getUrlAttribute()
    {
        return new UrlPresenter($this);
    }

    public function getTransformAttribute()
    {
        return new TransformPresenter($this);
    }

    public function getCacheCommentsAttribute()
    {
        return \Cache::remember($this->cacheKey().':comments', 15, function () {
            return $this->comments;
        });
    }
    public function getCacheCommentsCountAttribute()
    {
        return \Cache::remember($this->cacheKey().':comments_count', 15, function () {
            return $this->comments()->count();
        });
    }

    public function getCacheCommentsImagesCountAttribute()
    {
        return \Cache::remember($this->cacheKey().':comments_images_count', 15, function () {
            return $this->comments()->imageNotNull()->count();
        });
    }

    public function getImageAttribute($value)
    {
        try {
            return \Storage::url($value);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getThumbnailAttribute($value)
    {
        try {
            return \Storage::url($value);
        } catch (\Exception $e) {
            return null;
        }
    }
}
