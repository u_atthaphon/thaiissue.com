<?php

namespace Thaiissue\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Thaiissue\Presenters\Comment\UrlPresenter;
use Thaiissue\Presenters\Comment\TransformPresenter;

class Comment extends MyModel
{
    use AccessorsDateTrait, FavoriteableTrait, SoftDeletes;

    public $fillable = [
        'category_id', 'post_id', 'user_id', 'order_no',
        'body', 'thumbnail', 'image',
        'nsfw', 'active', 'meta', 'ip',
        'hash_ip',
    ];

    protected $casts = [
        'meta' => 'array',
    ];

    protected $touches = ['post'];

    protected $appends = [
        'url', 'transform'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function visits()
    {
        return visits($this);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeByPostId($query, $postId)
    {
        return $query->where('post_id', $postId);
    }

    public function scopeByCategoryId($query, $categoryId)
    {
        return $query->where('category_id', $categoryId);
    }

    public function scopeByOrderNo($query, $orderNo)
    {
        return $query->where('order_no', $orderNo);
    }

    public function scopeOrderById($query, $sort = 'desc')
    {
        return $query->orderBy('id', $sort);
    }

    public function scopeOrderByOrderNo($query, $sort = 'desc')
    {
        return $query->orderBy('order_no', $sort);
    }

    public function scopeImageNotNull($query)
    {
        return $query->whereNotNull('image');
    }

    public function getUrlAttribute()
    {
        return new UrlPresenter($this);
    }

    public function getTransformAttribute()
    {
        return new TransformPresenter($this);
    }

    public function getImageAttribute($value)
    {
        try {
            return \Storage::url($value);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getThumbnailAttribute($value)
    {
        try {
            return \Storage::url($value);
        } catch (\Exception $e) {
            return null;
        }
    }
}
