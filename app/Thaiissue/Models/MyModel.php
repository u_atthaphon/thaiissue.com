<?php

namespace Thaiissue\Models;

use Illuminate\Database\Eloquent\Model;

class MyModel extends Model
{
    protected function cacheKey()
    {
        return sprintf(
            "%s/%s-%s",
            $this->getTable(),
            $this->getKey(),
            $this->updated_at->timestamp
        );
    }
}
