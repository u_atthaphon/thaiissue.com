<?php

namespace Thaiissue\Models;

trait AccessorsDateTrait
{
    public function getCreatedAtThAttribute()
    {
        if ($this->created_at) {
            \Carbon\Carbon::setLocale('th');
            return $this->created_at->format('d-m-Y h:i:s');
        }
        return null;
    }

    public function getUpdatedAtThAttribute()
    {
        if ($this->updated_at) {
            \Carbon\Carbon::setLocale('th');
            return $this->updated_at->format('d-m-Y h:i:s');
        }
        return null;
    }
}
