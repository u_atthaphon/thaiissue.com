<?php

namespace Thaiissue\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Suspect extends Model
{
    public $fillable = [
        'ip', 'agent', 'meta'
    ];

    protected $casts = [
        'meta' => 'array',
    ];

    public function scopeByIp($query, $ip)
    {
        return $query->where('ip', $ip);
    }

    public function scopeWithInHour($query)
    {
        return $query->where(
            'created_at',
            '>',
            Carbon::now()->subHours(3)->toDateTimeString()
        );
    }
}
