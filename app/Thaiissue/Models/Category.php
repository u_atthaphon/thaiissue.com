<?php

namespace Thaiissue\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Thaiissue\Presenters\Category\UrlPresenter;

class Category extends Model
{
    use HasApiTokens, SoftDeletes;

    public $fillable = [
        'parent_id', 'order_no', 'title',
        'body', 'slug', 'cover', 'views', 'active',
    ];

    protected $appends = [
        'url'
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOnlyParents($query)
    {
        return $query->whereNull('parent_id');
    }

    public function scopeNotParent($query)
    {
        return $query->whereNotNull('parent_id');
    }

    public function scopeById($query, $id)
    {
        return $query->where('id', $id);
    }

    public function scopeBySlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

    public function scopeOderByTitle($query, $orderBy = 'asc')
    {
        return $query->orderBy('title', $orderBy);
    }

    public function getUrlAttribute()
    {
        return new UrlPresenter($this);
    }

    public function getCoverAttribute($value)
    {
        try {
            return \Storage::url($value);
        } catch (\Exception $e) {
            return null;
        }
    }
}
