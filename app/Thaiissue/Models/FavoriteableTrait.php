<?php

namespace Thaiissue\Models;

trait FavoriteableTrait
{
    /**
     * Get all of the favorites for the model.
     */
    public function favorites()
    {
        return $this->morphToMany(User::class, 'favoriteable');
    }

    public function addFavorite()
    {
        if (!\Auth::check()) {
            return;
        }

        if (!$this->favorites->contains(\Auth::id())) {
            $this->favorites()->attach(\Auth::id());
        }
    }

    public function delFavorite()
    {
        if (!\Auth::check()) {
            return;
        }

        $this->favorites()->detach(\Auth::id());
    }

    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }

    public function getIsFavoritedAttribute()
    {
        return \DB::table('favoriteables')
            ->where('user_id', \Auth::id())
            ->where('favoriteable_id', $this->id)
            ->where('favoriteable_type', get_class($this))
            ->exists();
    }

    public function scopeOrderByCreatedAt($query, $orderBy = 'desc')
    {
        return $query->orderBy('favoriteables.created_at', $orderBy);
    }
}
