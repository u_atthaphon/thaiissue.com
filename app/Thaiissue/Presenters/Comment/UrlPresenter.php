<?php

namespace Thaiissue\Presenters\Comment;

use Thaiissue\Models\Comment;

class UrlPresenter
{
    protected $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function __get($key)
    {
        if (method_exists($this, $key)) {
            return $this->$key();
        }
        return $this->$key;
    }

    public function show()
    {
        return route('comments-show', [
            'comment_order_no' => $this->comment->order_no,
            'post_id' => $this->comment->post_id,
            'category_slug' => $this->comment->category->slug,
        ]);
    }
}
