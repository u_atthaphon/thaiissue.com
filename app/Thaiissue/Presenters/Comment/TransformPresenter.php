<?php

namespace Thaiissue\Presenters\Comment;

use Thaiissue\Models\Comment;
use Thaiissue\Presenters\Transform\TransformTrait;

class TransformPresenter
{
    use TransformTrait;

    protected $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function __get($key)
    {
        if (method_exists($this, $key)) {
            return $this->$key();
        }
        return $this->$key;
    }

    public function body()
    {
        $body = $this->comment->body;
        if (!empty($body)) {
            $body = $this->quoteTransform($body);
            $body = $this->linkTransform($body);
            $body = $this->referToCrossTransform($body);
            $body = $this->referToTransform($body, $this->comment->post_id, $this->comment->category->slug);
        }
        return $body;
    }

    public function youtubeEmbeds()
    {
        return $this->youtubeEmbedTransfrom($this->comment->body);
    }
}
