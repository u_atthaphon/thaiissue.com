<?php

namespace Thaiissue\Presenters\Post;

use Thaiissue\Models\Post;
use Thaiissue\Presenters\Transform\TransformTrait;

class TransformPresenter
{
    use TransformTrait;

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function __get($key)
    {
        if (method_exists($this, $key)) {
            return $this->$key();
        }
        return $this->$key;
    }

    public function body()
    {
        $body = $this->post->body;
        if (!empty($body)) {
            $body = $this->quoteTransform($body);
            $body = $this->linkTransform($body);
            $body = $this->referToCrossTransform($body);
            $body = $this->referToTransform($body, $this->post->id, $this->post->category->slug);
        }
        return $body;
    }

    public function youtubeEmbeds()
    {
        return $this->youtubeEmbedTransfrom($this->post->body);
    }
}
