<?php

namespace Thaiissue\Presenters\Post;

use Thaiissue\Models\Post;

class UrlPresenter
{
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function __get($key)
    {
        if(method_exists($this, $key)) {
            return $this->$key();
        }
        return $this->$key;
    }

    public function show()
    {
        return route('posts-show', [
            'post_id' => $this->post->id,
            'category_slug' => $this->post->category->slug,
        ]);
    }
}
