<?php

namespace Thaiissue\Presenters\Category;

use Thaiissue\Models\Category;

class UrlPresenter
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function __get($key)
    {
        if (method_exists($this, $key)) {
            return $this->$key();
        }
        return $this->$key;
    }

    public function show()
    {
        return route('categories-show', [
            'slug' => $this->category->slug,
        ]);
    }

    public function showComments()
    {
        return route('categories-show-comments', [
            'slug' => $this->category->slug,
        ]);
    }

    public function showLinks()
    {
        return route('categories-show-links', [
            'slug' => $this->category->slug,
        ]);
    }

    public function createPost()
    {
        return route('categories-create-post', [
            'slug' => $this->category->slug,
        ]);
    }
}
