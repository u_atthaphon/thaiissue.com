<?php

namespace Thaiissue\Presenters\Transform;

trait TransformTrait
{
    protected function quoteTransform($body)
    {
        $pattern = '/\*([a-zA-Zก-เะ-์\d\s]{1,})\*/';
        $replacement = '<blockquote class="blockquote text-app-primary-dark">${0}</blockquote>';
        return preg_replace($pattern, $replacement, $body);
    }

    protected function linkTransform($body)
    {
        $pattern = '/(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&!:&\[\]\/~+#-]*[\w@?^=%&\/~+#-])?/';
        $replacement = '<a href="${0}" target="_blank">${0}</a>';
        return preg_replace($pattern, $replacement, $body);
    }

    protected function referToCrossTransform($body)
    {
        // Example: >>>/{postId}/{CommentId}/
        // (?:\/(\d+))? Post Id
        // (?:\/(\d+)(\-)?(\d+)?) Comment Id
        $pattern = '/>>>\/(\d+)?(?:\/(\d+)?(\d+)?)/';

        $replacement = '<span class="referToCrossWrap"><a href="'.url('comments/${2}/post/${1}').'" class="referToCross" data-post="${1}" data-comment-no="${2}">${0}</a></span>';
        return preg_replace($pattern, $replacement, $body);
    }

    protected function referToTransform($body, $postId, $categorySlug)
    {
        $pattern = '/>>(\d+)/';
        $replacement = '<span class="referToWrap"><a href="'.url('comments/${1}/post/'.$postId.'/categories/'.$categorySlug).'" class="referTo" data-post="'.$postId.'" data-comment-no="${1}">${0}</a></span>';
        return preg_replace($pattern, $replacement, $body);
    }

    protected function youtubeEmbedTransfrom($body)
    {
        $matchesYoutubeDotCom = [];
        $pattern = '/https?:\/\/(?:www\.)?youtube\.com\/watch\S+v=([a-zA-Z0-9_\-]+)/';
        preg_match_all($pattern, $body, $matchesYoutubeDotCom);

        $matchesYoutuDotBe = [];
        $pattern = '/https?:\/\/youtu\.be\/([a-zA-Z0-9_\-]+)/';
        preg_match_all($pattern, $body, $matchesYoutuDotBe);

        $vdoIds = [];
        if (!empty(array_filter($matchesYoutubeDotCom))) {
            $vdoIds = array_values($matchesYoutubeDotCom[1]);
        }
        if (!empty(array_filter($matchesYoutuDotBe))) {
            // array_push($vdoIds, array_values($matchesYoutuDotBe[1]));
            $vdoIds = array_merge($vdoIds, array_values($matchesYoutuDotBe[1]));
        }

        $vdoIds = array_unique($vdoIds);
        $bodyYouTubeEmbed = null;
        foreach ($vdoIds as $vdoId) {
            $bodyYouTubeEmbed .= "<iframe width='auto' height='auto' src='https://www.youtube.com/embed/{$vdoId}?rel=0' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>";
        }

        return $bodyYouTubeEmbed;
    }
}
