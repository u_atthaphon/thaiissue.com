<?php

namespace Thaiissue\Repositories\User;

use Thaiissue\Models\User;
use Thaiissue\Repositories\Image\UploadTriat;
use Thaiissue\Repositories\AbstractRepository;

class SocialiteRepo extends AbstractRepository implements SocialiteRepoInterface
{
    protected $with = [];
    protected $fillable = [
        'name', 'email', 'password',
        'avatar', 'provider', 'provider_id',
    ];

    use UploadTriat;

    public function model()
    {
        return new User;
    }

    public function firstByProvider($provider, $providerId)
    {
        return $this->model()
            ->byProvider($provider)
            ->byProviderId($providerId)
            ->first();
    }
}
