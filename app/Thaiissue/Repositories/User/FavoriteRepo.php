<?php

namespace Thaiissue\Repositories\User;

use Thaiissue\Models\Post;
use Thaiissue\Models\User;
use Thaiissue\Models\Comment;
use Thaiissue\Repositories\AbstractRepository;

class FavoriteRepo extends AbstractRepository implements FavoriteRepoInterface
{
    protected $with = [];
    protected $fillable = [
        'name', 'email', 'password',
        'avatar', 'provider', 'provider_id',
    ];

    public function model()
    {
        return new User;
    }

    public function toggleFavoritePost($postId)
    {
        $post = Post::findOrFail($postId);

        if (!$post->is_favorited) {
            $post->addFavorite();
            return true;
        } else {
            $post->delFavorite();
            return false;
        }
    }

    public function toggleFavoriteComment($commentId)
    {
        $comment = Comment::findOrFail($commentId);
        if (!$comment->is_favorited) {
            $comment->addFavorite();
            return true;
        } else {
            $comment->delFavorite();
            return false;
        }
    }
}
