<?php

namespace Thaiissue\Repositories\User;

use Thaiissue\Models\User;
use Thaiissue\Repositories\AbstractRepository;

class UserRepo extends AbstractRepository implements UserRepoInterface
{
    protected $with = [];
    protected $fillable = [
        'name', 'email', 'password',
        'avatar', 'provider', 'provider_id',
    ];

    public function model()
    {
        return new User;
    }

    public function favoritePostPaginateByUser(User $user, $limit = 20)
    {
        return $user->favoritePosts()->orderByCreatedAt()->paginate($limit);
    }

    public function favoriteCommentPaginateByUser(User $user, $limit = 20)
    {
        return $user->favoriteComments()->orderByCreatedAt()->paginate($limit);
    }

    public function historyPostPaginateByUser(User $user, $limit = 20)
    {
        return $user->posts()->paginate($limit);
    }

    public function historyCommentPaginateByUser(User $user, $limit = 20)
    {
        return $user->comments()->paginate($limit);
    }
}
