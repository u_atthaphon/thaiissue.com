<?php

namespace Thaiissue\Repositories;

use Illuminate\Database\Eloquent\Collection;

interface RepositoryInterface
{
    /**
     * Get model
     *
     * @return mixed
     */
    public function model();

    /**
     * Get fillable columns
     *
     * @return mixed
     */
    public function fillable();

    /**
     * Set the relationships that should be eager loaded.
     *
     * @param  mixed $without
     * @return static
     */
    public function reloadWith($with);

    /**
     * Get all with tables except for those with the specified without.
     *
     * @param  mixed $without
     * @return static
     */
    public function without($without);

    /**
     * Get all records by descending id
     *
     * @param  mixed $columns
     * @return mixed
     */
    public function all(array $columns = ['*']);

    /**
     * Get all records by descending id, paginated
     *
     * @param  integer $perPage
     * @param  mixed $columns
     * @return mixed
     */
    public function paginate($perPage = 10);

    /**
     * Find record by id
     *
     * @param  integer $id
     * @return mixed
     */
    public function byId($id, array $columns = ['*']);

    /**
     * Find multiple records by array of ids
     *
     * @param  array $ids
     * @param  mixed $columns
     * @return mixed
     */
    public function byIds(array $ids);

    /**
     * Create record
     *
     * @param  array $inputs
     * @param  mixed $columns
     * @return boolean|null
     */
    public function create(array $inputs);

    /**
     * Update record
     *
     * @param  array $inputs
     * @return boolean|null
     */
    public function update(array $inputs);

    /**
     * Update record by id
     *
     * @param  integer $id
     * @param  array
     * @return boolean|null
     */
    public function updateById($id, array $inputs);

    /**
     * Update multiple records by array of ids
     *
     * @param  array $ids
     * @param  array $inputs
     */
    public function updateByIds(array $ids, array $inputs);

    /**
     * Update multiple records by collection of model
     *
     * @param  Collection $collection
     * @param  array $inputs
     */
    public function bulkUpdate(Collection $collection, array $inputs);

    /**
     * Delete (soft) record
     *
     * @return bool|null
     */
    public function delete();

    /**
     * Delete (soft) record by id
     *
     * @param  integer $id
     * @param  array $inputs
     * @return boolean|null
     */
    public function deleteById($id);

    /**
     * Delete (soft) multiple records by array of ids
     *
     * @param  array $ids
     * @param  array $inputs
     */
    public function deleteByIds(array $ids);

    /**
     * Deleted (soft) multiple record by collection of model
     *
     * @param  Collection
     * @param  array
     */
    public function bulkDelete(Collection $collection);
}
