<?php

namespace Thaiissue\Repositories\Image;

use Illuminate\Support\Str;

trait UploadTriat
{
    public function uploadCover($coverImg)
    {
        ini_set('memory_limit', '1500M');
        $imgName = $coverImg->getClientOriginalName();
        $img = \Image::make($coverImg);
        $filename = "covers/$imgName";
        \Storage::put($filename, (string) $img->encode(), 'public');

        return $filename;
    }

    public function uploadByAvatarUrl($avatarUrl)
    {
        ini_set('memory_limit', '1500M');
        $filenaemUUID = Str::uuid();
        $imgName = $filenaemUUID.'.jpg';
        $img = \Image::make($avatarUrl);
        $img->widen(80)->heighten(80);
        $filename = "avatars/{$imgName}";
        \Storage::put($filename, (string) $img->encode('jpg', 75), 'public');
        return $filename;
    }

    public function uploadImage($imgSrc)
    {
        ini_set('memory_limit', '1500M');
        $filenaemUUID = Str::uuid();
        $imgName = $filenaemUUID.'.'.$imgSrc->getClientOriginalExtension();
        $thumbName = $filenaemUUID.'_thumb.'.$imgSrc->getClientOriginalExtension();
        $img = \Image::make($imgSrc->getRealPath());
        $img->backup();
        $imgName = $this->resizeImage($img, $imgName);
        $thumbName = $this->resizeThumbnail($img, $thumbName);
        return [
            'image' => $imgName,
            'thumbnail' => $thumbName
        ];
    }

    public function uploadAnimationGifImage($imgSrc)
    {
        ini_set('memory_limit', '1500M');
        $filenaemUUID = Str::uuid();
        $imgName = $filenaemUUID.'.'.$imgSrc->getClientOriginalExtension();
        $thumbName = $filenaemUUID.'_thumb.'.$imgSrc->getClientOriginalExtension();
        $img = \Image::make($imgSrc->getRealPath());
        $img->backup();
        $imgName = $this->saveAnimationGif($imgSrc, $imgName);
        $thumbName = $this->resizeOriginalImage($img, $thumbName);
        return [
            'image' => $imgName,
            'thumbnail' => $thumbName
        ];
    }

    public function saveAnimationGif($imgSrc, $filename)
    {
        $path = "images/";
        \Storage::putFileAs($path, $imgSrc, $filename, 'public');
        return "{$path}{$filename}";
    }

    // save original size as thumbnail for animation gif file
    protected function resizeOriginalImage($img, $filename)
    {
        $filename = "images/{$filename}";
        $img->reset();
        $width = $img->width();
        $img->widen($width, function ($constraint) {
            $constraint->upsize();
        });
        \Storage::put($filename, (string) $img->encode(), 'public');
        return $filename;
    }

    protected function resizeImage($img, $filename)
    {
        $filename = "images/{$filename}";
        $img->reset();
        $width = 800;
        if ($img->width() > $img->height()) {
            $width = 1200;
        }
        $img->widen($width, function ($constraint) {
            $constraint->upsize();
        });
        \Storage::put($filename, (string) $img->encode('jpg', 75), 'public');

        return $filename;
    }

    protected function resizeThumbnail($img, $filename)
    {
        $filename = "images/{$filename}";
        $img->reset();
        $width = 150;
        if ($img->width() > $img->height()) {
            $width = 250;
        }
        $img->widen($width, function ($constraint) {
            $constraint->upsize();
        });
        \Storage::put($filename, (string) $img->encode('jpg', 75), 'public');

        return $filename;
    }
}
