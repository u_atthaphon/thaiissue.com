<?php

namespace Thaiissue\Repositories\Post;

use Thaiissue\Models\Post;
use Thaiissue\Repositories\AbstractRepository;
use Thaiissue\Repositories\Image\UploadTriat;

class SavePostRepo extends AbstractRepository implements SavePostRepoInterface
{
    use UploadTriat;

    protected $with = [];
    protected $fillable = [];

    public function __construct()
    {
        $this->fillable = $this->model()->fillable;
    }

    public function model()
    {
        return new Post;
    }

    public function createPost(array $inputs, $image = null)
    {
        try {
            $post = $this->create($inputs);
            $imagePaths = [
                'image' => null,
                'thumbnail' => null,
            ];
            if (!empty($image)) {
                if (is_animated_gif($image)) {
                    $imagePaths = $this->uploadAnimationGifImage($image);
                } else {
                    $imagePaths = $this->uploadImage($image);
                }
            }
            $post->image = $imagePaths['image'];
            $post->thumbnail = $imagePaths['thumbnail'];
            $post->save();
            $post->touch();
        } catch (Exception $e) {
            throw new Exception("Error: Can't create post {$e->getMessage()}");
        }

        return $post;
    }
}
