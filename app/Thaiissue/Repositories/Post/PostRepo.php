<?php

namespace Thaiissue\Repositories\Post;

use Thaiissue\Repositories\AbstractRepository;
use Thaiissue\Models\Post;

class PostRepo extends AbstractRepository implements PostRepoInterface
{
    protected $with = [];
    protected $fillable = [];

    public function __construct()
    {
        $this->fillable = $this->model()->fillable;
    }

    public function model()
    {
        return new Post;
    }

    public function allActive()
    {
        return $this->model()
            ->active()
            ->statusOpen()
            ->orderById('asc')
            ->get();
    }

    public function paginateByCategoryId($categoryId, $limit = 20)
    {
        return $this->model()
            ->active()
            ->statusOpen()
            ->byCategoryId($categoryId)
            ->orderById()
            ->paginate($limit);
    }

    public function whenStatusOpenById($id)
    {
        return $this->model()
            ->active()
            ->statusOpen()
            ->byId($id)
            ->firstOrFail();
    }

    public function whenStatusOpenWithCommentsById($id)
    {
        return $this->model()
            ->with(['comments' => function ($query) {
                $query->active()
                    ->orderByOrderNo('asc');
            }])
            ->active()
            ->statusOpen()
            ->byId($id)
            ->firstOrFail();
    }

    public function isExistsById($id)
    {
        return $this->model()
            ->byId($id)
            ->exists();
    }
}
