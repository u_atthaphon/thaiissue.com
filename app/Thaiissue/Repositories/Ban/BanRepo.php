<?php

namespace Thaiissue\Repositories\Ban;

use Thaiissue\Models\Ban;
use Thaiissue\Repositories\AbstractRepository;
use Thaiissue\Enums\Ban as BanEnum;
use Carbon\Carbon;

class BanRepo extends AbstractRepository implements BanRepoInterface
{
    protected $with = [];
    protected $fillable = [];

    public function __construct()
    {
        $this->fillable = $this->model()->fillable;
    }

    public function model()
    {
        return new Ban;
    }

    public function latestBanbyIp($ip)
    {
        return $this->model()->byIp($ip)->latest()->first();
    }

    public function latestBanbyUserId($userId)
    {
        return $this->model()->byUserId($userId)->latest()->first();
    }

    public function ban($ip, $level = 1, $reason = null)
    {
        $expiredAt = Carbon::now()->addMinutes(30)->toDateTimeString();
        switch ($level) {
            case BanEnum::LEVEL2:
                $expiredAt = Carbon::now()->addHours(2)->toDateTimeString();
                break;
            case BanEnum::LEVEL3:
                $expiredAt = Carbon::now()->addHours(24)->toDateTimeString();
                break;
            case BanEnum::LEVEL4:
                $expiredAt = Carbon::now()->addDays(3)->toDateTimeString();
                break;
            case BanEnum::LEVEL1:
            default:
                $expiredAt = Carbon::now()->addMinutes(30)->toDateTimeString();
                break;
        }
        $userId = \Auth::check() ? \Auth::id() : null;
        $inputs = [
            'ip' => $ip,
            'user_id' => $userId,
            'reason' => $reason,
            'ban' => true,
            'level' => $level,
            'expired_at' => $expiredAt,
        ];

        return $this->model()->create($inputs);
    }
}
