<?php

namespace Thaiissue\Repositories\Suspect;

use Thaiissue\Models\Suspect;
use Thaiissue\Repositories\AbstractRepository;

class SuspectRepo extends AbstractRepository implements SuspectRepoInterface
{
    protected $with = [];
    protected $fillable = [];

    public function __construct()
    {
        $this->fillable = $this->model()->fillable;
    }

    public function model()
    {
        return new Suspect;
    }

    public function addSuspect($ip, $agent = null, $meta = null)
    {
        $inputs = [
            'ip' => htmlspecialchars($ip),
            'agent' => htmlspecialchars($agent),
            'meta' => $meta,
        ];

        if (\Auth::check()) {
            $inputs['user_id'] = \Auth::id();
        }

        return $this->model()->create($inputs);
    }

    public function shouldBanIp($ip)
    {
        // ban ip if suspected count 3 times in 30 minutes
        $count = $this->model()->byIp($ip)->withInHour()->count();

        return $count >= 3;
    }
}
