<?php

namespace Thaiissue\Repositories\Comment;

use Thaiissue\Models\Comment;
use Thaiissue\Repositories\Image\UploadTriat;
use Thaiissue\Repositories\AbstractRepository;

class SaveCommentRepo extends AbstractRepository implements SaveCommentRepoInterface
{
    use UploadTriat;

    protected $with = [];
    protected $fillable = [];

    public function __construct()
    {
        $this->fillable = $this->model()->fillable;
    }

    public function model()
    {
        return new Comment;
    }

    public function createComment(array $inputs, $image = null)
    {
        try {
            $comment = $this->create($inputs);
            $imagePaths = [
                'image' => null,
                'thumbnail' => null,
            ];
            if (!empty($image)) {
                if (is_animated_gif($image)) {
                    $imagePaths = $this->uploadAnimationGifImage($image);
                } else {
                    $imagePaths = $this->uploadImage($image);
                }
            }
            $comment->image = $imagePaths['image'];
            $comment->thumbnail = $imagePaths['thumbnail'];
            $comment->save();
            $comment->touch();
        } catch (Exception $e) {
            throw new Exception("Error: Can't create comment {$e->getMessage()}");
        }

        return $comment;
    }
}
