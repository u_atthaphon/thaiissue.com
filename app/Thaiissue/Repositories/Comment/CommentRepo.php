<?php

namespace Thaiissue\Repositories\Comment;

use Thaiissue\Models\Comment;
use Thaiissue\Repositories\AbstractRepository;

class CommentRepo extends AbstractRepository implements CommentRepoInterface
{
    protected $with = [];

    protected $fillable = [];

    public function __construct()
    {
        $this->fillable = $this->model()->fillable;
    }

    public function model()
    {
        return new Comment;
    }

    public function allActive()
    {
        return $this->model()
            ->active()
            ->orderById()
            ->get();
    }

    public function paginateByCategoryId($categoryId, $limit = 20)
    {
        return $this->model()
            ->with('post')
            ->active()
            ->byCategoryId($categoryId)
            ->orderById()
            ->paginate($limit);
    }

    public function paginateByPostId($postId, $limit = 20)
    {
        return $this->model()
            ->with('post')
            ->active()
            ->byPostId($postId)
            ->orderById('asc')
            ->paginate($limit);
    }

    public function byOrderNoAndPostId($orderNo, $postId)
    {
        return $this->model()
            ->with('post')
            ->active()
            ->byPostId($postId)
            ->byOrderNo($orderNo)
            ->firstOrFail();
    }

    public function latestOrderNoByPostId($postId)
    {
        $comment = $this->model()
            ->active()
            ->byPostId($postId)
            ->orderByOrderNo()
            ->first();
        if (empty($comment)) {
            return 0;
        }
        return $comment->order_no;
    }
}
