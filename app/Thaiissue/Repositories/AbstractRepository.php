<?php

namespace Thaiissue\Repositories;

use Illuminate\Database\Eloquent\Collection;

abstract class AbstractRepository implements RepositoryInterface
{
    protected $with = [];

    protected $fillable = [];

    /**
     * Get a new model instance for the repository.
     *
     * @return mixed
     */
    abstract public function model();

    /**
     * Get fillable columns
     *
     * @return mixed
     */
    public function fillable()
    {
        return $this->fillable;
    }

    /**
     * Set the relationships that should be eager loaded.
     *
     * @param  mixed $without
     * @return static
     */
    public function reloadWith($with)
    {
        $with = is_array($with) ? $with : func_get_args();

        $this->with = array_merge($this->with, $with);

        return $this;
    }

    /**
     * Get all with tables except for those with the specified without.
     *
     * @param  mixed $without
     * @return static
     */
    public function without($without)
    {
        $without = is_array($without) ? $without : func_get_args();

        $this->with = array_diff($this->with, $without);

        return $this;
    }

    /**
     * Get all records by descending id
     *
     * @param  mixed $columns
     * @return mixed
     */
    public function all(array $columns = ['*'])
    {
        return $this->model()
            ->with($this->with)
            ->orderByDesc('id')
            ->get($columns);
    }

    /**
     * Get all records by descending id, paginated
     *
     * @param  integer $perPage
     * @param  mixed $columns
     * @return mixed
     */
    public function paginate($perPage = 10)
    {
        return $this->model()
            ->with($this->with)
            ->orderByDesc('id')
            ->paginate($perPage);
    }

    /**
     * Find record by id
     *
     * @param  integer $id
     * @param  mixed $columns
     * @return mixed
     */
    public function byId($id, array $columns = ['*'])
    {
        return $this->model()
            ->with($this->with)
            ->findOrFail($id, $columns);
    }

    public function byTitle($title, array $columns = ['*'])
    {
        return $this->model()
            ->with($this->with)
            ->where('title', $title)
            ->select($columns)
            ->firstOrFail();
    }

    /**
     * Find multiple records by array of ids
     *
     * @param  array $ids
     * @param  mixed $columns
     * @return mixed
     */
    public function byIds(array $ids)
    {
        return $this->model()
            ->with($this->with)
            ->whereIn('id', $ids)
            ->get();
    }

    /**
     * Create record
     *
     * @param  array $inputs
     * @return boolean|null
     */
    public function create(array $inputs)
    {
        return $this->model()->create(array_only($inputs, $this->fillable()));
    }

    /**
     * Upsert by array key and value before upsert
     *
     * @param  array  $inputs The updated value
     * @param  array  $key    The key with value to check for upsert
     * @return mixed
     */
    public function upsertByKey(array $inputs, array $key)
    {
        return $this->model()->updateOrCreate($key, array_only($inputs, $this->fillable()));
    }

    /**
     * Require save() to record model
     *
     * @param  array  $inputs
     * @param  array  $key
     * @return Model
     */
    public function firstOrNew(array $key, array $inputs)
    {
        return $this->model()->firstOrNew(
            $key,
            array_only($inputs, $this->fillable())
        );
    }

    /**
     * Upsert with one field
     *
     * @param  array  $inputs The key, value to upsert
     * @return mixed
     */
    public function upsert(array $inputs)
    {
        return $this->model()->updateOrCreate(array_only($inputs, $this->fillable()));
    }

    /**
     * Upsert key value by model
     *
     * @param  Model $model
     * @param  array
     * @return mixed
     */
    protected function upsertByModel($model, array $inputs)
    {
        return $model->updateOrCreate(array_only($inputs, $this->fillable()));
    }

    /**
     * Upsert multiple records by collection of model
     *
     * @param  Collection $collection
     * @param  array      $inputs
     * @return mixed
     */
    public function bulkUpsert(Collection $collection, array $inputs)
    {
        $collection->each(function ($model) use ($inputs) {
            $this->upsertByModel($model, $inputs);
        });
    }

    /**
     * Update record
     *
     * @param  array $inputs
     * @return boolean|null
     */
    public function update(array $inputs)
    {
        return $this->model()->update(array_only($inputs, $this->fillable()));
    }

    /**
     * Update record by model
     *
     * @param  Model $model
     * @param  array $inputs
     * @return boolean|null
     */
    protected function updateByModel($model, array $inputs)
    {
        return $model->update(array_only($inputs, $this->fillable()));
    }

    /**
     * Update record by id
     *
     * @param  integer $id
     * @param  array
     * @return boolean|null
     */
    public function updateById($id, array $inputs)
    {
        return $this->model()->where('id', $id)->update(array_only($inputs, $this->fillable()));
    }

    /**
     * Update multiple records by array of ids
     *
     * @param  array $ids
     * @param  array $inputs
     */
    public function updateByIds(array $ids, array $inputs)
    {
        $modelCollection = $this->getModel->whereIn('id', $ids)->get();

        $modelCollection->each(function ($model) use ($inputs) {
            $this->updateByModel($model, $inputs);
        });
    }

    /**
     * Update multiple records by collection of model
     *
     * @param  Collection $collection
     * @param  array $inputs
     */
    public function bulkUpdate(Collection $collection, array $inputs)
    {
        $collection->each(function ($model) use ($inputs) {
            $this->updateByModel($model, $inputs);
        });
    }

    /**
     * Delete (soft) record
     *
     * @return bool|null
     */
    public function delete()
    {
        return $this->model()->delete();
    }

    /**
     * Delete record by model
     *
     * @param  Model $model
     * @return boolean|null
     */
    protected function deleteByModel($model)
    {
        return $model->delete();
    }

    /**
     * Delete (soft) record by id
     *
     * @param  integer $id
     * @param  array $inputs
     * @return boolean|null
     */
    public function deleteById($id)
    {
        return $this->model()->where('id', $id)->delete();
    }

    /**
     * Delete (soft) multiple records by array of ids
     *
     * @param  array $ids
     * @param  array $inputs
     */
    public function deleteByIds(array $ids)
    {
        $modelCollection = $this->model()->whereIn('id', $ids)->get();

        $modelCollection->each(function ($model) {
            $this->deleteByModel($model);
        });
    }

    /**
     * Deleted (soft) multiple record by collection of model
     *
     * @param  Collection
     * @param  array
     */
    public function bulkDelete(Collection $collection)
    {
        $collection->each(function ($model) {
            $this->deleteByModel($model);
        });
    }

    public function touch()
    {
        return $this->model()->touch();
    }

    public function nextRecord($model, $key = 'id')
    {
        return $this->model()
            ->where($key, '>', $model->{$key})
            ->min($key);
    }

    public function previousRecord($model, $key = 'id')
    {
        return $this->model()
            ->where($key, '<', $model->{$key})
            ->max($key);
    }

    public function countAll()
    {
        return $this->model()->count();
    }

    public function incrementVisitsById($id)
    {
        $model = $this->model()
            ->byId($id)
            ->firstOrFail();
        try {
            $model->visits()->increment();
        } catch (\Exception $e) {
            //
        }
    }

    public function restoreSoftDeletedById($id)
    {
        return $this->model()->where('id', $id)->restore();
    }
}
