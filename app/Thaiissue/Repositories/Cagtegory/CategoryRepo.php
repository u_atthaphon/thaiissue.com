<?php

namespace Thaiissue\Repositories\Category;

use Thaiissue\Models\Category;
use Thaiissue\Repositories\Image\UploadTriat;
use Thaiissue\Repositories\AbstractRepository;

class CategoryRepo extends AbstractRepository implements CategoryRepoInterface
{
    use UploadTriat;

    protected $with = [];

    protected $fillable = [];

    public function __construct()
    {
        $this->fillable = $this->model()->fillable;
    }

    public function model()
    {
        return new Category;
    }

    public function getAllParentWithchildren()
    {
        return $this->model()
            ->with(['children' => function ($query) {
                $query->active();
                $query->oderByTitle();
            }])
            ->onlyParents()
            ->active()
            ->get();
    }

    public function bySlug($slug)
    {
        return $this->model()
            ->bySlug($slug)
            ->first();
    }

    public function notParentBySlug($slug)
    {
        return $this->model()
            ->notParent()
            ->bySlug($slug)
            ->firstOrFail();
    }

    public function notParentById($id)
    {
        return $this->model()
            ->notParent()
            ->byId($id)
            ->firstOrFail();
    }

    public function isExistsBySlug($slug)
    {
        return $this->model()
            ->notParent()
            ->bySlug($slug)
            ->exists();
    }

    public function allParent()
    {
        return $this->model()
            ->onlyParents()
            ->active()
            ->get();
    }

    public function allChidren()
    {
        return $this->model()
            ->notParent()
            ->active()
            ->get();
    }

    public function createSubCategory(array $inputs, $image)
    {
        $category = $this->create($inputs);
        $cover = null;

        try {
            if (!empty($image)) {
                $cover = $this->uploadCover($image);
            }
            $category->cover = $cover;
            $category->save();
            $category->touch();
        } catch (Exception $e) {
            throw new Exception("Error: Can't create category {$e->getMessage()}");
        }

        return $category;
    }
}
