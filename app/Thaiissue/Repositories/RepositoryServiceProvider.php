<?php

namespace Thaiissue\Repositories;

use Illuminate\Support\ServiceProvider;
use Thaiissue\Repositories\Ban\BanRepo;
use Thaiissue\Repositories\Post\PostRepo;
use Thaiissue\Repositories\User\UserRepo;
use Thaiissue\Repositories\Post\SavePostRepo;
use Thaiissue\Repositories\User\FavoriteRepo;
use Thaiissue\Repositories\User\SocialiteRepo;
use Thaiissue\Repositories\Comment\CommentRepo;
use Thaiissue\Repositories\Suspect\SuspectRepo;
use Thaiissue\Repositories\Ban\BanRepoInterface;
use Thaiissue\Repositories\Category\CategoryRepo;
use Thaiissue\Repositories\Post\PostRepoInterface;
use Thaiissue\Repositories\User\UserRepoInterface;
use Thaiissue\Repositories\Comment\SaveCommentRepo;
use Thaiissue\Repositories\Post\SavePostRepoInterface;
use Thaiissue\Repositories\User\FavoriteRepoInterface;
use Thaiissue\Repositories\User\SocialiteRepoInterface;
use Thaiissue\Repositories\Comment\CommentRepoInterface;
use Thaiissue\Repositories\Suspect\SuspectRepoInterface;
use Thaiissue\Repositories\Category\CategoryRepoInterface;
use Thaiissue\Repositories\Comment\SaveCommentRepoInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CategoryRepoInterface::class,
            CategoryRepo::class
        );
        $this->app->bind(
            PostRepoInterface::class,
            PostRepo::class
        );
        $this->app->bind(
            SavePostRepoInterface::class,
            SavePostRepo::class
        );
        $this->app->bind(
            CommentRepoInterface::class,
            CommentRepo::class
        );
        $this->app->bind(
            SaveCommentRepoInterface::class,
            SaveCommentRepo::class
        );
        $this->app->bind(
            SocialiteRepoInterface::class,
            SocialiteRepo::class
        );
        $this->app->bind(
            FavoriteRepoInterface::class,
            FavoriteRepo::class
        );
        $this->app->bind(
            UserRepoInterface::class,
            UserRepo::class
        );
        $this->app->bind(
            SuspectRepoInterface::class,
            SuspectRepo::class
        );
        $this->app->bind(
            BanRepoInterface::class,
            BanRepo::class
        );
    }
}
