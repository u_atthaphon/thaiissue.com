<?php

namespace Thaiissue\Middleware;

use Closure;
use Thaiissue\Repositories\Ban\BanRepoInterface;
use Carbon\Carbon;

class CheckBannedUserId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!\Auth::check()) {
            return $next($request);
        }

        $banRepo = app(BanRepoInterface::class);
        $user = \Auth::user();
        $ban = $banRepo->latestBanbyUserId($user->id);

        if (!empty($ban) &&
            $ban->ban &&
            $ban->expired_at->isFuture()
        ) {
            Carbon::setLocale('th');
            $diffHuman = $ban->expired_at->diffForHumans(Carbon::now());

            return response("ขออภัย, {$user->name} คุณโดนแบน เดี๋ยวค่อยกลับมาประมาณ {$diffHuman}", 401);
        }

        return $next($request);
    }
}
