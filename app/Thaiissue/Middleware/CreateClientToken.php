<?php

namespace Thaiissue\Middleware;

use Closure;
use League\OAuth2\Server\ResourceServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class CreateClientToken
{
    /**
     * Create a new middleware instance.
     *
     * @param  \League\OAuth2\Server\ResourceServer  $server
     * @return void
     */
    public function __construct(ResourceServer $server)
    {
        $this->server = $server;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->shouldReceiveFreshToken($request)) {
            $this->saveSessionAccessToken(
                $request,
                $this->refreshAccessToken()
            );
        }

        if (!$this->validateBearerToken($request)) {
            $this->saveSessionAccessToken(
                $request,
                $this->refreshAccessToken()
            );
        }

        return $next($request);
    }

    protected function shouldReceiveFreshToken($request)
    {
        return !$request->session()->has('client_access_token') ||
            $request->session()->has('client_expires_in') &&
            $request->session()->get('client_expires_in') < \Carbon\Carbon::now();
    }

    public function validateBearerToken($request)
    {
        $request->headers->add([
            'authorization' => "Bearer {$request->session()->get('client_access_token')}"
        ]);
        $psr = (new DiactorosFactory)->createRequest($request);
        try {
            $psr = $this->server->validateAuthenticatedRequest($psr);
        } catch (OAuthServerException $e) {
            return false;
        }

        return true;
    }

    protected function refreshAccessToken()
    {
        $tokenController = app('\Laravel\Passport\Http\Controllers\AccessTokenController');
        $serverRequest = \GuzzleHttp\Psr7\ServerRequest::fromGlobals()->withParsedBody([
            'grant_type' => 'client_credentials',
            'client_id' => \Config::get('services.thaissue.client_id'),
            'client_secret' => \Config::get('services.thaissue.client_secret'),
            'scope' => '*',
        ]);

        $response = $tokenController->issueToken($serverRequest);
        $token = json_decode($response->getContent());

        return $token;
    }

    protected function saveSessionAccessToken($request, $token)
    {
        $accessToken = $token->access_token;
        $expiresIn = \Carbon\Carbon::now()->addSeconds($token->expires_in);

        $request->session()->forget('client_access_token');
        $request->session()->forget('client_expires_in');
        $request->session()->put('client_access_token', $accessToken);
        $request->session()->put('client_expires_in', $expiresIn);
    }
}
