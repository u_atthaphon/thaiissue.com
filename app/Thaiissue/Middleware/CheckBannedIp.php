<?php

namespace Thaiissue\Middleware;

use Closure;
use Thaiissue\Repositories\Ban\BanRepoInterface;
use Carbon\Carbon;

class CheckBannedIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $banRepo = app(BanRepoInterface::class);
        $ip = real_ip();
        $ban = $banRepo->latestBanbyIp($ip);

        if (!empty($ban) &&
            $ban->ban &&
            $ban->expired_at->isFuture()
        ) {
            Carbon::setLocale('th');
            $diffHuman = $ban->expired_at->diffForHumans(Carbon::now());

            if ($ban->user_id != null) {
                return response("ขออภัย, คุณโดนแบน เดี๋ยวค่อยกลับมาประมาณ {$diffHuman}", 401);
            }
            return response("ขออภัย, {$ip} โดนแบน เดี๋ยวค่อยกลับมาประมาณ {$diffHuman}", 401);
        }

        return $next($request);
    }
}
