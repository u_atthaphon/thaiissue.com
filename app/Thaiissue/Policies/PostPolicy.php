<?php

namespace Thaiissue\Policies;

use Thaiissue\Models\User;
use Thaiissue\Models\Post;
use Thaiissue\Enums\Role as RoleEnum;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function deletedPost(User $user, Post $post)
    {
        return $user->inRole(RoleEnum::ADMIN);
    }

    public function viewPostHistory(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
}
