<?php

namespace Thaiissue\Admin\Controllers;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        return view('admin.dashboard.index');
    }

    public function postIndex()
    {
        return view('admin.posts.index');
    }
}
