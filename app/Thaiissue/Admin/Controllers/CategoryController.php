<?php

namespace Thaiissue\Admin\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Category\CategoryRepoInterface;

class CategoryController extends Controller
{
    protected $categoryRepo;

    public function __construct(CategoryRepoInterface $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    public function categoryDatatables()
    {
        $builder = \DB::table('categories')
            ->select([
                'categories.*',
                \DB::raw('parent.title as parent_title'),
            ])
            ->leftJoin('categories As parent', 'parent.id', '=', 'categories.parent_id')
            ->orderBy('parent_id', 'asc')
            ->orderBy('id', 'asc');

        return DataTables::of($builder)
            ->addColumn('parent', function ($category) {
                return $category->parent_title ? $category->parent_title : null;
            })
            ->editColumn('cover', function ($category) {
                try {
                    return \Storage::url($category->cover);
                } catch (\Exception $e) {
                    return null;
                }
            })
            ->addColumn('action', function ($category) {
                $editUrl = route('admin-categories-edit', ['post_id' => $category->id]);
                $destroyUrl = route('admin-categories-destroy', ['post_id' => $category->id]);
                $restoreUrl = route('admin-categories-restore', ['post_id' => $category->id]);
                $csrfToken = request()->session()->token();
                $editAction = "<a href='{$editUrl}'
                    class='btn btn-primary btn-sm'
                    >Edit</a>";
                $deleteAction = "<form action='{$destroyUrl}' method='POST'>
                    <input type='hidden' name='_method' value='DELETE'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to delete?\")'
                    class='btn btn-danger btn-sm'
                    >Del</button>
                    </form>";
                $restoreAction = "<form action='{$restoreUrl}' method='POST'>
                    <input type='hidden' name='_method' value='PATCH'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to restore?\")'
                    class='btn btn-success btn-sm'
                    >Restore</button>
                    </form>";
                if ($category->deleted_at !== null) {
                    return $restoreAction;
                }
                return "{$editAction}{$deleteAction}";
            })
            ->setRowClass(function ($category) {
                if ($category->deleted_at) {
                    return 'soft-delete';
                }
                return $category->parent_title == null ? 'parent-category' : 'sub-category';
            })
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $isSub = $request->input('sub-category', false);

        if ($isSub) {
            $categoryParentOnly = $this->categoryRepo->allParent();
            return view('admin.categories.create-sub', [
                'categories' => $categoryParentOnly,
            ]);
        }
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug'  => 'required',
        ]);

        $inputs = [
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'body' => $request->input('body'),
            'active' => $request->has('active'),
        ];

        $category = $this->categoryRepo->create($inputs);
        return redirect(route('admin-categories-index'))->with('message', "Added category id: {$category->id} added!");
    }

    public function storeSub(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'title' => 'required',
            'slug'  => 'required',
            'cover' => 'required|mimes:png|dimensions:width=851height=207',
        ]);

        $inputs = [
            'parent_id' => $request->input('category_id'),
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'body' => $request->input('body'),
            'active' => $request->has('active'),
        ];

        $coverImg = $request->file('cover');
        $category = $this->categoryRepo->createSubCategory($inputs, $coverImg);
        return redirect(route('admin-categories-index'))->with('message', "Added sub category id: {$category->id} added!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepo->byId($id);
        $parents = $this->categoryRepo->allParent();
        return view('admin.categories.edit', [
            'category' => $category,
            'parents' => $parents,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug'  => 'required',
        ]);

        $inputs = [
            'parent_id' => $request->input('category_id'),
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'body' => $request->input('body'),
            'active' => $request->has('active'),
            'update_at' => \Carbon\Carbon::now(),
        ];

        $post = $this->categoryRepo->updateById($id, $inputs);
        return redirect()->back()->with('message', "Category id {$id} updated!");
        ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->categoryRepo->deleteById($id);
        return back();
    }

    public function restore($id)
    {
        $this->categoryRepo->restoreSoftDeletedById($id);
        return back();
    }
}
