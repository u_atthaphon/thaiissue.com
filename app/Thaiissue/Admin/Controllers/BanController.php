<?php

namespace Thaiissue\Admin\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Ban\BanRepoInterface;

class BanController extends Controller
{
    protected $banRepo;

    public function __construct(
        BanRepoInterface $banRepo
    ) {
        $this->banRepo = $banRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.bans.index');
    }

    public function banDatatables()
    {
        $builder = \DB::table('bans')
            ->select([
                'bans.*',
                'users.name'
            ])
            ->leftJoin('users', 'users.id', '=', 'bans.user_id')
            ->orderBy('created_at', 'desc');

        return DataTables::of($builder)
            ->addColumn('user', function ($ban) {
                if ($ban->user_id) {
                    return "{$ban->user_id}: {$ban->name}";
                }
                return $ban->user_id;
            })
            ->addColumn('action', function ($ban) {
                $editUrl = route('admin-bans-edit', ['ban_id' => $ban->id]);
                $destroyUrl = route('admin-bans-destroy', ['ban_id' => $ban->id]);
                $restoreUrl = route('admin-bans-restore', ['ban_id' => $ban->id]);
                $csrfToken = request()->session()->token();
                $editAction = "<a href='{$editUrl}'
                    class='btn btn-primary btn-sm'
                    >Edit</a>";
                $deleteAction = "<form action='{$destroyUrl}' method='POST'>
                    <input type='hidden' name='_method' value='DELETE'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to delete?\")'
                    class='btn btn-danger btn-sm'
                    >Del</button>
                    </form>";
                $restoreAction = "<form action='{$restoreUrl}' method='POST'>
                    <input type='hidden' name='_method' value='PATCH'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to restore?\")'
                    class='btn btn-success btn-sm'
                    >Restore</button>
                    </form>";
                if ($ban->deleted_at !== null) {
                    return $restoreAction;
                }
                return "{$editAction}{$deleteAction}";
            })
            ->setRowClass(function ($post) {
                return $post->deleted_at ? 'soft-delete' : '';
            })
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'ip'      => 'required_without:user_id',
            'user_id' => 'required_without:ip',
        ]);

        $inputs = [
            'ip' => $request->input('ip'),
            'user_id' => $request->input('user_id'),
            'reason' => $request->input('reason'),
            'ban' => $request->has('ban'),
            'level' => $request->input('level'),
            'created_at' => $request->input(),
            'expired_at' => ban_expired_by_level($request->input('level')),
        ];

        $ban = $this->banRepo->create($inputs);
        return redirect(route('admin-bans-index'))->with('message', "Added Ban id: {$ban->id} added!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ban = $this->banRepo->byId($id);
        return view('admin.bans.edit', [
            'ban' => $ban,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'ip'      => 'required_without:user_id',
            'user_id' => 'required_without:ip',
        ]);

        $inputs = [
            'ip' => $request->input('ip'),
            'user_id' => $request->input('user_id'),
            'reason' => $request->input('reason'),
            'ban' => $request->has('ban'),
            'level' => $request->input('level'),
            'created_at' => $request->input(),
            'expired_at' => ban_expired_by_level($request->input('level')),
        ];

        $ban = $this->banRepo->updateById($id, $inputs);
        return redirect()->back()->with('message', "Ban id:{$id} updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->banRepo->deleteById($id);
        return back();
    }

    public function restore($id)
    {
        $this->banRepo->restoreSoftDeletedById($id);
        return back();
    }
}
