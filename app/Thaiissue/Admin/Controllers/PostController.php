<?php

namespace Thaiissue\Admin\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Post\PostRepoInterface;
use Thaiissue\Repositories\Category\CategoryRepoInterface;

class PostController extends Controller
{
    protected $postRepo;
    protected $categoryRepo;


    public function __construct(
        PostRepoInterface $postRepo,
        CategoryRepoInterface $categoryRepo
    ) {
        $this->postRepo = $postRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.posts.index');
    }

    public function postDatatables()
    {
        $builder = \DB::table('posts')
            ->select([
                'posts.*',
                'users.name',
                \DB::raw('categories.title as category_title'),
                \DB::raw('categories.slug as category_slug')
            ])
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->leftJoin('categories', 'categories.id', '=', 'posts.category_id')
            ->orderBy('id', 'desc');

        return DataTables::of($builder)
            ->editColumn('title', function ($post) {
                return str_limit($post->title, 150);
            })
            ->editColumn('body', function ($post) {
                return str_limit($post->body, 150);
            })
            ->editColumn('thumbnail', function ($post) {
                try {
                    return \Storage::url($post->thumbnail);
                } catch (\Exception $e) {
                    return null;
                }
            })
            ->addColumn('user', function ($post) {
                if ($post->user_id) {
                    return "{$post->user_id}: {$post->name}";
                }
                return $post->user_id;
            })
            ->addColumn('action', function ($post) {
                $editUrl = route('admin-posts-edit', ['post_id' => $post->id]);
                $destroyUrl = route('admin-posts-destroy', ['post_id' => $post->id]);
                $viewUrl = route('posts-show', ['id' => $post->id, 'slug' => $post->category_slug]);
                $restoreUrl = route('admin-posts-restore', ['post_id' => $post->id]);
                $csrfToken = request()->session()->token();
                $editAction = "<a href='{$editUrl}'
                    class='btn btn-primary btn-sm'
                    >Edit</a>";
                $deleteAction = "<form action='{$destroyUrl}' method='POST'>
                    <input type='hidden' name='_method' value='DELETE'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to delete?\")'
                    class='btn btn-danger btn-sm'
                    >Del</button>
                    </form>";
                $viewAction = "<a href='{$viewUrl}'
                    class='btn btn-warning btn-sm'
                    target='_blank'
                    >View</a>";
                $restoreAction = "<form action='{$restoreUrl}' method='POST'>
                    <input type='hidden' name='_method' value='PATCH'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to restore?\")'
                    class='btn btn-success btn-sm'
                    >Restore</button>
                    </form>";
                if ($post->deleted_at !== null) {
                    return $restoreAction;
                }
                return "{$editAction}{$deleteAction}{$viewAction}";
            })
            ->setRowClass(function ($post) {
                return $post->deleted_at ? 'soft-delete' : '';
            })
            ->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->postRepo->byId($id);
        $categories = $this->categoryRepo->allChidren();
        return view('admin.posts.edit', [
            'post' => $post,
            'categories' => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'required|numeric',
            'title'       => 'required',
        ]);

        $inputs = [
            'category_id' => $request->input('category_id'),
            'title' => $request->input('title'),
            'body' => $request->input('body'),
            'nsfw' => $request->has('nsfw'),
            'active' => $request->has('active'),
            'status' => $request->input('status'),
        ];

        $postToCompare = $this->postRepo->byId($id);

        if ($postToCompare->status != $request->input('status')) {
            $inputs['status_at'] = Carbon::now();
        }

        $post = $this->postRepo->updateById($id, $inputs);
        return redirect()->back()->with('message', "Post id {$id} updated!");
        ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->postRepo->deleteById($id);
        return back();
    }

    public function restore($id)
    {
        $this->postRepo->restoreSoftDeletedById($id);
        return back();
    }
}
