<?php

namespace Thaiissue\Admin\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Comment\CommentRepoInterface;
use Thaiissue\Repositories\Category\CategoryRepoInterface;

class CommentController extends Controller
{
    protected $commentRepo;
    protected $categoryRepo;


    public function __construct(
        CommentRepoInterface $commentRepo,
        CategoryRepoInterface $categoryRepo
    ) {
        $this->commentRepo = $commentRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = $this->commentRepo->paginate(20);

        return view('admin.comments.index')->with([
            'comments' => $comments,
        ]);
    }

    public function commentDatatables()
    {
        $builder = \DB::table('comments')
            ->select([
                'comments.*',
                'users.name',
                \DB::raw('categories.title as category_title'),
                \DB::raw('categories.slug as category_slug')
            ])
            ->leftJoin('users', 'users.id', '=', 'comments.user_id')
            ->leftJoin('categories', 'categories.id', '=', 'comments.category_id')
            ->orderBy('id', 'desc');

        return DataTables::of($builder)
            ->editColumn('body', function ($comment) {
                return str_limit($comment->body, 150);
            })
            ->editColumn('thumbnail', function ($post) {
                try {
                    return \Storage::url($post->thumbnail);
                } catch (\Exception $e) {
                    return null;
                }
            })
            ->addColumn('user', function ($comment) {
                if ($comment->user_id) {
                    return "{$comment->user_id}: {$comment->name}";
                }
                return $comment->user_id;
            })
            ->addColumn('action', function ($comment) {
                $editUrl = route('admin-comments-edit', ['comment_id' => $comment->id]);
                $destroyUrl = route('admin-comments-destroy', ['comment_id' => $comment->id]);
                $viewUrl = route('comments-show-without-category-slug', ['comment_order_no' => $comment->order_no, 'post_id' => $comment->post_id]);
                $restoreUrl = route('admin-comments-restore', ['comment_id' => $comment->id]);
                $csrfToken = request()->session()->token();
                $editAction = "<a href='{$editUrl}'
                    class='btn btn-primary btn-sm'
                    >Edit</a>";
                $deleteAction = "<form action='{$destroyUrl}' method='POST'>
                    <input type='hidden' name='_method' value='DELETE'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to delete?\")'
                    class='btn btn-danger btn-sm'
                    >Del</button>
                    </form>";
                $viewAction = "<a href='{$viewUrl}'
                    class='btn btn-warning btn-sm'
                    target='_blank'
                    >View</a>";
                $restoreAction = "<form action='{$restoreUrl}' method='POST'>
                    <input type='hidden' name='_method' value='PATCH'>
                    <input type='hidden' name='_token' value='{$csrfToken}''>
                    <button
                    onclick='return confirm(\"Are you sure to restore?\")'
                    class='btn btn-success btn-sm'
                    >Restore</button>
                    </form>";
                if ($comment->deleted_at !== null) {
                    return $restoreAction;
                }
                return "{$editAction}{$deleteAction}{$viewAction}";
            })
            ->setRowClass(function ($comment) {
                return $comment->deleted_at ? 'soft-delete' : '';
            })
            ->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = $this->commentRepo->byId($id);
        $categories = $this->categoryRepo->allChidren();
        return view('admin.comments.edit', [
            'comment' => $comment,
            'categories' => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'required|numeric',
            'body'       => 'required',
        ]);

        $inputs = [
            'category_id' => $request->input('category_id'),
            'body' => $request->input('body'),
            'nsfw' => $request->has('nsfw'),
            'active' => $request->has('active'),
        ];

        $commentToCompare = $this->commentRepo->byId($id);

        $comment = $this->commentRepo->updateById($id, $inputs);
        return redirect()->back()->with('message', 'Comment updated!');
        ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->commentRepo->deleteById($id);
        return back();
    }

    public function restore($id)
    {
        $this->commentRepo->restoreSoftDeletedById($id);
        return back();
    }
}
