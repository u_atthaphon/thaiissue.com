<?php

namespace Thaiissue\Resources\Post;

use Illuminate\Http\Resources\Json\JsonResource;
use Thaiissue\Resources\Comment\CommentResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_category' => $this->category->parent->slug,
            'category_id' => $this->category_id,
            'is_admin_posted' => is_admin($this->user),
            'title' => $this->title,
            'body' => $this->transform->body,
            'youtube_embeds' => $this->transform->youtubeEmbeds,
            'thumbnail' => $this->thumbnail,
            'image' => $this->image,
            'nsfw' => $this->nsfw,
            'active' => $this->active,
            'comments_count' => $this->cache_comments_count,
            'comments_images_count' => $this->commentImageCount($this),
            'views' => $this->views,
            'ip' => $this->ip,
            'hash_ip' => $this->hash_ip,
            'routes' => [
                'show' => $this->url->show,
            ],
            'comments' => CommentResource::collection($this->whenLoaded('comments')),
            'visits' => $this->visits()->count(),
            'is_favorited' => $this->isFavorited,
            'created_at' => $this->created_at_th,
            'updated_at' => $this->updated_at_th,
        ];
    }

    public function commentImageCount($post)
    {
        $one = !empty($post->image) ? 1 : 0;
        return $post->cache_comments_images_count + $one;
    }
}
