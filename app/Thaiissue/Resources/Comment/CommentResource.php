<?php

namespace Thaiissue\Resources\Comment;

use Illuminate\Http\Resources\Json\JsonResource;
use Thaiissue\Resources\Post\PostResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_category' => $this->category->parent->slug,
            'category_id' => $this->category_id,
            'is_admin_posted' => is_admin($this->user),
            'post' => new PostResource($this->post),
            'order_no' => $this->order_no,
            'body' => $this->transform->body,
            'youtube_embeds' => $this->transform->youtubeEmbeds,
            'thumbnail' => $this->thumbnail,
            'image' => $this->image,
            'nsfw' => $this->nsfw,
            'active' => $this->active,
            'ip' => $this->ip,
            'hash_ip' => $this->hash_ip,
            'routes' => [
                'show_specific_comment' => $this->url->show,
            ],
            'is_favorited' => $this->isFavorited,
            'created_at' => $this->created_at_th,
            'updated_at' => $this->updated_at_th,
        ];
    }
}
