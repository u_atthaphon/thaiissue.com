<?php

namespace Thaiissue\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class ChildrenResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'order_no' => $this->order_no,
            'title' => $this->title,
            'body' => $this->body,
            'slug' => $this->slug,
            'views' => $this->views,
            'routes' => [
                'show' => $this->url->show,
            ],

        ];
    }
}
