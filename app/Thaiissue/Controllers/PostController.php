<?php

namespace Thaiissue\Controllers;

use Thaiissue\Requests\NewPost;
use Thaiissue\Repositories\Post\PostRepoInterface;
use Thaiissue\Repositories\Post\SavePostRepoInterface;
use Thaiissue\Repositories\Category\CategoryRepoInterface;

class PostController extends MyController
{
    protected $postRepo;
    protected $categoryRepo;
    protected $savePostRepo;

    public function __construct(
        PostRepoInterface $postRepo,
        CategoryRepoInterface $categoryRepo,
        SavePostRepoInterface $savePostRepo
    ) {
        parent::__construct();
        $this->postRepo = $postRepo;
        $this->categoryRepo = $categoryRepo;
        $this->savePostRepo = $savePostRepo;
    }

    public function show($postId, $categorySlug)
    {
        if (empty($categorySlug) || empty($postId)) {
            abort(404);
        }

        $postExists = $this->postRepo->isExistsById($postId);
        $category = $this->categoryRepo->bySlug($categorySlug);
        if (!$postExists || !$category) {
            abort(404);
        }

        $post = $this->postRepo->byId($postId);
        $this->metaTags([
            'title' => 'เปิดประเด็น - '.\Lang::get('seo.title_with_value', [
                'value' => $category->title
            ])." - {$post->title}",
            'description' => $post->body,
            'image' => $post->image ?? \URL::to($category->cover),
        ]);

        $this->postRepo->incrementVisitsById($postId);

        return view('posts.show', [
            'postId' => $postId,
            'category' => $category,
        ]);
    }

    public function store(NewPost $request)
    {
        if ($this->isSpam($request)) {
            $meta = [
                'info' => 'post in category',
                'category_id' => $request->input('categoryId'),
            ];
            $this->suspected($request, $meta);
            return redirect('/');
        }

        $categoryId = $request->input('categoryId');
        $userId = \Auth::check() ? \Auth::user()->id : null;
        $postTitle = $request->input('postTitle');
        $postBody = strip_tags($request->input('postBody'));
        $postImg = $request->file('image');
        $ip = thaiissue_get_ip();
        $hashIp = \Hashids::encode(only_number($ip));
        $inputs = [
            'order_no' => null,
            'category_id' => $categoryId,
            'user_id' => $userId,
            'ip' => $ip,
            'hash_ip' => $hashIp,
            'title' => $postTitle,
            'body' => $postBody,
            'image' => null,
            'thumbnail' => null,
        ];
        // Save Post
        $post = $this->savePostRepo->createPost($inputs, $postImg);
        $redirectTo = $post->url->show;

        return redirect($redirectTo);
    }
}
