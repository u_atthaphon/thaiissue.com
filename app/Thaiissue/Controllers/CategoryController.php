<?php

namespace Thaiissue\Controllers;

use Thaiissue\Repositories\Category\CategoryRepoInterface;

class CategoryController extends MyController
{
    protected $categoryRepo;

    public function __construct(
        CategoryRepoInterface $categoryRepo
    ) {
        $this->categoryRepo = $categoryRepo;
    }

    public function show($slug)
    {
        $category = $this->categoryRepo->notParentBySlug($slug);
        $this->metaTags([
            'title' => 'ประเด็นมาใหม่ - '.\Lang::get('seo.title_with_value', [
                'value' => $category->title
            ]),
            'image' => \URL::to($category->cover),
        ]);

        if (empty($category)) {
            abort(404);
        }

        return view('categories.show', ['category' => $category]);
    }

    public function showComments($slug)
    {
        $category = $this->categoryRepo->notParentBySlug($slug);
        $this->metaTags([
            'title' => 'คอมเมนต์มาใหม่ - '.\Lang::get('seo.title_with_value', [
                'value' => $category->title
            ]),
            'image' => $post->image ?? \URL::to($category->cover),
        ]);

        if (empty($category)) {
            abort(404);
        }

        return view('categories.show-comments', ['category' => $category]);
    }

    // public function showLinks($slug)
    // {
    //     $category = $this->categoryRepo->notParentBySlug($slug);

    //     if (empty($category)) {
    //         abort(404);
    //     }

    //     return view('categories.show-links', ['category' => $category]);
    // }

    public function createPost($slug)
    {
        $category = $this->categoryRepo->notParentBySlug($slug);
        $this->metaTags([
            'title' => 'เปิดประเด็น - '.\Lang::get('seo.title_with_value', [
                'value' => $category->title
            ])
        ]);

        if (empty($category)) {
            abort(404);
        }

        return view('posts.create', ['category' => $category]);
    }
}
