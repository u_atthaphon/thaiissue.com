<?php

namespace Thaiissue\Controllers;

use Thaiissue\Enums\Provider as ProviderEnum;
use Thaiissue\Enums\Role as RoleEnum;
use Thaiissue\Repositories\User\SocialiteRepoInterface;
use App\Http\Controllers\Controller;

class SocialiteController extends Controller
{
    protected $allowedProvider = [];
    protected $redirectTo = '/';
    protected $socialiteRepo;

    public function __construct(
        SocialiteRepoInterface $socialiteRepo
    ) {
        $this->allowedProvider = ProviderEnum::all();
        $this->socialiteRepo = $socialiteRepo;
    }

    public function redirectTo()
    {
        return \Session::has('redirectTo') ? \Session::get('redirectTo') : $this->redirectTo;
    }

    /**
     * Redirect the user to the Provider authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        if (!in_array($provider, $this->allowedProvider)) {
            throw new \Exception(
                \Lang::get('error.socialite-not-support', ['provider'=>$provider])
            );
        }
        return \Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        if (!in_array($provider, $this->allowedProvider)) {
            throw new \Exception(
                \Lang::get('error.socialite-not-support', ['provider'=>$provider])
            );
        }

        $data = \Socialite::driver($provider)->stateless()->user();

        $this->loginOrCreate($provider, $data);

        return redirect($this->redirectTo());
    }

    protected function loginOrCreate($provider, $data)
    {
        $user = $this->socialiteRepo->firstByProvider($provider, $data->id);
        if (empty($user)) {
            $method = "{$provider}CreateUser";
            $user = $this->$method($data);
        }

        \Auth::login($user);
    }

    protected function prepareInputs($data, $provider)
    {
        $avatar = "";
        if (isset($data->avatar) && !empty($data->avatar)) {
            $avatar = $this->socialiteRepo->uploadByAvatarUrl($data->avatar);
        }

        return [
            'name' => $data->name,
            'email' => $data->email,
            'avatar' =>  $avatar,
            'provider' => $provider,
            'provider_id' =>  $data->id,
        ];
    }

    private function googleCreateUser($data)
    {
        $user = $this->socialiteRepo->create(
            $this->prepareInputs($data, ProviderEnum::GOOGLE)
        );
        $userRoleId = array_flip(RoleEnum::all())[RoleEnum::USER];
        $user->roles()->attach($userRoleId);

        return $user;
    }

    private function facebookCreateUser($data)
    {
        $user = $this->socialiteRepo->create(
            $this->prepareInputs($data, ProviderEnum::FACEBOOK)
        );
        $userRoleId = array_flip(RoleEnum::all())[RoleEnum::USER];
        $user->roles()->attach($userRoleId);

        return $user;
    }
}
