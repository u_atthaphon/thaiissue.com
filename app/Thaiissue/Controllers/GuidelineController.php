<?php

namespace Thaiissue\Controllers;

class GuidelineController extends MyController
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->metaTags([
            'title' => \Lang::get('layouts.footer-guildeline').' - '.\Lang::get('seo.title'),
        ]);
        $guildelines = [
            'ช่วยจัดรูปแบบ' => [
                'เราสามารถ อ้างถึงคอมเมนต์ในประเด็นเดียวกันได้โดย <code>>>{เลขคอมเมนต์}</code> ตัวอย่าง <a class="text-app-primary">>>2</a>',
                'เราสามารถ อ้างถึงคอมเมนต์ในประเด็นอื่นได้โดย <code>>>>/{เลขประเด็น}/{เลขคอมเมนต์}</code> ตัวอย่าง <a class="text-app-primary">>>>1/1</a>',
                'เราสามารถ เน้นข้อความได้โดย <code>*{ข้อความที่ต้องการเน้น}*</code> ตัวอย่าง <blockquote class="blockquote text-app-primary-dark">*คนไทยโนเนม - ไทยอิชชู่*</blockquote>',
            ]
        ];

        return view('guidelines.index', ['guidelines' => $guildelines]);
    }
}
