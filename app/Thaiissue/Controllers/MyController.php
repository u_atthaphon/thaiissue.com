<?php

namespace Thaiissue\Controllers;

use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Ban\BanRepoInterface;
use Thaiissue\Repositories\Suspect\SuspectRepoInterface;

class MyController extends Controller
{
    protected $suspectRepo;
    protected $banRepo;

    public function __construct()
    {
        $this->suspectRepo = app(SuspectRepoInterface::class);
        $this->banRepo = app(BanRepoInterface::class);
    }

    public function metaTags(array $metaTags = [])
    {
        $defaultMetaTags = [
            'title'               => \Lang::get('seo.title'),
            'description'         => \Lang::get('seo.description'),
            'keywords'            => \Lang::get('seo.keywords'),
            'url'                 => \URL::to("/artists"),
            'property'            => '',
            'image'               => \URL::to('storage/cover.png'),
            'images'              => [],
            'twitter_site'        => '',
            'twitter_Large_image' => true,
            'prev'                => '',
            'next'                => '',
        ];
        foreach ($metaTags as $name => $content) {
            if (array_key_exists($name, $defaultMetaTags)) {
                $defaultMetaTags[$name] = $content;
            }
        }

        meta_tags($defaultMetaTags);
    }

    public function isSpam($request)
    {
        // Input field name "checkname" must be hidden field
        // We guess, if bot fill the form it will fill this hidden input
        return $request->input('checkname') && !empty($request->input('checkname'))?? false;
    }

    public function suspected($request, $meta = null)
    {
        $ip = real_ip();
        $agent = $request->userAgent();
        $this->suspectRepo->addSuspect($ip, $agent, $meta);
        if ($this->suspectRepo->shouldBanIp($ip)) {
            $this->banRepo->ban($ip, 1, 'Ban from check spam');
        }
    }
}
