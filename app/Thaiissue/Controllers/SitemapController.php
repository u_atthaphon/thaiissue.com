<?php

namespace Thaiissue\Controllers;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Post\PostRepoInterface;
use Thaiissue\Repositories\Comment\CommentRepoInterface;
use Thaiissue\Repositories\Category\CategoryRepoInterface;

class SitemapController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        // create new sitemap object
        $sitemap = \App::make('sitemap');
        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        // $sitemap->setCache('laravel.sitemap', 60);
        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {
            // add item to the sitemap (url, date, priority, freq)
            $this->homepage($sitemap);
            $this->ethices($sitemap);
            $this->categories($sitemap);
            // $this->posts($sitemap);
            // $this->comments($sitemap);
        }
        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');
    }

    public function indexPost()
    {
        // create new sitemap object
        $sitemap = \App::make('sitemap');
        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        $sitemap->setCache('laravel.sitemap.post', 1440);
        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {
            // add item to the sitemap (url, date, priority, freq)
            $this->posts($sitemap);
            // $this->comments($sitemap);
        }
        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');
    }

    public function indexComment()
    {
        // create new sitemap object
        $sitemap = \App::make('sitemap');
        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        // by default cache is disabled
        $sitemap->setCache('laravel.sitemap.comment', 1440);
        // check if there is cached sitemap and build new only if is not
        if (!$sitemap->isCached()) {
            // add item to the sitemap (url, date, priority, freq)
            $this->comments($sitemap);
        }
        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');
    }

    protected function homepage($sitemap)
    {
        $sitemap->add(
            \URL::to('/'),
            Carbon::create(2018, 8, 12, 0, 0, 0),
            '0.9',
            'daily'
        );
    }

    protected function ethices($sitemap)
    {
        $sitemap->add(
            \URL::to('/ethices'),
            Carbon::create(2018, 8, 12, 0, 0, 0),
            '0.9',
            'daily'
        );
    }

    protected function categories($sitemap)
    {
        $categoryRepo = app(CategoryRepoInterface::class);
        $categories = $categoryRepo->allChidren();
        foreach ($categories as $catgory) {
            $sitemap->add(
                $catgory->url->show(),
                Carbon::create(2018, 5, 25, 0, 0, 0),
                '0.5',
                'daily'
            );
            $sitemap->add(
                $catgory->url->showComments(),
                Carbon::create(2018, 5, 25, 0, 0, 0),
                '0.5',
                'daily'
            );
        }
    }

    protected function posts($sitemap)
    {
        $postRepo = app(PostRepoInterface::class);
        $posts = $postRepo->allActive();
        foreach ($posts as $post) {
            $sitemap->add(
                $post->url->show(),
                $post->created_at,
                '0.5',
                'daily'
            );
        }
    }

    // it cause error Allowed memory size of 134217728 bytes exhausted (tried to allocate 16781312 bytes) in local development
    protected function comments($sitemap)
    {
        $commentRepo = app(CommentRepoInterface::class);
        $comments = $commentRepo->allActive();
        foreach ($comments->chunk(10) as $chunk) {
            foreach ($chunk as $comment) {
                $sitemap->add(
                    $comment->url->show(),
                    $comment->created_at,
                    '0.5',
                    'daily'
                );
            }
        }
    }
}
