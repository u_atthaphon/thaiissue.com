<?php

namespace Thaiissue\Controllers;

use Thaiissue\Requests\NewComment;
use Thaiissue\Repositories\Post\PostRepoInterface;
use Thaiissue\Repositories\Comment\CommentRepoInterface;
use Thaiissue\Repositories\Category\CategoryRepoInterface;
use Thaiissue\Repositories\Comment\SaveCommentRepoInterface;

class CommentController extends MyController
{
    protected $categoryRepo;
    protected $commentRepo;
    protected $saveCommentRepo;
    protected $postRepo;

    public function __construct(
        CategoryRepoInterface $categoryRepo,
        CommentRepoInterface $commentRepo,
        SaveCommentRepoInterface $saveCommentRepo,
        PostRepoInterface $postRepo
    ) {
        parent::__construct();
        $this->categoryRepo = $categoryRepo;
        $this->commentRepo = $commentRepo;
        $this->saveCommentRepo = $saveCommentRepo;
        $this->postRepo = $postRepo;
    }

    public function show($commentOrderNo, $postId, $categorySlug)
    {
        $category = $this->categoryRepo->bySlug($categorySlug);
        $post = $this->postRepo->byId($postId);
        $comment = $this->commentRepo->byOrderNoAndPostId($commentOrderNo, $postId);

        $this->metaTags([
            'title' => 'คอมเมนต์ - '.\Lang::get('seo.title_with_value', [
                'value' => $category->title
            ])." - {$comment->body}",
            'description' => \Lang::get('comment_post', [
                $comment->body,
                $post->title
            ]),
            'image' => $comment->image ?? $post->image ?? \URL::to($category->cover),
        ]);

        if (empty($category)) {
            abort(404);
        }

        return view('comments.show', [
            'commentOrderNo' => $commentOrderNo,
            'postId' => $postId,
            'category' => $category,
        ]);
    }

    public function showWithoutCategorySlug($commentOrderNo, $postId)
    {
        $post = $this->postRepo->byId($postId);
        $comment = $this->commentRepo->byOrderNoAndPostId($commentOrderNo, $postId);

        if (empty($comment)) {
            abort(404);
        }

        $this->metaTags([
            'title' => 'คอมเมนต์ - '.\Lang::get('seo.title')." - {$comment->body}",
            'description' => \Lang::get('comment_post', [
                $comment->body,
                $post->title
            ]),
            'image' => $comment->image ?? $post->image ?? \URL::to($category->cover),
        ]);

        return view('comments.show', [
            'commentOrderNo' => $commentOrderNo,
            'postId' => $postId,
            'category' => $comment->category,
        ]);
    }

    public function store(NewComment $request)
    {
        if ($this->isSpam($request)) {
            $meta = [
                'info' => 'comment in post',
                'category_id' => $request->input('categoryId'),
                'post_id' => $request->input('postId'),
            ];
            $this->suspected($request, $meta);
            return redirect('/');
        }

        $categoryId = $request->input('categoryId');
        $postId = $request->input('postId');
        $userId = \Auth::check() ? \Auth::user()->id : null;
        $body = strip_tags($request->input('body'));
        $image = $request->file('image');
        $ip = thaiissue_get_ip();
        $hashIp = \Hashids::encode(only_number($ip));
        $orderNo = $this->commentRepo->latestOrderNoByPostId($postId) + 1;
        $inputs = [
            'order_no' => $orderNo,
            'post_id' => $postId,
            'category_id' => $categoryId,
            'user_id' => $userId,
            'ip' => $ip,
            'hash_ip' => $hashIp,
            'body' => $body,
            'image' => null,
            'thumbnail' => null,
        ];
        // Save Comment
        $comment = $this->saveCommentRepo->createComment($inputs, $image);
        $redirectTo = $comment->url->show;

        return redirect($redirectTo);
    }
}
