<?php

namespace Thaiissue\Auth\Controllers;

use Thaiissue\Controllers\MyController;

class UserController extends MyController
{
    public function __construct()
    {
        //
    }

    public function me()
    {
        $this->metaTags();
        return view('users.me');
    }

    public function favoritePosts()
    {
        $this->metaTags();
        return view('users.favorite-posts');
    }

    public function favoriteComments()
    {
        $this->metaTags();
        return view('users.favorite-comments');
    }

    public function historyPosts()
    {
        $this->metaTags();
        return view('users.history-posts');
    }

    public function historyComments()
    {
        $this->metaTags();
        return view('users.history-comments');
    }
}
