<?php

namespace Thaiissue\Api\Controllers;

use App\Http\Controllers\Controller;
use Thaiissue\Repositories\User\FavoriteRepoInterface;

class FavoriteController extends Controller
{
    protected $favoriteRepo;


    public function __construct(FavoriteRepoInterface $favoriteRepo)
    {
        $this->favoriteRepo = $favoriteRepo;
    }

    public function toggleFavoritePost($postId)
    {
        $isFavorited = $this->favoriteRepo->toggleFavoritePost($postId);
        return response()->json([
            'is_favorited' => $isFavorited,
        ]);
    }

    public function toggleFavoriteComment($commentId)
    {
        $isFavorited = $this->favoriteRepo->toggleFavoriteComment($commentId);
        return response()->json([
            'is_favorited' => $isFavorited,
        ]);
    }
}
