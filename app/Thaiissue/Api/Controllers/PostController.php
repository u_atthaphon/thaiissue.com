<?php

namespace Thaiissue\Api\Controllers;

use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Post\PostRepoInterface;
use Thaiissue\Repositories\Comment\CommentRepoInterface;
use Thaiissue\Resources\Post\PostResource;
use Thaiissue\Resources\Comment\CommentResource;

class PostController extends Controller
{
    protected $postRepo;
    protected $commentRepo;

    public function __construct(
        PostRepoInterface $postRepo,
        CommentRepoInterface $commentRepo
    ) {
        $this->postRepo = $postRepo;
        $this->commentRepo = $commentRepo;
    }

    public function show($id)
    {
        $post = $this->postRepo->whenStatusOpenById($id);
        return new PostResource($post);
    }

    public function showCommentsPaginateByPostId($id)
    {
        $comments = $this->commentRepo->paginateByPostId($id, 100);
        return CommentResource::collection($comments);
    }
}
