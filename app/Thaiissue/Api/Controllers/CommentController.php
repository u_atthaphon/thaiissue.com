<?php

namespace Thaiissue\Api\Controllers;

use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Comment\CommentRepoInterface;
use Thaiissue\Resources\Comment\CommentResource;

class CommentController extends Controller
{
    protected $commentRepo;

    public function __construct(
        CommentRepoInterface $commentRepo
    ) {
        $this->commentRepo = $commentRepo;
    }

    public function byCommentOrderNoAndPostId($commmentOrderNo, $postId)
    {
        $comment = $this->commentRepo->byOrderNoAndPostId($commmentOrderNo, $postId);
        return new CommentResource($comment);
    }
}
