<?php

namespace Thaiissue\Api\Controllers;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Thaiissue\Requests\ResetPassword;
use Thaiissue\Resources\Post\PostResource;
use Thaiissue\Resources\Comment\CommentResource;
use Thaiissue\Repositories\User\UserRepoInterface;

class UserController extends Controller
{
    protected $userRepo;


    public function __construct(
        UserRepoInterface $userRepo
    ) {
        $this->userRepo = $userRepo;
    }

    public function resetPassword(ResetPassword $request)
    {
        $password = $request->input('new_password');
        $user = $request->user();
        $user->password = \Hash::make($password);
        $user->setRememberToken(Str::random(60));
        $user->save();

        return response()->json([
            'status' => 'updated',
            'ok' => true,
        ]);
    }

    public function getFevoritePostPaginate()
    {
        $posts = $this->userRepo->favoritePostPaginateByUser(\Auth::user());
        return PostResource::collection($posts);
    }

    public function getFevoriteCommentPaginate()
    {
        $comments = $this->userRepo->favoriteCommentPaginateByUser(\Auth::user());
        return CommentResource::collection($comments);
    }

    public function getHistoryPostPaginate()
    {
        $posts = $this->userRepo->historyPostPaginateByUser(\Auth::user());
        return PostResource::collection($posts);
    }

    public function getHistoryCommentPaginate()
    {
        $comments = $this->userRepo->historyCommentPaginateByUser(\Auth::user());
        return CommentResource::collection($comments);
    }
}
