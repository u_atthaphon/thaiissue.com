<?php

namespace Thaiissue\Api\Controllers;

use App\Http\Controllers\Controller;
use Thaiissue\Repositories\Category\CategoryRepoInterface;
use Thaiissue\Resources\Category\CategoryResource;
use Thaiissue\Repositories\Post\PostRepoInterface;
use Thaiissue\Resources\Post\PostResource;
use Thaiissue\Repositories\Comment\CommentRepoInterface;
use Thaiissue\Resources\Comment\CommentResource;

class CategoryController extends Controller
{
    protected $categoryRepo;
    protected $postRepo;
    protected $commentRepo;

    public function __construct(
        CategoryRepoInterface $categoryRepo,
        PostRepoInterface $postRepo,
        CommentRepoInterface $commentRepo
    ) {
        $this->categoryRepo = $categoryRepo;
        $this->postRepo = $postRepo;
        $this->commentRepo =$commentRepo;
    }

    public function index()
    {
        $categories = $this->categoryRepo->getAllParentWithchildren();
        return CategoryResource::collection($categories);
    }

    public function showPostsPaginateByCategoryId($id)
    {
        $posts = $this->postRepo->paginateByCategoryId($id, 10);
        return PostResource::collection($posts);
    }

    public function showCommentsPaginateByCategoryId($id)
    {
        $comments = $this->commentRepo->paginateByCategoryId($id);
        return CommentResource::collection($comments);
    }
}
