<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearExpiredToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thaiissue:clear-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete expired token...';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Check expired access token...');
        $expiredTokens = \DB::table('oauth_access_tokens')
            ->where('expires_at', '<', \Carbon\Carbon::now())
            ->delete();
        $this->info("Deleted {$expiredTokens} expired access tokens ...");
    }
}
