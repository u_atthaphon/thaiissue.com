<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $thaiissueNamespace = 'Thaiissue\Controllers';
    protected $thaiissueApiNamespace = 'Thaiissue\Api\Controllers';
    protected $thaiissueAuthNamespace = 'Thaiissue\Auth\Controllers';
    protected $thaiissueAdminNamespace = 'Thaiissue\Admin\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapThaissueRoutes();

        $this->mapThaissueApiRoutes();

        $this->mapThaissueAuthRoutes();

        $this->mapThaissueAdminRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    protected function mapThaissueRoutes()
    {
        Route::middleware(['web', 'create_client_token'])
             ->namespace($this->thaiissueNamespace)
             ->group(base_path('routes/thaiissue_web.php'));
    }

    protected function mapThaissueApiRoutes()
    {
        Route::prefix('api')
             ->middleware([
                'web',
                'client_credentials',
        ])
             ->namespace($this->thaiissueApiNamespace)
             ->group(base_path('routes/thaiissue_api.php'));
    }

    protected function mapThaissueAuthRoutes()
    {
        Route::middleware([
            'web',
            'auth',
        ])
             ->namespace($this->thaiissueAuthNamespace)
             ->group(base_path('routes/thaiissue_auth.php'));
    }

    protected function mapThaissueAdminRoutes()
    {
        Route::prefix('admin')
            ->middleware([
                'web_admin',
                'auth',
                'admin',
            ])
             ->namespace($this->thaiissueAdminNamespace)
             ->group(base_path('routes/thaiissue_admin.php'));
    }
}
