<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Thaiissue\Enums\Provider as ProviderEnum;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        \Session::put('redirectTo', \URL::previous()); // for socialite
        $this->redirectTo = \Url::previous();
        $this->middleware('guest')->except('logout');
    }

    /**
     * Override from AuthenticatesUsers trait
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $extendeRquest = $request->only($this->username(), 'password');
        $extendeRquest['provider'] = ProviderEnum::EMAIL;

        return $extendeRquest;
    }

    public function redirectTo()
    {
        return \Session::has('redirectTo') ? \Session::get('redirectTo') : $this->redirectTo;
    }
}
