<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseSetupTrait;
    use ExceptionSetupTrait;

    public function __construct(){
        parent::__construct();
        // Overriding the constructor so need to reinstantiate
        $this->createApplication();
        $this->setUp();
        //Constructor code here
    }

    protected function setUp()
    {
        parent::setUp();
        $this->setupDatabase();
        $this->disableExceptionHandling();
    }
}
