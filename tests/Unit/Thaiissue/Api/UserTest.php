test
<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use WithoutMiddleware;
    // use DatabaseTransactions;

    /** @test */
    public function test_reset_password_required_all_fields()
    {
        $user = factory(Thaiissue\Models\User::class)->create([
             'email' => 'john@example.com',
             'password' => bcrypt('testpass123')
        ]);
        \Auth::login($user);

        $inputs = [
            'current_password' => 'password',
            'new_password' => 'password',
            'new_password_confirmation' => 'password',
        ];

        $this->json('POST', '/api/users/reset-password', $inputs)
             ->assertJson([
                 'updated' => true,
             ]);
    }
}
