<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryApiTest extends TestCase
{
    /** @test */
    function test_fetches_all_parent_with_children()
    {
        $response = $this->get('api/categories');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'parent_id',
                    'order_no',
                    'title',
                    'body',
                    'slug',
                    'active',
                    'children',
                ],
            ]
        ]);
    }
}
