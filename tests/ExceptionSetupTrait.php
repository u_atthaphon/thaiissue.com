<?php

namespace Tests;

use Exception;
use App\Exceptions\Handler;
use Illuminate\Contracts\Debug\ExceptionHandler;

trait ExceptionSetupTrait
{
    // Framework-supplied test case methods snipped for brevity
    // This function working with PHP 7+
    protected function disableExceptionHandling()
    {
        $this->app->instance(ExceptionHandler::class, new class extends Handler {
            public function __construct() {}

            public function report(Exception $exception)
            {
                parent::report($exception);
            }

            public function render($request, Exception $exception)
            {
                throw $exception;
            }
        });
    }
}
