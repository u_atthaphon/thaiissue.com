<?php

use Faker\Generator as Faker;;
use Thaiissue\Enums\Status as StatusEnum;

$factory->define(Thaiissue\Models\Post::class, function (Faker $faker) {
    $faker->region = 'th_TH';
    $ip = $faker->ipv4;
    $hashIp = \Hashids::encode(str_replace(['.', ':'], '', $ip));
    $category = new Thaiissue\Models\Category;
    $category = $category->whereNotNull('parent_id')->inRandomOrder()->first();
    $image = rand(0, 1) ? "https://robohash.org/{$faker->uuid}.png" : null;
    $thumbnail = !empty($image) ? $image.'?size=150x150' : null;
    $image = !empty($image) ? $image.'?size=600x600' : null;
    return [
        'category_id' => $category->id,
        'ip' => $ip,
        'hash_ip' => $hashIp,
        'title' => $faker->text(100),
        'body' => $faker->sentence,
        'thumbnail' => $thumbnail,
        'image' => $image,
        'status' => StatusEnum::OPEN,
    ];
});
