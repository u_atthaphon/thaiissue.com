<?php

use Faker\Generator as Faker;

$factory->define(Thaiissue\Models\Comment::class, function (Faker $faker) {
    $faker->region = 'th_TH';
    $ip = $faker->ipv4;
    $hashIp = \Hashids::encode(str_replace(['.', ':'], '', $ip));
    $category = new Thaiissue\Models\Category;
    $category = $category->whereNotNull('parent_id')->inRandomOrder()->first();
    $post = new Thaiissue\Models\Post;
    $post = $post->withCount('comments')->inRandomOrder()->first();
    $image = rand(0, 1) ? "https://robohash.org/{$faker->uuid}.png" : null;
    $thumbnail = !empty($image) ? $image.'?size=150x150' : null;
    $image = !empty($image) ? $image.'?size=600x600' : null;
    $commentNo = $post->comments()->count() + 1;
    return [
        'category_id' => $category->id,
        'post_id' => $post->id,
        'order_no' => $commentNo,
        'ip' => $ip,
        'hash_ip' => $hashIp,
        'body' => $faker->sentence,
        'thumbnail' => $thumbnail,
        'image' => $image,
    ];
});
