<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThaiissueTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->integer('order_no')->nullable();
            $table->string('title');
            $table->text('body')->nullable();
            $table->string('slug')->unique();
            $table->string('cover')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('title', 250)->nullable();
            $table->text('body')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('image')->nullable();
            $table->boolean('nsfw')->default(false);
            $table->boolean('active')->default(true);
            $table->json('meta')->nullable();
            $table->string('ip', 45);
            $table->string('hash_ip');
            $table->enum('status', ['open','locked','archived']);
            $table->timestamp('status_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('post_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->integer('order_no');
            $table->text('body');
            $table->string('thumbnail')->nullable();
            $table->string('image')->nullable();
            $table->boolean('nsfw')->default(false);
            $table->boolean('active')->default(true);
            $table->json('meta')->nullable();
            $table->string('ip', 45);
            $table->string('hash_ip');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('slug')->unique();
            $table->jsonb('permissions')->nallable(); // jsonb deletes duplicates
        });

        Schema::create('user_roles', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('role_id');
            $table->unique(['user_id','role_id']);
            $table->timestamps();
        });

        Schema::create('favoriteables', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->index();
            $table->string('favoriteable_id', 36);
            $table->string('favoriteable_type', 255);
            $table->timestamp('created_at')->useCurrent();
            $table->unique([
                'user_id',
                'favoriteable_id',
                'favoriteable_type',
            ], 'favoriteable_unique');
        });

        // check suspects user before add ip to spam
        Schema::create('suspects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->string('agent')->nullable();
            $table->json('meta')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        Schema::create('bans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('reason')->nullable();
            $table->boolean('ban')
                ->default(true)
                ->comment('if ban is true, mean your are banned, force cancle ban by set it to false');
            $table->enum('level', ['1','2','3','4'])
                ->default('1')
                ->comment('lvl1 = 30 minutes, lvl2 = 2 hours, lvl3 = 24 hours, lvl4 = 3 days');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('bans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('reason')->nullable();
            $table->boolean('ban')
                ->default(true)
                ->comment('if ban is true, mean your are banned, force cancle ban by set it to false');
            $table->enum('level', ['1','2','3','4'])
                ->default('1')
                ->comment('lvl1 = 30 minutes, lvl2 = 2 hours, lvl3 = 24 hours, lvl4 = 3 days');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('expired_at')->nullable();
            $table->softDeletes();
        });

        Schema::create('news_links', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('category', [
                'business', 'entertainment', 'health',
                'science', 'sports', 'technology',
            ])->default('business');
            $table->string('domain')->nullable();
            $table->string('author')->nullable();
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('url')->nullable();
            $table->string('image')->nullable();
            $table->text('content')->nullable();
            $table->enum('added_by', ['newsapi', 'backoffice', 'user'])->default('newsapi');
            $table->timestamp('published_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();

            $table->unique('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('posts');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('user_roles');
        Schema::dropIfExists('favoriteables');
        Schema::dropIfExists('suspects');
        Schema::dropIfExists('bans');
        Schema::dropIfExists('news_links');
    }
}
