<?php

use Illuminate\Database\Seeder;

use Thaiissue\Models\User;
use Thaiissue\Enums\Role as RoleEnum;

class UsersTableSeeder extends Seeder
{
    protected $users = [
        [
            'name' => 'jingjo',
            'email' => 'u.atthaphon@gmail.com',
            'password' => 'jingjo_deego123',
            'avatar' => null,
            'provider' => 'email',
            'provider_id' => null,
        ],
        [
            'name' => 'deego',
            'email' => 'deego.pix@gmail.com',
            'password' => 'jingjo_deego123',
            'avatar' => null,
            'provider' => 'email',
            'provider_id' => null,
        ],
        [
            'name' => 'Atthaphon Urairat',
            'email' => 'u.atthaphon@gmail.com',
            'password' => '',
            'avatar' => null,
            'provider' => 'google',
            'provider_id' => '115108040373902738616',
        ],
        [
            'name' => 'Sarawut Urairat',
            'email' => 'deego.pix@gmail.com',
            'password' => '',
            'avatar' => null,
            'provider' => 'google',
            'provider_id' => '115337683092044947315',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminId = array_flip(RoleEnum::all())[RoleEnum::ADMIN];
        $masterId = array_flip(RoleEnum::all())[RoleEnum::MASTER];
        foreach ($this->users as $data) {
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => \Hash::make($data['password']),
                'avatar' => $data['avatar'],
                'provider' => $data['provider'],
                'provider_id' => $data['provider_id'],
            ]);

            if (in_array($user->name, ['Atthaphon Urairat', 'Sarawut Urairat'])) {
                $user->roles()->attach($masterId);
            } else {
                $user->roles()->attach($adminId);
            }
        }
    }
}
