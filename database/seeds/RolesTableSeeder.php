<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    protected $roles = [
        [
            'name' => 'admin',
            'description' => 'An admin',
            'slug' => 'admin',
            'permissions' => [
                'deletePost' => true,
            ],
        ],
        [
            'name' => 'user',
            'description' => 'Normal User',
            'slug' => 'user',
            'permissions' => [
                'viewPostHistory' => true,
            ],
        ],
        [
            'name' => 'master',
            'description' => 'Master User',
            'slug' => 'master',
            'permissions' => [
                'viewPostHistory' => true,
            ],
        ],
    ];

    public function __construct()
    {
        foreach ($this->roles as &$role) {
            $role['permissions'] = json_encode($role['permissions']);
        }
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert($this->roles);
    }
}
