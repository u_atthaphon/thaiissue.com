<?php

use Illuminate\Database\Seeder;
use Thaiissue\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    protected $categories = [
        [
            'title' => 'ทั่วไป',
            'order_no' => 1,
            'body' => null,
            'slug' => 'other',
            'sub' => [
                [
                    'title' => 'พูดคุยเรื่องทั่วไป',
                    'body' => '',
                    'slug' => 'general_discussion',
                    'cover'=> 'covers/general_discussion.png'
                ],
                [
                    'title' => 'การเมือง',
                    'body' => null,
                    'slug' => 'politics',
                    'cover'=> 'covers/politics.png'
                ],
                [
                    'title' => 'ปัญหาชีวิต',
                    'body' => 'ต้องการระบาย ต้องการคนรับฟัง มามา มาทางนี้',
                    'slug' => 'tough_life',
                    'cover'=> 'covers/tough_life.png'
                ],
            ],
        ],
        [
            'title' => 'ไทยอิชชู่',
            'order_no' => 2,
            'body' => 'บอร์ดสำหรับประกาศและติดตามความเคลื่อนไหวจากทางไทยอิชชู่ ',
            'slug' => 'thaiissue',
            'sub' => [
                [
                    'title' => 'ประกาศจากทีมงาน',
                    'body' => null,
                    'slug' => 'announcement',
                    'cover'=> 'covers/announcement.png'
                ],
            ],
        ],
        [
            'title' => 'อะเด้าท์',
            'order_no' => 3,
            'body' => null,
            'slug' => 'adult',
            'sub' => [
                [
                    'title' => 'เรื่องของอะเด้าท์',
                    'body' => 'เนื้อหาที่มีความรุนแรง หรือ เนื้อหา 18+ เด็กอายุน้อยกว่า 18 ไม่ควรเข้ามาห้องนี้',
                    'slug' => 'general_adult',
                    'cover'=> 'covers/general_adult.png'
                ],
            ],
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->categories as $category) {
            $cat = new Category;
            $cat->title = $category['title'];
            $cat->body = $category['body'];
            $cat->slug = $category['slug'];
            $cat->save();
            foreach ($category['sub'] as $subCategory) {
                $subCat = new Category;
                $subCat->parent_id = $cat->id;
                $subCat->title = $subCategory['title'];
                $subCat->body = $subCategory['body'];
                $subCat->slug = $subCategory['slug'];
                $subCat->cover = $subCategory['cover'];
                $subCat->save();
            }
        }
    }
}
