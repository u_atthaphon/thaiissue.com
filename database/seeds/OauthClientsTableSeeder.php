<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{
    protected $clients = [
        [
            'id'                     => 1,
            'user_id'                => null,
            'name'                   => 'Global Web Client Grant Type',
            'secret'                 => 'GwpYTy53SEJ09GtqrLJtEFMqt5FKbfaSE3Gphj0g',
            'redirect'               => 'http://thaiissue.localhost/oauth/callback',
            'personal_access_client' => 0,
            'password_client'        => 0,
            'revoked'                => 0,
        ],
        [
            'id'                     => 2,
            'user_id'                => null,
            'name'                   => 'Global Web Password Grant Type',
            'secret'                 => 'xWIERdQgF2voxNRhWKCFlc0WqXLukg7JKH0fNGy3',
            'redirect'               => 'http://thaiissue.localhost/oauth/callback',
            'personal_access_client' => 0,
            'password_client'        => 1,
            'revoked'                => 0,
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('oauth_clients')->insert($this->clients);
    }
}
