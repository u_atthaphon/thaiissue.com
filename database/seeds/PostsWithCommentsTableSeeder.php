<?php

use Illuminate\Database\Seeder;

class PostsWithCommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Thaiissue\Models\Post::class, 50)->create()->each(function ($post) {
            $randomNumber = rand(1, 100);
            if (!empty($post->image)) {
                $post->total_image++;
            }
            $post->save();
            // Comments
            $orderNo = 0;
            factory(Thaiissue\Models\Comment::class, $randomNumber)->create(
                [
                    'post_id' => $post->id,
                    'category_id' => $post->category_id,
                ]
            )
            ->each(function ($comment) use ($post, $orderNo) {
                $post->total_comment++;
                if (!empty($comment->image)) {
                    $post->total_image++;
                }
                $post->save();
                $comment->order_no = $post->total_comment;
                $comment->save();
            });
        });
    }
}
