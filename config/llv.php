<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Llv Locales Configuration
    |--------------------------------------------------------------------------
    |
    | The application locales determines the available locales that will be used
    | by the vue translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locales' => ['en'],

    /*
    |--------------------------------------------------------------------------
    | Llv Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the locales
    | are not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'th',

    'cache' => env('LLV_CACHE', false),
];
