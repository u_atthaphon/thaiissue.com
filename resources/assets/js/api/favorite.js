import { ApiClient } from '../packages/client/client.js';

let client = new ApiClient();

export default {
  toggleFavoritePost(postId) {
    return client.post(`/api/favorites/posts/${postId}`);
  },
  toggleFavoriteComment(commentId) {
    return client.post(`/api/favorites/comments/${commentId}`);
  }
}
