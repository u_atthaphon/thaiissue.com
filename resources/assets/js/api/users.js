import { ApiClient } from '../packages/client/client.js';

let client = new ApiClient();

export default {
  resetPassword(password, newPassword, currentPassword) {
    var data = {
      current_password: password,
      new_password: newPassword,
      new_password_confirmation: currentPassword
    };
    var headers = {
      "headers": { "content-type": "application/json" }
    };
    return client.post(`/api/users/reset-password`, data, headers);
  },
  getFavoritePostPaginate(page = 1) {
    return client.get(`/api/users/favorites-posts?page=${page}`);
  },
  getFavoriteCommentPaginate(page = 1) {
    return client.get(`/api/users/favorites-comments?page=${page}`);
  },
  getHistoryPostPaginate(page = 1) {
    return client.get(`/api/users/history-posts?page=${page}`);
  },
  getHistoryCommentPaginate(page = 1) {
    return client.get(`/api/users/history-comments?page=${page}`);
  }
}
