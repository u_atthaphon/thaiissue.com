import { ApiClient } from '../packages/client/client.js';

let client = new ApiClient();

export default {
  byOrderNoAndPostId(commentOrderNo, postId) {
    return client.get(`/api/comments/${commentOrderNo}/posts/${postId}`);
  }
}
