import { ApiClient } from '../packages/client/client.js';

let client = new ApiClient();

export default {
  all() {
    return client.get('/api/categories');
  },
  getPostPaginatebyId(id, page) {
    return client.get(`/api/categories/${id}/posts-paginate?page=${page}`);
  },
  getCommentPaginateById(id, page) {
    return client.get(`/api/categories/${id}/comments-paginate?page=${page}`);
  }
}
