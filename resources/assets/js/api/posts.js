import { ApiClient } from '../packages/client/client.js';

let client = new ApiClient();

export default {
  byId(id) {
    return client.get(`/api/posts/${id}`)
  },
  getCommentPaginateById(id, page = 1) {
    return client.get(`/api/posts/${id}/comments-paginate?page=${page}`);
  }
}
