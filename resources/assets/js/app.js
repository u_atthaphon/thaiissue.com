
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueUtil from './vue-util.js'
import Vuex from 'vuex'
import AuthClientType from './packages/auth/AuthClientType.js'

Vue.use(VueUtil)
Vue.use(Vuex)
Vue.use(AuthClientType)

Vue.prototype.lang = string => _.get(window.i18n, string);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Users component
Vue.component('user-reset-password-component', require('./components/users/ResetPasswordComponent.vue'));
Vue.component('user-favorite-post-list-component', require('./components/users/FavoritePostListComponent.vue'));
Vue.component('user-favorite-comment-list-component', require('./components/users/FavoriteCommentListComponent.vue'));
Vue.component('user-history-post-list-component', require('./components/users/HistoryPostListComponent.vue'));
Vue.component('user-history-comment-list-component', require('./components/users/HistoryCommentListComponent.vue'));

Vue.component('categories-component', require('./components/CategoriesComponent.vue'));
Vue.component('post-list-component', require('./components/PostListComponent.vue'));
Vue.component('post-component', require('./components/PostComponent.vue'));
Vue.component('post-by-id-component', require('./components/PostByIdComponent.vue'));
Vue.component('comment-list-component', require('./components/CommentListComponent.vue'));
Vue.component('comment-list-by-post-id-component', require('./components/CommentListByPostIdComponent.vue'));
Vue.component('comment-component', require('./components/CommentComponent.vue'));
Vue.component('comment-by-post-id-and-order-no-component', require('./components/CommentByPostIdAndOrderNoComponent.vue'));

// Includs Component
Vue.component('pagination-component', require('./components/includes/PaginationBasicComponent.vue'));
Vue.component('pagination-load-more-component', require('./components/includes/PaginationLoadMoreComponent.vue'));
Vue.component('lazy-image-component', require('./components/includes/LazyImageComponent.vue'));
Vue.component('favorite-component', require('./components/includes/FavoriteComponent.vue'));
Vue.component('go-to-top-component', require('./components/includes/GoToTopComponent.vue'));

// Input Component
Vue.component('input-image-component', require('./components/inputs/InputImageComponent.vue'));


const app = new Vue({
    el: '#app',
    created() {
      axios.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': $('meta[name=client-token]').attr('content'),
      };
      // Vue.authClientType.initAuthenticated()
      // axios.defaults.headers.common['Authorization'] = $('meta[name=client-token]').attr('content')
    }
});

