import config from './config.json'

export default function (Vue) {
  Vue.authClientType = {
    refreshAccessToken() {
      var data = {
        client_id: config.clientType.clientId,
        client_secret: config.clientType.clientSecret,
        grant_type: config.clientType.grantType,
        scope: config.clientType.scope,
      }
      axios.post(config.clientType.uri, data)
        .then(response => {
          this.setToken(
            response.data.access_token,
            response.data.expires_in
          )
        })
    },
    setToken(token, expiration) {
      // Make laravel expireIn accurate with date javascript
      expiration =  parseInt((Date.now() / 1000)) + expiration;
      localStorage.setItem('token', token)
      localStorage.setItem('expiration', expiration)
    },
    getToken() {
      var token = localStorage.getItem('token')
      var expiration = localStorage.getItem('expiration')
      // Make sure we use same format date with expiration
      var now = parseInt((Date.now() / 1000))
      if (!token || !expiration) {
        // console.log('auth client type access token not exists')
        this.refreshAccessToken()
      } else if (now > expiration) {
        // console.log('auth client type access token expirated')
        this.destroyToken()
        this.refreshAccessToken()
      }
      return { "access_token": localStorage.getItem('token')}
    },
    destroyToken() {
      localStorage.removeItem('token')
      localStorage.removeItem('expiration')
    },
    async initAuthenticated() {
      var isOk = false;
      try {
        var headers = {
          "headers": {
            "Authorization": "Bearer " + this.getToken().access_token
          }
        }
        const response = await axios.get(config.authenticatedUri, headers);
        isOk = response.data.isOk;
      } catch (error) {
        isOk = false;
      }
      return isOk;
    },
    echo() {
      // console.log('echo')
    }
  }
  Object.defineProperties(Vue.prototype, {
    $authClientType: {
      get () {
        return Vue.authClientType
      }
    }
  })
}
