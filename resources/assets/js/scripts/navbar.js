function navbarCategoryLabel()
{
  if ($('#navbar-category-label').length == 0) {
    return;
  }
  var labelEle = $('#navbar-category-label');
  var top = window.pageYOffset;
  var windowWidth = window.innerWidth
  var topOffset = 180;
  if (windowWidth < 768) {
    topOffset = 80;
  }

  if (top >= topOffset && labelEle.attr('hidden')) {
    labelEle.attr('hidden', false);
  }
  $(document).scroll(function () {
    var top = window.pageYOffset;
    if (top >= topOffset && labelEle.attr('hidden')) {
      labelEle.attr('hidden', false);
    } else if (top < topOffset && !labelEle.attr('hidden')) {
      labelEle.attr('hidden', true);
    }
  });
}

// function navbarCategoryLabel()
// {
//   if ($('#navbar-category-label').length == 0) {
//     return;
//   }
//   var labelEle = $('#navbar-category-label');
//   var top = window.pageYOffset;
//   var windowWidth = window.innerWidth
//   var topOffset = 155;
//   if (windowWidth < 768) {
//     topOffset = 50;
//   }

//   if (top >= topOffset && labelEle.css('display') == 'none') {
//     labelEle.fadeIn();
//   }
//   $(document).scroll(function () {
//     var top = window.pageYOffset;
//     if (top >= topOffset && labelEle.css('display') == 'none') {
//       labelEle.fadeIn();
//     } else if (top < topOffset && !labelEle.css('display') !== 'none') {
//       labelEle.fadeOut();
//     }
//   });
// }
