function addEventlistenerReferToCross()
{
  $('.referToCross').mouseenter(function() {
    if ($(this).has('.referCrossFloatingBox').length > 0) {
      return;
    }
    var thisElem = $(this);
    var postId = thisElem.attr('data-post');
    var commentNo = thisElem.attr('data-comment-no');
    var localAccessToken = $('meta[name=client-token]').attr('content');
    $.ajax({
      url: `/api/comments/${commentNo}/posts/${postId}`,
      type:'GET',
      headers: {
        "Authorization": `Bearer ${localAccessToken}`
      },
      success: function(response) {
        appendReferToCrossBox(thisElem, response.data);
      },
      error: function(xhr, error, errorThrown) {
        appendErrorReferToCrossBox(thisElem);
      }
    });
  });
}

function appendReferToCrossBox(parent, data) {
  var referToFloating = $('<span>', {
    class: 'referCrossFloatingBox',
  });
  var orderNo = $('<span>', {
    id: `post-${data.post.id}-comment-${data.order_no}`,
    class: 'order-no'
  }).text(data.order_no);

  var headTitle = $('<p>', {
    class: 'title'
  }).html(`<i class="fa fa-star fa-lg"></i> ${data.post.title}</p>`);
  var headInfo = $('<span>').html(
    `${lang('category.by-no-name')} | ${data.updated_at} | ID: <strong style="color: ${colorFromStirng(data.hash_ip)}">${data.hash_ip}</strong>
    `
    );
  var withImag = '';
  if (data.thumbnail !== null) {
    withImag = `<span><img src="${data.thumbnail}" class="img-switch"><span>`;
  }
  var body = $('<p>', {
    class: 'body with-img'
  }).html(`${withImag}<span>${data.body}</span>`);
  referToFloating.append(headTitle).append(orderNo).append(headInfo).append(body);
  parent.append(referToFloating);
}

function appendErrorReferToCrossBox(parent) {
  var referToFloating = $('<span>', {
    class: 'referCrossFloatingBox',
  });
  var body = $('<p>', {
    class: 'body error'
  }).html(lang('error.no-this-comment-yet'));
  referToFloating.append(body);
  parent.append(referToFloating);
}

