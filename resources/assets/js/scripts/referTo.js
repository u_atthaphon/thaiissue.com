function addEventlistenerReferTo()
{
  $('.referTo').mouseenter(function() {
    if ($(this).has('.referFloatingBox').length > 0) {
      return;
    }
    var thisElem = $(this);
    var postId = thisElem.attr('data-post');
    var commentNo = thisElem.attr('data-comment-no');
    var localAccessToken = $('meta[name=client-token]').attr('content');
    $.ajax({
      url: `/api/comments/${commentNo}/posts/${postId}`,
      type:'GET',
      headers: {
        "Authorization": `Bearer ${localAccessToken}`
      },
      success: function(response) {
        appendReferToBox(thisElem, response.data);
      },
      error: function(xhr, error, errorThrown) {
        appendErrorReferToBox(thisElem);
      }
    });
  });
}

function appendReferToBox(parent, data) {
  var referToFloating = $('<span>', {
    class: 'referFloatingBox',
  });
  var orderNo = $('<span>', {
    id: `post-${data.post.id}-comment-${data.order_no}`,
    class: 'order-no'
  }).text(data.order_no);
  var headInfo = $('<span>').text(
    `${lang('category.by-no-name')} | ${data.updated_at} | ID: `
    );
  var hashColor = `<strong style="color: ${colorFromStirng(data.hash_ip)}">${data.hash_ip}</strong>`;
  var withImag = '';
  if (data.thumbnail !== null) {
    withImag = `<span><img src="${data.thumbnail}" class="img-switch"><span>`;
  }
  var body = $('<p>', {
    class: 'body with-img'
  }).html(`${withImag}<span>${data.body}</span>`);
  referToFloating.append(orderNo).append(headInfo).append(hashColor).append(body);
  parent.append(referToFloating);
}

function appendErrorReferToBox(parent) {
  var referToFloating = $('<span>', {
    class: 'referFloatingBox',
  });
  var body = $('<p>', {
    class: 'body error'
  }).html(lang('error.no-this-comment-yet'));
  referToFloating.append(body);
  parent.append(referToFloating);
}
