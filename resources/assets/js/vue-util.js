import randomcolor from './scripts-vue/random-color.js'

// This is your plugin object. It can be exported to be used anywhere.
const VueUtil = {
  // The install method is all that needs to exist on the plugin object.
  // It takes the global Vue object as well as user-defined options.
  install(Vue, options) {
    // We call Vue.mixin() here to inject functionality into all components.
    Vue.mixin({
      // Anything added to a mixin will be injected into all components.
      // In this case, the mounted() method runs when the component is added to the DOM.
      mounted() {
        // console.log('Mounted VueUtil!')
      }
    });

    Vue.prototype.$util = {
      /**
       * Get the value of a querystring
       * @param  {String} field The field to get the value of
       * @param  {String} url   The URL to get the value from (optional)
       * @return {String}       The field value
       */
      getQueryString(field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
        var string = reg.exec(href);
        return string ? string[1] : null;
      },
      colorFromStirng(text) {
        return randomcolor.colorFromStirng(text);
      },

      removeElements(elms) {
        [...elms].forEach(el => el.remove());
      },

      showElement(elm) {
        if (!document.body.contains(elm)) {
          return;
        }
        if (elm.style.display === 'none') {
          elm.style.display = '';
        }
      },

      hideElement(elm) {
        elm.style.display = 'none';
      },

      toggleElement(elm) {
        elm.style.display = elm.style.display === 'none' ? '' : 'none';
      },
    },

    Vue.filter('truncate', function(text, length, clamp){
        clamp = clamp || '...';
        var node = document.createElement('div');
        node.innerHTML = text;
        var content = node.textContent;
        return content.length > length ? content.slice(0, length) + clamp : content;
    })
  }
};

export default VueUtil;
