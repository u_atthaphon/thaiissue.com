<?php

return [
    'prev' => '< ใหม่กว่า',
    'next' => 'เก่ากว่า >',
    'load-more-page' => 'โหลดเพิ่มหน้า',
    'from-all-page' => 'จากทั้งหมด',
    'page' => 'หน้า',
];
