<?php

return [
    'post-header' => 'เปิดประเด็น',
    'post-title' => 'หัวข้อ',
    'post-body' => 'เนื้อหา',
    'post-hint-title' => '<p>หมวด <strong class="text-app-primary">:title</strong> :body</p>',
    'post-hint-body' => '
    <p class="small">
        <strong>สิทธิเสรีถาพในการพูด (freedom of speech)</strong>
        <br>
        เว็บ thaiissue ต้องการส่งเสริมให้เกิดความคิดสร้างสรรค์ เราไม่จำเป็นต้องเชื่อทุกเรื่องที่ได้อ่านหรือได้ยินหรือได้เห็น แต่เมื่อยิ่งเราได้อ่าน ยิ่งได้พูด นั่นจะทำให้เราได้คิด ทางเราเชื่อว่านี่เป็นกระบวนการที่ช่วยสร้างให้เกิดการพัฒนาทางความคิดสร้างสรรค์ได้
    </p>
    <p class="small mb-1">
        <strong>**อ่านก่อนโพส</strong>
    </p>
    <ol class="small pl-3">
        <li>นึกถึงใจเขาใจเรา</li>
        <li>อ้างอิงถึงแหล่งของเนื้อหาที่นำมาโพส</li>
        <li>หากระทู้ที่มีเนื้อหาเหมือนกันกับที่จะโพส ก่อนทำการโพส</li>
        <li>อ่าน <a href=":ethices-url" target="_blank">มรรยาท</a> ก่อนทำการโพส</li>
    </ol>
    <p class="small">
        <span>อ่าน ฟัง แต่อย่าเพิ่งปักใจเชื่อ ขอให้คิดวิเคราะห์ควบคู่กันไปด้วย</span>
    </p>
    ',
    'selected-img' => 'เลือกรูป',
    'allowed-file-type' => 'อนุญาติเพียง jpeg, jpg, gif, png ขนาดไม่เกิน 5 MB',
    'comment' => 'คอมเมนต์',
    'post' => 'ส่งประเด็น',
    'moderator' => 'พิดไม่คูด',
    'image' => 'รูป',
];
