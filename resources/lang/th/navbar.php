<?php

return [
    'new' => 'มาใหม่',
    'comment' => 'คอมเมนต์',
    'link' => 'ลายแทง',
    'new_post' => 'เปิดประเด็น',
    'register' => 'สมัคร',
    'login' => 'เข้าสู่ระบบ',
    'logout' => 'ออกจากระบบ',
    'view-profile' => 'ดูข้อมูลส่วนตัว',
    'favorite' => 'โปรดปราน',
    'history' => 'ประวัติ',
    'setting' => 'ตั้งค่า',
    'backoffice' => 'หลังบ้าน',
    'beta' => 'เบต้า',
];
