<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'register_sub_header' => 'เว็บบอร์ดแบบไม่ระบุตัวตนผู้ใช้งาน',
    'btn_register_google' => 'Sign in with Google',
    'btn_register_facebook' => 'Sign in with Facebook',
    'hint' => 'สำหรับผู้ใช้งานระบบที่สมัครสามาชิก ท่านสามารถที่จะ เก็บประเด็นและคอมเมนต์ที่ท่านชื่อชอบไว้ดูใหม่ในภายหลังได้ และท่านยังสามารถดู รายการที่เคยได้ทำการเปิดประเด็นไปหรือคอมเมนต์ในขณะที่เข้าสู่ระบบได้โดยอัตโนมัติ  แต่ไม่ว่าจะเข้าสู่ระบบหรือไม่ก็ตาม ชื่อของผู้โพสจะเป็น โนเนม เสมอ',
    'register' => 'สมัคร',
    'name' => 'นามแฝง',
    'email' => 'อีเมล์',
    'password' => 'รหัสผ่าน',
    'password-confirm' => 'ยืนยันรหัสผ่าน',
    'login' => 'เข้าสู่ระบบ',
    'remember' => 'จดจำฉันไว้ในระบบ',
    'forget-password' => 'ลืมรหัสผ่าน',
    'reset-password' => 'ขอรหัสผ่านใหม่',
];
