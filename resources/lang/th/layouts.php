<?php

return [
    'brand' => 'Thaiissue',
    'footer-home' => 'หน้าแรก',
    'footer-ethics' => 'มรรยาท',
    'footer-guildeline' => 'ไกด์ไลน์',
    'footer' => '© :currentyear  thaiissue.com, all rights reserved.',
];
