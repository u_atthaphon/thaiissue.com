<?php

return [
    'setting' => 'ตั้งค่า',
    'moderator' => 'ทีมงานพูดไม่คิด',
    'update' => 'อัพเดท',
    'favorite' => 'โปรดปราน',
    'history' => 'ประวัติ',
    'profile' => 'ข้อมูลส่วนตัว',
    'your-provider' => 'คุณสมัครสมาชิกด้วย [<strong class="text-app-primary text-uppercase">:provider</strong>]',
    'password' => 'รหัสผ่าน',
    'current-password' => 'รหัสผ่านเดิม',
    'new-password' => 'รหัสผ่านใหม่',
    'confirm-password' => 'ยืนยันรหัสผ่าน',
    'favorite-post' => 'ประเด็นที่ชอบ',
    'favorite-comment' => 'คอมเมนต์ที่ใช่',
    'history-post' => 'ประเด็นที่เคยเปิด',
    'history-comment' => 'คอมเมนต์ที่เคยลง',
];
