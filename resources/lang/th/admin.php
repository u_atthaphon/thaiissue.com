<?php

return [
    'dashboard' => 'แดชบอร์ด',
    'posts' => 'ประเด็น',
    'comments' => 'คอมเมนต์',
    'categories' => 'หมวดหมู่',
    'users' => 'สมาชิก',
    'ban-ip' => 'แบน IP',
    'errors' => [
        'waring' => 'มี errors อยู่นะแก้ไขหน่อย',
    ],
    'post' => [
        'header' => 'ประเด็นทั้งหมด',
        'header-edit' => 'แก้ไขประเด็น',
        'id' => 'id',
        'category_id' => 'category_id',
        'user_id' => 'user_id',
        'title' => 'title',
        'body' => 'body',
        'thumbnail' => 'thumbnail',
        'image' => 'image',
        'nsfw' => 'nsfw',
        'active' => 'active',
        'total_comment' => 'total_comment',
        'total_image' => 'total_image',
        'meta' => 'meta',
        'ip' => 'ip',
        'hash_ip' => 'hash_ip',
        'status' => 'status',
        'status_at' => 'status_at',
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
    ],
    'comment' => [
        'header' => 'คอมเมนต์ทั้งหมด',
        'header-edit' => 'แก้ไขคอมเมนต์',
    ],
    'ban' => [
        'header' => 'ผู้ถูกแบนทั้งหมด',
        'header-create' => 'เพิ่มผู้ถูกแบน',
        'header-edit' => 'แก้ไขการแบน',
        'create' => 'เพิ่มผู้ถูกแบน',
    ],
    'category' => [
        'header' => 'หมวดหมู่ทั้งหมด',
        'header-edit' => 'แก้ไขหมวดหมู่',
        'header-create-parent' => 'เพิ่มหมวดหมู่หลัก',
        'header-create-sub' => 'เพิ่มหมวดหมู่ย่อย',
        'create' => 'เพิ่มหมวดหมู่หลัก',
        'create-sub' => 'เพิ่มหมวดหมู่ย่อย',
        'create-sub' => 'เพิ่มหมวดหมู่ย่อย',
        'allowed-file-type' => 'อนุญาติเพียง ขนาดตายตัว widht: 851, height: 207 และรูปแบบ png',
    ],
];
