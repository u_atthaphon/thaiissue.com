<span id="socialite-section">
  <div class="social-buttons">
    <a class="btn btn-social btn-google" href="{{ url('auth/google') }}">
      <i class="fa fa-google-plus align-middle mr-2"></i>
      <span class="align-middle">@lang('auth.btn_register_google')</span>
    </a>
    {{-- <a class="btn btn-social btn-facebook" href="{{ url('auth/facebook') }}">
      <i class="fa fa-facebook align-middle mr-2"></i>
      <span class="align-middle">@lang('auth.btn_register_facebook')</span>
    </a> --}}
  </div>
  <p class="hint">
    @lang('auth.hint')
  </p>
</span>
