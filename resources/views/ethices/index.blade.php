@extends('layouts.app')

@section('content')
<div style="height:1px;"></div>
<div class="container">
  <div class="grid-card">
    <h5>
      @lang('ethice.header')
    </h5>
    <p>
      @lang('ethice.body')
    </p>
  </div>

  <div class="ethices">
    <div class="grid-card">
      <div class="row">
        <div class="col-12 col-md-4">
          <div class="board">
            <h6>
              @lang('ethice.board')
            </h6>
            <p>
              <ul>
                @foreach ($ethices as $title => $ethice)
                  <li>
                    <a href="#{{ $title }}" class="hashtag-jump">
                      {{ $title }}
                    </a>
                  </li>
                @endforeach
              </ul>
            </p>
          </div>
        </div> <!-- col-12 col-md-4 -->
        <div class="col-12 col-md-8">
          <div class="detials">
            @foreach ($ethices as $title => $ethice)
              <h5 id="{{ $title }}">
                {{ $title }}
              </h5>
              <p>
                <ol>
                    @foreach ($ethice as $text)
                      <li>
                        {{ $text }}
                      </li>
                    @endforeach
                  </ol>
              </p>
            @endforeach
          </div>
        </div> <!-- col-12 col-md-8 -->
      </div>
    </div>
  </div>
</div>
@endsection
