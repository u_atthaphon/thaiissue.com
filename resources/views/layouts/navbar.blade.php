<nav class="navbar navbar-expand-md navbar-thaiissue">
  <div class="container">
    <a class="navbar-brand" href="{{ url('/') }}">
      <img src="/logo.png"
      height="32"
      class="d-inline-block align-top"
      alt="@lang('layouts.brand')">
      {{-- <span class="beta">
        @lang('navbar.beta')
      </span> --}}
    </a>

    @include('layouts.includes.navbar-menu')

  </div>
</nav>

@include('layouts.includes.navbar-category-label')
