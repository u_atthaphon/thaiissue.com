<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="client-token" content="{{ client_token() }}">
  {!! SEO::generate() !!}
  <link href="{{ asset('css/app.css?v1') }}" rel="stylesheet">
  @include('layouts.includes.favicon')
  {{-- @yield('customCss') --}}
  @stack('recaptcha-script')
</head>
<body>
  @include('layouts.navbar')

  <main>
    <div id="app">
      @yield('content')
      <go-to-top-component></go-to-top-component>
    </div>
  </main>

  @include('layouts.footer')


  @stack('pageScript')

  <script src="/js/llv-{{ \App::getLocale() }}.js"></script>
  <script src="{{ asset('js/app.js?v1') }}"></script>
  <script src="{{ asset('js/scripts.js') }}"></script>

  @env('production')
    @include('layouts.includes.google-analytices')
  @endenv

</body>
</html>
