@inject('roleEnum', 'Thaiissue\Enums\Role')
<?php
  $currRtouePrefix = Route::getCurrentRoute()->getPrefix();
  $currRouteName = Route::currentRouteName();
?>
<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
  <i class="fa fa-ellipsis-v"></i>
</button>

<div class="navbar-collapse collapse bg-white" id="navbarCollapse" style="">
  @if (isset($category) && (
  stristr($currRtouePrefix, '/categories') !== false ||
  stristr($currRtouePrefix, '/posts') !== false ||
  stristr($currRtouePrefix, '/comments') !== false
  ))
    <ul class="navbar-nav mr-auto">
      <li
        class="nav-item {{ ($currRouteName == 'categories-show')? 'active' : '' }}"
      >
        <a class="nav-link" href="{{ $category->url->show}}">
          @lang('navbar.new')
        </a>
      </li>
      <li
        class="nav-item {{ ($currRouteName == 'categories-show-comments')? 'active' : '' }}"
      >
        <a class="nav-link" href="{{ $category->url->showComments}}">
          @lang('navbar.comment')
        </a>
      </li>
    </ul>
  @else
    <div class="navbar-nav mr-auto"></div>
  @endif
  {{-- right bar --}}
  <div class="navbar-right  my-2 my-lg-0">
    <ul class="navbar-nav mr-auto">
      @if (isset($category) && $category->slug !== "announcement")
        <li class="nav-item ">
          <a
            href="{{ $category->url->createPost}}"
            class="nav-link"
          >
            <i class="fa fa-plus"></i>
            @lang('navbar.new_post')
          </a>
        </li>
      @elseif (
        isset($category) &&
        $category->slug == "announcement" &&
        \Auth::check() &&
        \Auth::user()->inRole($roleEnum::ADMIN)
      )
        <li class="nav-item">
          <a
            href="{{ $category->url->createPost}}"
            class="nav-link"
          >
            <i class="fa fa-plus"></i>
            @lang('navbar.new_post')
          </a>
        </li>
      @endif

      @if (\Auth::check() && \Auth::user()->inRole($roleEnum::ADMIN))
        <li class="nav-item">
          <a
            href="{{ route('admin-dashboard-index') }}"
            class="nav-link"
          >
            <i class="fa fa-gamepad" aria-hidden="true"></i>
            @lang('navbar.backoffice')
          </a>
        </li>
      @endif

      @if (\Auth::check())
        <li class="nav-item dropdown">
          <div
            id="userDropdown"
            class="nav-link dropdown-toggle"
            role="button"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <img
              src="{{ \Auth::user()->avatar }}"
              class="avatar"
              width="28"
              height="28"
            >
          </div>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="{{ route('users-me') }}">
              <i class="fa fa-user-circle mr-2"></i>
              @lang('navbar.view-profile')
            </a>
            <a class="dropdown-item" href="{{ route('users-favorites-posts') }}">
              <i class="fa fa-heart mr-2"></i>
              @lang('navbar.favorite')
            </a>
            <a class="dropdown-item" href="{{ route('users-history-posts') }}">
              <i class="fa fa-history mr-2"></i>
              @lang('navbar.history')
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();"
            >
              @lang('navbar.logout')
            </a>
            <form
              id="logout-form"
              action="{{ route('logout') }}"
              method="POST"
              style="display: none;"
            >
                {{ csrf_field() }}
            </form>
          </div>
        </li>
      @else
        <li class="nav-item {{ ($currRouteName == 'login')? 'active' : '' }}">
          <a
            href="{{ route('login') }}"
            class="nav-link"
          >
            @lang('navbar.login')
          </a>
        </li>
        <li class="nav-item {{ ($currRouteName == 'register')? 'active' : '' }}">
          <a
            href="{{ route('register') }}"
            class="nav-link nav-register"
          >
            <i class="fa fa-user-circle-o"></i>
            @lang('navbar.register')
          </a>
        </li>
      @endif
    </ul>
  </div>
</div>
