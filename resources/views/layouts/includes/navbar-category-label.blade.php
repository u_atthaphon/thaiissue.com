@if (isset($category) && Route::getCurrentRoute()->uri() != '/')
  <nav id="navbar-category-label" hidden>
    <a href="{{ $category->url->show }}">
      # {{ $category->title }}
    </a>
  </nav>
@endif
