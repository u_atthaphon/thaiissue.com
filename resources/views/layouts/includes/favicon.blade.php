<!-- iOS 2.0+ and Android 2.1+ -->
<link rel="apple-touch-icon-precomposed" href="{{ asset('favicon-152.png') }}">

<!-- IE 10 Metro tile icon -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('favicon-144.png') }}">

<link rel="icon" href="{{ asset('favicon-32.png') }}" sizes="32x32">
<link rel="shortcut icon" href="{{ asset('favicon-32.png') }}">

<meta name="theme-color" content="#ffffff" />
