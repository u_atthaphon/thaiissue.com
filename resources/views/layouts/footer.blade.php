<footer>
  <div class="container p-3 p-md-4">
    <ul class="list-inline">
      <li class="list-inline-item">
        <a href="{{ url('/') }}">
          @lang('layouts.footer-home')
        </a>
      </li>
      <li class="list-inline-item">
        <a href="{{ route('ethices-index') }}">
          @lang('layouts.footer-ethics')
        </a>
      </li>
      <li class="list-inline-item">
        <a href="{{ route('guidelines-index') }}">
          @lang('layouts.footer-guildeline')
        </a>
      </li>
    </ul>
    <p class="mb-1">
      @lang('layouts.footer', ['currentyear' => date("Y")])
    </p>
  </div>
</footer>
