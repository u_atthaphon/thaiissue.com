@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="grid-cover">
    <img
    class="img-fluid"
    src="{{ asset('storage/cover.png') }}"
    >
  </div>
</div>

<div class="container">
  <div class="grid-card">
    <h5>
      @lang('home.category')
    </h5>
    @shelves(2)
    <categories-component></categories-component>
  </div>
</div>
@endsection

