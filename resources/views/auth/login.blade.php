@extends('layouts.app')

@section('content')
  <div style="height:1px;"></div>
    <div class="container">
      <div class="grid-card" id="login">
        <div class="row row-header">
          <div class="col-12 text-center">
            <h2>
              THAIISSUE
            </h2>
            <h5 class="text-secondary">
              @lang('auth.register_sub_header')
            </h5>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-md-6">
            @include('socialite.socialite')
          </div>
          <div class="col-12 col-md-6">
            <hr class="d-block d-md-none my-4">
            <h4>
              @lang('auth.login')
            </h4>
            <form method="POST" action="{{ route('login') }}">
              @csrf
               <div class="form-group row">
                <label
                  for="email"
                  class="col-4 col-form-label"
                >
                  @lang('auth.email')
                </label>
                <div class="col-8">
                  <input
                    id="email"
                    type="email"
                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email"
                    value="{{ old('email') }}"
                    required
                    autofocus
                    autocomplete="email"
                  >
                  @if ($errors->has('email'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
              </div> {{-- end form-group row --}}
              <div class="form-group row">
                <label
                for="password"
                class="col-4 col-form-label"
                >
                  @lang('auth.password')
                </label>
                <div class="col-8">
                  <input
                    id="password"
                    type="password"
                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                    name="password"
                    value="{{ old('password') }}"
                    required
                    autofocus
                    autocomplete="password"
                  >
                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
              </div> {{-- end form-group row --}}
              <div class="form-group row">
                  <div class="col-12">
                    <input
                      id="remember-me"
                      type="checkbox"
                      name="remember" {{ old('remember') ? 'checked' : '' }}
                    >
                    <label for="remember-me">
                      @lang('auth.remember')
                    </label>
                  </div>
              </div>
              <div class="form-group row">
                <div class="offset-4 col-8">
                  <button type="submit" class="btn btn-primary">
                    @lang('auth.login')
                  </button>
                  {{-- <a class="btn btn-link p-0 ml-3" href="{{ route('password.request') }}">
                    @lang('auth.forget-password')
                  </a> --}}
                </div>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
