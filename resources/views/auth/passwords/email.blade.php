@extends('layouts.app')

@section('content')
  <div style="height:1px;"></div>
  <div class="container">
    <div class="grid-card" id="reset-password-email">
      <div class="row">
          <div class="offset-md-2 col-8">
            <h4>
              @lang('auth.reset-password')
            </h4>
            <form method="POST" action="{{ route('password.email') }}">
              @csrf
              <div class="form-group row">
                <label
                  for="email"
                  class="col-4 col-form-label text-md-right"
                >
                  @lang('auth.email')
                </label>

                <div class="col-6">
                    <input
                      id="email"
                      type="email"
                      class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                      name="email"
                      value="{{ old('email') }}"
                      required
                    >
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
              </div>
              <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary">
                    {{ __('Send Password Reset Link') }}
                  </button>
                </div>
              </div>
            </form>
          </div>
      </div>
    </div>
  </div>
@endsection
