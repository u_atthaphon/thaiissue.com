@extends('layouts.app')

@section('content')
  <div style="height:1px;"></div>
  <div class="container">
    <div class="grid-card" id="register">
      <div class="row row-header">
        <div class="col-12 text-center">
          <h2>
            THAIISSUE
          </h2>
          <h5 class="text-secondary">
            @lang('auth.register_sub_header')
          </h5>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-6">
          @include('socialite.socialite')
        </div>
        <div class="col-12 col-md-6">
          <hr class="d-block d-md-none my-4">
          <h4>
            @lang('auth.register')
          </h4>
          <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group row">
              <label
              for="name"
              class="col-4 col-form-label"
              >
                @lang('auth.name')
              </label>
              <div class="col-8">
                <input
                  id="name"
                  type="text"
                  class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                  name="name"
                  value="{{ old('name') }}"
                  required
                  autofocus
                >
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
              </div>
            </div> {{-- end form-group row --}}
            <div class="form-group row">
              <label
              for="email"
              class="col-4 col-form-label"
              >
                @lang('auth.email')
              </label>
              <div class="col-8">
                <input
                  id="email"
                  type="email"
                  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                  name="email"
                  value="{{ old('email') }}"
                  required
                  autofocus
                >
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div> {{-- end form-group row --}}
            <div class="form-group row">
              <label
              for="password"
              class="col-4 col-form-label"
              >
                @lang('auth.password')
              </label>
              <div class="col-8">
                <input
                  id="password"
                  type="password"
                  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                  name="password"
                  value="{{ old('password') }}"
                  required
                  autofocus
                >
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div> {{-- end form-group row --}}
            <div class="form-group row">
              <label
              for="password-confirm"
              class="col-4 col-form-label"
              >
                @lang('auth.password-confirm')
              </label>
              <div class="col-8">
                <input
                  id="password-confirm"
                  type="password"
                  class="form-control{{ $errors->has('password-confirm') ? ' is-invalid' : '' }}"
                  name="password_confirmation"
                  value="{{ old('password-confirm') }}"
                  required
                >
              </div>
            </div> {{-- end form-group row --}}
            <div class="form-group row">
              <div class="offset-4 col-8">
                <button type="submit" class="btn btn-primary mb-2">
                  @lang('auth.register')
                </button>
              </div>
            </div> {{-- end form-group row --}}
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
