

@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="grid-cover">

    <img
    class="img-fluid"
    src="{{ $category->cover }}"
    >
  </div>
</div>

<div class="container">
  @shelvesWithImage(20)
</div>

<div class="container">
    <comment-list-component
      :category-id="{{ $category->id }}"
      auth="{{ \Auth::check()?? false }}"
    ></comment-list-component>
</div>
@endsection

