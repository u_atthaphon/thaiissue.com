
@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="grid-cover">
    <img
    class="img-fluid"
    src="{{ $category->cover }}"
    >
  </div>
</div>

<div class="container">
    @shelvesWithImage(20)
    <posts-component
      :category-id="{{ $category->id }}"
    ></posts-component>
</div>
@endsection

