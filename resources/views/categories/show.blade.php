
@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="grid-cover">
    <img
    class="img-fluid"
    src="{{ $category->cover }}"
    >
  </div>
</div>

<div class="container">
    @shelvesWithImage(20)
    <post-list-component
      :category-id="{{ $category->id }}"
      auth="{{ \Auth::check()?? false }}"
    ></post-list-component>
</div>
@endsection

