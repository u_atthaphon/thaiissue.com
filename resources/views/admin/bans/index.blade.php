@extends('admin.layouts.app')

@section('content')

  <h1 class="h2 m-3">
    @lang('admin.ban.header')
  </h1>

  <!-- will be used to show any messages -->
  @if (\Session::has('message'))
      <div class="alert alert-info">{{ \Session::get('message') }}</div>
  @endif

  <div class="row">
    <div class="col-6">
      NOW: {{ \Carbon\Carbon::now() }}
    </div>
    <div class="col-6 text-right">
      <a href="{{ route('admin-bans-create') }}" class="btn btn-danger text-white">
        @lang('admin.ban.create')
      </a>
    </div>
  </div>
  <div class="table-responsive mb-5">
    <br>
    <table class="table table-striped table-sm admin-table" id="bans-table">
      <thead>
        <tr>
          <th>ID</th>
          <th>IP</th>
          <th>USER</th>
          <th>REASON</th>
          <th>BAN</th>
          <th>LEVEL</th>
          <th>CREATED AT</th>
          <th>EXPIRED AT</th>
          <th>ACTION</th>
      </tr>
      </thead>
    </table>
  </div>

@endsection

@push('script-datatables')
<script>
$(function() {
    $('#bans-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin-bans-datatables') }}',
        columns: [
            { data: 'id', name: 'id', searchable: true  },
            { data: 'ip', name: 'ip', searchable: true  },
            { data: 'user', name: 'user', searchable: false  },
            { data: 'reason', name: 'reason', searchable: true  },
            { data: 'ban', name: 'ban', searchable: false  },
            { data: 'level', name: 'level', searchable: false  },
            { data: 'created_at', name: 'created_at', searchable: false },
            { data: 'expired_at', name: 'expired_at', searchable: false },
            { data: 'action', name: 'action', width: '10%', orderable: false, searchable: false }
        ]
    });
});
</script>
@endpush
