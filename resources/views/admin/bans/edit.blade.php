@extends('admin.layouts.app')

@inject('banEnum' , 'Thaiissue\Enums\Ban')

@section('content')

  <h1 class="h2 m-3">
    @lang('admin.ban.header-edit')
  </h1>

  <!-- will be used to show any messages -->
  @if (\Session::has('message'))
      <div class="alert alert-info">{{ \Session::get('message') }}</div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger">
      @lang('admin.errors.waring')
    </div>
  @endif

<form
  method="post"
  action="{{ route('admin-bans-update', ['ban_id' => $ban->id]) }}"
>
  @csrf
  @method('PATCH')


  <div class="form-group row">
    <label for="ip"
      class="col-sm-2 col-form-label"
    >
      IP
    </label>
    <div class="col-sm-10">
      <input
        type="text"
        class="form-control @if ($errors->has('ip')) is-invalid @endif"
        id="ip"
        name="ip"
        placeholder="Example: 127.0.0.1"
        value="{{ $ban->ip }}"
        tabindex="1"
      >
      @if ($errors->has('ip'))
        <div class="small text-danger">
          {{ $errors->first('ip') }}
        </div>
      @endif
    </div>
  </div>

  <div class="form-group row">
    <label for="user_id"
      class="col-sm-2 col-form-label"
    >
      USER ID
    </label>
    <div class="col-sm-10">
      <input
        type="number"
        class="form-control @if ($errors->has('user_id')) is-invalid @endif"
        id="user_id"
        name="user_id"
        placeholder="USER ID"
        value="{{ $ban->user_id }}"
        tabindex="2"
      >
      @if ($errors->has('user_id'))
        <div class="small text-danger">
          {{ $errors->first('user_id') }}
        </div>
      @endif
    </div>
  </div>

  <div class="form-group row">
    <label for="reason"
      class="col-sm-2 col-form-label"
    >
      REASON
    </label>
    <div class="col-sm-10">
      <textarea
        class="form-control @if ($errors->has('reason')) is-invalid @endif"
        id="reason"
        name="reason"
        rows="5"
        tabindex="3"
      >{{ $ban->reason }}</textarea>
      @if ($errors->has('reason'))
        <div class="small text-danger">
          {{ $errors->first('reason') }}
        </div>
      @endif
    </div>
  </div>

<div class="form-group row">
    <label for="ban"
      class="col-sm-2 col-form-label"
    >
      BAN
    </label>
    <div class="col-sm-10">
      <div class="form-check">
        <input
          class="form-check-input"
          type="checkbox"
          {{ $ban->ban ? 'checked' : ''}}
          id="ban"
          name="ban"
          tabindex="4"
        >
        <label class="form-check-label" for="ban">
          ban
        </label>
      </div>
    </div>
  </div>

  <div class="form-group row">
    <label for="level"
      class="col-sm-2 col-form-label"
    >
      level
    </label>
    <div class="col-sm-10">
      <select
        name="level"
        id="level"
        class="form-control"
        tabindex="4"
      >
        @foreach ($banEnum->options() as $value)
          <option
            value="{{ $value }}"
            {{ $ban->level == $value ? 'selected' : '' }}
          >
            {{ $value }}
          </option>
        @endforeach
      </select>
      <div class="text-primary">
        NOW: {{ \Carbon\Carbon::now() }}
      </div>
      <div class="small text-secondary">
        lvl1 ban for 30 minutes<br>
        lvl2 ban for 2 hours<br>
        lvl3 ban for 24 hours<br>
        lvl4 ban for 3 days<br>
      </div>

    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-2 "></div>
    <div class="col-sm-10">
      <button
        type="submit"
        class="btn btn-primary"
        tabindex="7"
      >
        Submit
      </button>
    </div>

</form>


@endsection
