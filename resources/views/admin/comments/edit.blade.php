@extends('admin.layouts.app')

@inject('statusEnum', 'Thaiissue\Enums\Status')

@section('content')

  <h1 class="h2 m-3">
    @lang('admin.post.header-edit')
  </h1>

  <!-- will be used to show any messages -->
  @if (\Session::has('message'))
      <div class="alert alert-info">{{ \Session::get('message') }}</div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger">
      @lang('admin.errors.waring')
    </div>
  @endif

<form
  method="post"
  action="{{ route('admin-comments-update', ['comment_id' => $comment->id]) }}"
>
  {{ csrf_field() }}
  {{ method_field('PATCH') }}

  <div class="form-group row">
    <label for="id"
      class="col-sm-2 col-form-label"
    >
      ID
    </label>
    <div class="col-sm-10">
      {{ $comment->id }}
    </div>
  </div>
  <div class="form-group row">
    <label for="category_id"
      class="col-sm-2 col-form-label"
    >
      CATEGORY
    </label>
    <div class="col-sm-10">
      <select
        name="category_id"
        id="category_id"
        class="form-control @if ($errors->has('category_id')) is-invalid @endif"
        tabindex="1"
      >
        @foreach ($categories as $category)
          <option
            value="{{ $category->id }}"
            {{ $comment->category_id == $category->id ? 'selected' : '' }}
          >
            {{ $category->title }}
          </option>
        @endforeach
      </select>
      @if ($errors->has('category_id'))
        <div class="small text-danger">
          {{ $errors->first('category_id') }}
        </div>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="user_id"
      class="col-sm-2 col-form-label"
    >
      USER ID
    </label>
    <div class="col-sm-10">
      {{ $comment->user_id ?? 'nil' }}
    </div>
  </div>
  <div class="form-group row">
    <label for="post_id"
      class="col-sm-2 col-form-label"
    >
      POST ID
    </label>
    <div class="col-sm-10">
      {{ $comment->post_id ?? 'nil' }}
    </div>
  </div>
  <div class="form-group row">
    <label for="title"
      class="col-sm-2 col-form-label"
    >
      ORDER NO
    </label>
    <div class="col-sm-10">
      {{ $comment->order_no}}
    </div>
  </div>
  <div class="form-group row">
    <label for="body"
      class="col-sm-2 col-form-label"
    >
      BODY
    </label>
    <div class="col-sm-10">
      <textarea
        class="form-control"
        id="body"
        name="body"
        tabindex="3"
      >{{ $comment->body }}</textarea>
      @if ($errors->has('body'))
        <div class="small text-danger">
          {{ $errors->first('body') }}
        </div>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="thumbnail"
      class="col-sm-2 col-form-label"
    >
      THUMBNAIL
    </label>
    <div class="col-sm-10">
      <img
        src="{{ $comment->thumbnail }}"
        class="img-responsive"
        height="150"
      >
    </div>
  </div>
  <div class="form-group row">
    <label for="image"
      class="col-sm-2 col-form-label"
    >
      IMAGE
    </label>
    <div class="col-sm-10">
      <img
        src="{{ $comment->image }}"
        class="img-responsive"
        height="150"
      >
    </div>
  </div>
  <div class="form-group row">
    <label for="nsfw"
      class="col-sm-2 col-form-label"
    >
      NSFW
    </label>
    <div class="col-sm-10">
      <div class="form-check">
        <input
          class="form-check-input"
          type="checkbox"
          {{ $comment->nsfw ? 'checked' : '' }}
          id="nsfw"
          name="nsfw"
          tabindex="4"
        >
        <label class="form-check-label" for="nsfw">
          nsfw
        </label>
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="active"
      class="col-sm-2 col-form-label"
    >
      ACTIVE
    </label>
    <div class="col-sm-10">
      <div class="form-check">
        <input
          class="form-check-input"
          type="checkbox"
          {{ $comment->active ? 'checked' : '' }}
          id="active"
          name="active"
          tabindex="5"
        >
        <label class="form-check-label" for="active">
          active
        </label>
      </div>
    </div>
  </div>
  <div class="form-group row">
    <label for="meta"
      class="col-sm-2 col-form-label"
    >
      META
    </label>
    <div class="col-sm-10">
      @if ($comment->meta)
        @foreach ($comment->meta as $key => $value)
          {{ $key }} : {{ $value }}
        @endforeach
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="ip"
      class="col-sm-2 col-form-label"
    >
      IP
    </label>
    <div class="col-sm-10">
      {{ $comment->ip }}
    </div>
  </div>
  <div class="form-group row">
    <label for="hash_ip"
      class="col-sm-2 col-form-label"
    >
      HASH_IP
    </label>
    <div class="col-sm-10">
      {{ $comment->hash_ip }}
    </div>
  </div>
  <div class="form-group row">
    <label for="created_at"
      class="col-sm-2 col-form-label"
    >
      CREATED_AT
    </label>
    <div class="col-sm-10">
      {{ $comment->created_at }}
    </div>
  </div>
  <div class="form-group row">
    <label for="updated_at"
      class="col-sm-2 col-form-label"
    >
      UPDATED_AT
    </label>
    <div class="col-sm-10">
      {{ $comment->updated_at }}
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-2 "></div>
    <div class="col-sm-10">
      <button
        type="submit"
        class="btn btn-primary"
        tabindex="7"
      >
        Submit
      </button>
    </div>

</form>


@endsection
