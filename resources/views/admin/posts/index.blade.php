@extends('admin.layouts.app')

@section('content')

  <h1 class="h2 m-3">
    @lang('admin.post.header')
  </h1>

  <!-- will be used to show any messages -->
  @if (\Session::has('message'))
      <div class="alert alert-info">{{ \Session::get('message') }}</div>
  @endif

  <div class="table-responsive mb-5">
    <table class="table table-striped table-sm admin-table" id="posts-table">
      <thead>
        <tr>
          <th>ID</th>
          <th>CATEGORY</th>
          <th>USER</th>
          <th>TITLE</th>
          <th>BODY</th>
          <th>THUMBNAIL</th>
          <th>ACTIVE</th>
          <th>IP</th>
          <th>HASH IP</th>
          <th>STATUS</th>
          <th>CREATED AT</th>
          <th>ACTION</th>
        </tr>
      </thead>
    </table>
  </div>

@endsection

@push('script-datatables')
<script>
$(function() {
    $('#posts-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin-posts-datatables') }}',
        columns: [
            { data: 'id', name: 'id', searchable: false  },
            { data: 'category_title', name: 'category_title', searchable: false  },
            { data: 'user', name: 'user', searchable: false  },
            { data: 'title', name: 'title', searchable: true  },
            { data: 'body', name: 'body', searchable: true  },
            {
              targets : 5,
              data: 'thumbnail',
              name: 'thumbnail',
              searchable: false,
              render: function(data, type, row, meta) {
                if (data !== null) {
                  return `<img src="${data}" width="100">`;
                } else {
                  return null;
                }
              }
            },
            { data: 'active', name: 'active', searchable: false },
            { data: 'ip', name: 'ip' },
            {
              data: 'hash_ip',
              name: 'hash_ip',
              render: function(data, type, row, meta) {
                var color = colorFromStirng(data);
                return `<span style="font-weight: 900;color:${color};">${data}</span>`;
              }
            },
            { data: 'status', name: 'status', searchable: false },
            { data: 'created_at', name: 'created_at', searchable: false },
            { data: 'action', name: 'action', width: '10%', orderable: false, searchable: false }
        ]
    });
});
</script>
@endpush
