@extends('admin.layouts.app')

@section('content')

  <h1 class="h2 m-3">
    @lang('admin.category.header')
  </h1>

  <!-- will be used to show any messages -->
  @if (\Session::has('message'))
      <div class="alert alert-info">{{ \Session::get('message') }}</div>
  @endif

  <div class="row">
    <div class="col-6">
      {{--  --}}
    </div>
    <div class="col-6 text-right">
      <a href="{{ route('admin-categories-create') }}" class="btn btn-danger text-white">
        @lang('admin.category.create')
      </a>
      <a href="{{ route('admin-categories-create', ['sub-category' => true]) }}" class="btn btn-primary text-white">
        @lang('admin.category.create-sub')
      </a>
    </div>
  </div>

  <div class="table-responsive mb-5 mt-3">
    <table class="table table-striped table-sm admin-table" id="categories-table">
      <thead>
        <tr>
          <th>ID</th>
          <th>PARENT</th>
          <th>TITLE</th>
          <th>BODY</th>
          <th>SLUG</th>
          <th>COVER</th>
          <th>ACTIVE</th>
          <th>UPDATED AT</th>
          <th>DELETED AT</th>
          <th>ACTION</th>
        </tr>
      </thead>
    </table>
  </div>

@endsection

@push('script-datatables')
<script>
$(function() {
    $('#categories-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('admin-categories-datatables') }}',
        columns: [
            { data: 'id', name: 'id', searchable: false },
            { data: 'parent', name: 'parent', searchable: true },
            { data: 'title', name: 'title', searchable: true },
            { data: 'body', name: 'body', searchable: true },
            { data: 'slug', name: 'slug', searchable: true },
            {
              data: 'cover',
              name: 'cover',
              searchable: false,
              render: function(data, type, row, meta) {
                if (data !== null) {
                  return `<img src="${data}" height="80">`;
                } else {
                  return null;
                }
              }
            },
            { data: 'active', name: 'active', searchable: false },
            { data: 'updated_at', name: 'updated_at', searchable: false },
            { data: 'deleted_at', name: 'deleted_at', searchable: false },
            { data: 'action', name: 'action', width: '10%', orderable: false, searchable: false }
        ]
    });
});
</script>
@endpush
