@extends('admin.layouts.app')

@section('content')

  <h1 class="h2 m-3">
    @lang('admin.category.header-create-parent')
  </h1>

  <!-- will be used to show any messages -->
  @if (\Session::has('message'))
      <div class="alert alert-info">{{ \Session::get('message') }}</div>
  @endif

  @if ($errors->any())
    <div class="alert alert-danger">
      @lang('admin.errors.waring')
    </div>
  @endif

<form
  method="post"
  action="{{ route('admin-categories-store') }}"
>
  {{ csrf_field() }}

  <div class="form-group row">
    <label for="title"
      class="col-sm-2 col-form-label"
    >
      TITLE
    </label>
    <div class="col-sm-10">
      <input
        type="text"
        class="form-control @if ($errors->has('title')) is-invalid @endif"
        id="title"
        name="title"
        placeholder="TITLE"
        value="{{ old('title') }}"
        tabindex="1"
      >
      @if ($errors->has('title'))
        <div class="small text-danger">
          {{ $errors->first('title') }}
        </div>
      @endif
    </div>
  </div>

  <div class="form-group row">
    <label for="slug"
      class="col-sm-2 col-form-label"
    >
      SLUG
    </label>
    <div class="col-sm-10">
      <input
        type="text"
        class="form-control @if ($errors->has('slug')) is-invalid @endif"
        id="slug"
        name="slug"
        placeholder="SLUG"
        value="{{ old('slug') }}"
        tabindex="2"
      >
      @if ($errors->has('slug'))
        <div class="small text-danger">
          {{ $errors->first('slug') }}
        </div>
      @endif
    </div>
  </div>

  <div class="form-group row">
    <label for="body"
      class="col-sm-2 col-form-label"
    >
      BODY
    </label>
    <div class="col-sm-10">
      <textarea
        class="form-control @if ($errors->has('body')) is-invalid @endif"
        id="body"
        name="body"
        tabindex="3"
        >{{ old('body') }}</textarea>
      @if ($errors->has('body'))
        <div class="small text-danger">
          {{ $errors->first('body') }}
        </div>
      @endif
    </div>
  </div>

  <div class="form-group row">
    <label for="active"
      class="col-sm-2 col-form-label"
    >
      ACTIVE
    </label>
    <div class="col-sm-10">
      <div class="form-check">
        <input
          class="form-check-input"
          type="checkbox"
          checked
          id="active"
          name="active"
          tabindex="4"
        >
        <label class="form-check-label" for="active">
          ACTIVE
        </label>
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-2 "></div>
    <div class="col-sm-10">
      <button
        type="submit"
        class="btn btn-primary"
        tabindex="5"
      >
        Submit
      </button>
    </div>

</form>


@endsection
