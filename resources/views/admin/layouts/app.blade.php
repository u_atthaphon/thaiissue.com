<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="client-token" content="{{ client_token() }}">
  {!! SEO::generate() !!}

  <link href="{{ asset('css/app.css?v0.5') }}" rel="stylesheet">

  @include('admin.includes.datatable-resources-css')

</head>
<body id="admin">
  @include('admin.includes.navbar')

  <div id="app"></div>

  <div class="container-fluid mt-5 h-100">
    <div class="row h-100">
      @include('admin.includes.sidebar')
      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-0">
        @yield('content')
      </main>
    </div>
  </div>

  <!-- DataTables -->
  <script src="/js/llv-{{ \App::getLocale() }}.js"></script>
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/scripts.js') }}"></script>
  @include('admin.includes.datatable-resources-js')
  @stack('script-datatables')
</body>

</html>
