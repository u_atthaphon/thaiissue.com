<nav class="col-md-2 d-none d-md-block bg-light sidebar pt-3 h-100">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" href="{{ route('admin-dashboard-index') }}">
          <i class="fa fa-gamepad mr-2"></i>
          @lang('admin.dashboard')
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin-posts-index') }}">
          <i class="fa fa-plus mr-2"></i>
          @lang('admin.posts')
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin-comments-index') }}">
          <i class="fa fa-comment mr-2"></i>
          @lang('admin.comments')
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin-categories-index') }}">
          <i class="fa fa-list mr-2"></i>
          @lang('admin.categories')
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('admin-bans-index') }}">
          <i class="fa fa-ban mr-2"></i>
          @lang('admin.ban-ip')
        </a>
      </li>
    </ul>
  </div>
</nav>
