<nav class="navbar navbar-expand-md navbar-thaiissue">
  <a class="navbar-brand" href="{{ url('/') }}">
    <img src="/logo.png"
    height="32"
    class="d-inline-block align-top"
    alt="@lang('layouts.brand')">
  </a>

  <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fa fa-ellipsis-v"></i>
  </button>

  <div class="navbar-collapse collapse bg-white" id="navbarCollapse" style="">
    <div class="navbar-nav mr-auto">
      {{-- menu --}}
    </div>
    <div class="navbar-right  my-2 my-lg-0">
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"
          >
            @lang('navbar.logout')
          </a>
          <form
            id="logout-form"
            action="{{ route('logout') }}"
            method="POST"
            style="display: none;"
          >
              {{ csrf_field() }}
          </form>
        </li>
      </ul>
    </div>
  </div>
</nav>
