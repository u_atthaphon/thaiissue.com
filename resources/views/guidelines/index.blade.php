@extends('layouts.app')

@section('content')
<div style="height:1px;"></div>
<div class="container">
  <div class="grid-card">
    <h5>
      @lang('guideline.header')
    </h5>
    <p>
      @lang('guideline.body')
    </p>
  </div>

  <div class="guidelines">
    <div class="grid-card">
      <div class="row">
        <div class="col-12 col-md-4">
          <div class="board">
            <h6>
              @lang('guideline.board')
            </h6>
            <p>
              <ul>
                @foreach ($guidelines as $title => $guideline)
                  <li>
                    <a href="#{{ $title }}" class="hashtag-jump">
                      {{ $title }}
                    </a>
                  </li>
                @endforeach
              </ul>
            </p>
          </div>
        </div> <!-- col-12 col-md-4 -->
        <div class="col-12 col-md-8">
          <div class="detials">
            @foreach ($guidelines as $title => $guideline)
              <h5 id="{{ $title }}">
                {{ $title }}
              </h5>
              <p>
                <ol>
                    @foreach ($guideline as $text)
                      <li>
                        {!! $text !!}
                      </li>
                    @endforeach
                  </ol>
              </p>
            @endforeach
          </div>
        </div> <!-- col-12 col-md-8 -->
      </div>
    </div>
  </div>
</div>
@endsection
