@inject('str', 'Illuminate\Support\Str')
<?php
  $uuid = $str->uuid();
?>

<div class="container">
  <div class="grid-card">
    <div class="create-comment" id="commentTitle">
      <h4>
        @lang('comment.header')
      </h4>
      <form
      role="form"
      method="POST"
      action="{{ route('comments-store') }}"
      enctype="multipart/form-data"
      id="comment-box">
        {{ csrf_field() }}
        <fieldset>
          <div class="form-group">

            @if ($errors->has('cannot-create'))
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ $errors->first('cannot-create') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            <input type="hidden" name="categoryId" value="{{ $category->id }}">
            <input type="hidden" name="postId" value="{{ $postId }}">
            <div class="csp">
              <label for="checkname">
                Name:
              </label>
              <br>
              <input
              class="form-control"
              type="text"
              id="checkname"
              name="checkname">
            </div>
            <label class="mt-3" for="comment-body">
              @lang('comment.body')
            </label>
            <br>
            <textarea
            class="form-control @if ($errors->has('body')) is-invalid @endif"
            id="body"
            name="body"
            rows="3">{{ trim(old('body')) }}</textarea>
            @if ($errors->has('body'))
              <div class="small text-danger">
                {{ $errors->first('body') }}
              </div>
            @endif

            <input-image-component
              is-error="{{ $errors->has('image') }}"
              error-msg="{{ $errors->first('image') }}"
            ></input-image-component>

            <button
            type="submit"
            class="btn btn-primary mt-3">
              @lang('comment.comment')
            </button>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>


@push('pageScript')
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var el = document.getElementById("comment-box");
      el.addEventListener("submit", function(e) {
        var formFieldset = el.querySelector('fieldset');
        formFieldset.className = 'disabled';
        var loader = document.createElement("i");
        loader.className = 'processing fa fa-spinner fa-pulse fa-3x fa-fw';
        formFieldset.appendChild(loader);
      });
    }, false);
  </script>
@endpush
