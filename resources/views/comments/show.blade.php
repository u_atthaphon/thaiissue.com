@extends('layouts.app')

@section('content')

<div class="grid-post-page">
  <div class="container">
      <span class="shelves-post">
        @shelvesMultipleLine(5)
      </span>

      <post-by-id-component
        :post-id="{{ $postId }}"
        auth="{{ \Auth::check()?? false }}"
      ></post-by-id-component>
  </div>

  <div class="container">
      <span class="shelves-comment">
        @shelvesWithLeftImage(1)
      </span>

      <comment-by-post-id-and-order-no-component
        :comment-order-no="{{ $commentOrderNo }}"
        :post-id="{{ $postId }}"
        auth="{{ \Auth::check()?? false }}"
      ></comment-by-post-id-and-order-no-component>
  </div>

  <div class="container grid-back-to-post">
    <a href="{{ route('posts-show', [
      'post_id' => $postId,
      'category_slug' => $category->slug,
    ]) }}">
      <div class="grid-card">
        @lang('comment.back-to-post')
      </div>
    </a>
  </div>
</div>
@endsection

