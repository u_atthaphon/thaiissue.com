@extends('layouts.app')

@section('content')

@inject('str', 'Illuminate\Support\Str')
<?php
  $uuid = $str->uuid();
?>

<div class="grid-post-create-page">
  <div class="container">
    <div class="grid-card">
      <div class="create-post">
        <h2>
          @lang('post.post-header')
        </h2>
        @lang('post.post-hint-title', [
          'title' => $category->title,
          'body' => $category->body,
        ])
        @lang('post.post-hint-body', [
          'ethice-url' => route('ethices-index')
        ])
        <form
        role="form"
        method="POST"
        action="{{ route('posts-store') }}"
        class="post-box"
        enctype="multipart/form-data"
        id="category-{{ $uuid }}">
          {{ csrf_field() }}
          <fieldset>
            <div class="form-group">
              @if ($errors->has('cannot-create'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  {{ $errors->first('cannot-create') }}

                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              @endif

              <input type="hidden" name="categoryId" value="{{ $category->id }}">
              <div class="csp">
                <label for="checkname">
                  Name:
                </label>
                <br>
                <input
                class="form-control"
                type="text"
                id="checkname"
                name="checkname">
              </div>
              <label for="post-title">
                @lang('post.post-title')
              </label>
              <br>
              <input
              class="form-control @if ($errors->has('postTitle')) is-invalid @endif"
              type="text"
              id="postTitle"
              name="postTitle"
              value="{{ trim(old('postTitle')) }}">
              @if ($errors->has('postTitle'))
                <div class="small text-danger">
                  {{ $errors->first('postTitle') }}
                </div>
              @endif
              <br>
              <label class="mt-3" for="post-body">
                @lang('post.post-body')
              </label>
              <br>
              <textarea
              class="form-control @if ($errors->has('postBody')) is-invalid @endif"
              id="postBody"
              name="postBody"
              rows="3">{{ trim(old('postBody')) }}</textarea>
              @if ($errors->has('postBody'))
                <div class="small text-danger">
                  {{ $errors->first('postBody') }}
                </div>
              @endif

              <div class="preview-post-img-wrapper d-none">
                <img
                class="preview-post-img"
                id="preview-post-img-{{ $uuid }}"/>
              </div>


              <input-image-component
                is-error="{{ $errors->has('image') }}"
                error-msg="{{ $errors->first('image') }}"
              ></input-image-component>
              <button
              type="submit"
              class="btn btn-primary mt-3">
                @lang('post.post')
              </button>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@push('pageScript')
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var el = document.getElementById("category-{{ $uuid }}");
      el.addEventListener("submit", function(e) {
        var formFieldset = el.querySelector('fieldset');
        formFieldset.className = 'disabled';
        var loader = document.createElement("i");
        loader.className = 'processing fa fa-spinner fa-pulse fa-3x fa-fw';
        formFieldset.appendChild(loader);
      });
    }, false);
  </script>
@endpush
