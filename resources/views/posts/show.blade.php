@extends('layouts.app')

@section('content')

<div class="grid-post-page">
  <div class="container">
      <span class="shelves-post">
        @shelvesMultipleLine(4)
      </span>
      <post-by-id-component
        :post-id="{{ $postId }}"
        auth="{{ \Auth::check()?? false }}"
      ></post-by-id-component>
  </div>

  <div class="container">
    <span class="shelves-comments">
      @shelvesWithLeftImage(10)
    </span>

    <comment-list-by-post-id-component
      :post-id="{{ $postId }}"
      auth="{{ \Auth::check()?? false }}"
    ></comment-list-by-post-id-component>
  </div>

  <div id="grid-post-create-page" style="display: none;">
    @include('comments.includes.create-comment', [
      'postId' => $postId
    ])
  </div>
</div>
@endsection

