@extends('layouts.app')

@inject('providerEnum', 'Thaiissue\Enums\Provider')

@section('content')
  <div style="height:1px;"></div>
    <div class="container">
      <div class="grid-card" id="favorites-list">
        <div class="row row-header">
          <div class="col-12">
            <h4>
              <i class="fa fa-heart mr-2"></i>
              @lang('user.favorite')
            </h4>
          </div>
        </div>

        @include('users.includes.favorite-menu')
      </div> <!-- #favorites-posts -->

      <div class="row row-header">
        <div class="col-12" id="favorites-list">
          @shelvesNavPill(2)
          @shelvesWithImage(10)
          <user-favorite-comment-list-component
            auth="{{ \Auth::check()?? false }}"
          >
          </user-favorite-comment-list-component>
        </div>
      </div>
    </div>
  </div>
@endsection
