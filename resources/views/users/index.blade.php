@extends('layouts.app')

@section('content')
  <div style="height:1px;"></div>
    <div class="container">
      <div class="grid-card" id="me">
        <div class="row row-header">
          <div class="col-12 col-md-6">
            <div class="media">
              <img
                class="mr-3 avatar"
                src="{{ \Auth::user()->avatar }}"
                alt="{{ \Auth::user()->name }}"
                widht="52"
                height="52"
              >
              <div class="media-body">
                <h4 class="m-0">
                  {{ \Auth::user()->name }}
                </h4>
                {{ \Auth::user()->email }}
              </div>
            </div>
          </div>
          <div class="col-12 col-md-6">
            <hr class="d-block d-md-none my-4">
            @if (\Auth::check() && is_admin(\Auth::user()))
              <h4>
                <i class="fa fa-glass"></i>
                @lang('user.moderator')
              </h4>
            @endif
            <hr class="d-block d-md-none my-4">
          </div>
        </div>

        <div class="row mt-5">
          <div class="col-12">
            <h2>setting</h2>
            <ul class="nav nav-pills my-3" id="pills-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">
                  <i class="fa fa-favorite"></i>
                  @lang('user.favorite')
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">
                  <i class="fa fa-user-circle mr-3"></i>
                  @lang('user.profile')
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
              </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
              <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                test1
              </div>
              <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                test2
              </div>
              <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                test3
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
