@extends('layouts.app')

@inject('providerEnum', 'Thaiissue\Enums\Provider')

@section('content')
  <div style="height:1px;"></div>
    <div class="container">
      <div class="grid-card" id="favorites-list">
        <div class="row row-header">
          <div class="col-12">
            <h4>
              <i class="fa fa-history mr-2"></i>
              @lang('user.history')
            </h4>
          </div>
        </div>

        @include('users.includes.history-menu')
      </div> <!-- #favorites-posts -->

      <div class="row row-header">
        <div class="col-12" id="favorites-list">
          @shelvesNavPill(2)
          @shelvesWithImage(10)
          <user-history-comment-list-component
            auth="{{ \Auth::check()?? false }}"
          >
          </user-history-comment-list-component>
        </div>
      </div>
    </div>
  </div>
@endsection
