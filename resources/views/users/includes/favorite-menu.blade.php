<?php
  $currRtouePrefix = Route::getCurrentRoute()->getPrefix();
  $currRouteName = Route::currentRouteName();
?>

<ul class="nav nav-pills nav-fill">
  <li
    class="nav-item"
  >
    <a
      href="{{ route('users-favorites-posts') }}"
      class="nav-link {{ ($currRouteName == 'users-favorites-posts')? 'active' : '' }}"
    >
      @lang('user.favorite-post')
    </a>
  </li>
  <li
    class="nav-item"
  >
    <a
      href="{{ route('users-favorites-comments') }}"
      class="nav-link {{ ($currRouteName == 'users-favorites-comments')? 'active' : '' }}"
    >
      @lang('user.favorite-comment')
    </a>
  </li>
</ul>
