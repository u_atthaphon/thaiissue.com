<div class="grid-card" id="user-password">
  <div class="row row-header">
    <div class="col-12">
      <h5>
        @lang('user.password')
      </h5>
      <form>
        <div class="form-group row">
          <label
            for="oldPassword"
            class="col-12 col-md-2 col-form-label"
          >
            @lang('user.old-password')
          </label>
          <div class="col-12 col-md-10">
            <input
              type="password"
              class="form-control"
              id="oldPassword"
              placeholder="@lang('user.old-password')"
            >
          </div>
        </div>
        <div class="form-group row">
          <label
            for="newPassword"
            class="col-12 col-md-2 col-form-label"
          >
            @lang('user.new-password')
          </label>
          <div class="col-12 col-md-10">
            <input
              type="password"
              class="form-control"
              id="newPassword"
              placeholder="@lang('user.new-password')"
            >
          </div>
        </div>
        <div class="form-group row">
          <label
            for="confirmPassword"
            class="col-12 col-md-2 col-form-label"
          >
            @lang('user.confirm-password')
          </label>
          <div class="col-12 col-md-10">
            <input
              type="password"
              class="form-control"
              id="confirmPassword"
              placeholder="@lang('user.confirm-password')"
            >
          </div>
        </div>
        <div class="form-group row">
          <div class="col-12 offset-md-2 col-md-12">
            <button
              type="submit"
              class="btn btn-primary mb-2"
            >
              Confirm identity
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div> <!-- #user-password -->
