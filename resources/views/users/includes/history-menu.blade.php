<?php
  $currRtouePrefix = Route::getCurrentRoute()->getPrefix();
  $currRouteName = Route::currentRouteName();
?>

<ul class="nav nav-pills nav-fill">
  <li
    class="nav-item"
  >
    <a
      href="{{ route('users-history-posts') }}"
      class="nav-link {{ ($currRouteName == 'users-history-posts')? 'active' : '' }}"
    >
      @lang('user.history-post')
    </a>
  </li>
  <li
    class="nav-item"
  >
    <a
      href="{{ route('users-history-comments') }}"
      class="nav-link {{ ($currRouteName == 'users-history-comments')? 'active' : '' }}"
    >
      @lang('user.history-comment')
    </a>
  </li>
</ul>
