<?php
// Route::get('clients/is-authenticated', function () {
//     return response()->json(['isOk' => true]);
// });

Route::resource('categories', 'CategoryController', [
    'except' => [
        'show',
        'create', 'store',
        'edit', 'update',
        'delete', 'destroy'
    ],
]);

Route::prefix('categories')->group(function () {
    Route::get('{category_id}/posts-paginate', 'CategoryController@showPostsPaginateByCategoryId')
        ->name('categories-posts-paginate')
        ->where(['category_id' => '[0-9]+']);

    Route::get('{category_id}/comments-paginate', 'CategoryController@showCommentsPaginateByCategoryId')
        ->name('categories-comments-paginate')
        ->where(['category_id' => '[0-9]+']);
});

Route::resource('posts', 'PostController', [
    'except' => [
        'index',
        'create', 'store',
        'edit', 'update',
        'delete', 'destroy'
    ],
]);

Route::prefix('posts')->group(function () {
    Route::get(
        '{post_id}/comments-paginate',
        'PostController@showCommentsPaginateByPostId'
    )
        ->name('posts-comments-paginate')
        ->where(['category_id' => '[0-9]+']);
});

Route::resource('comments', 'CommentController', [
    'except' => [
        'index', 'show',
        'create', 'store',
        'edit', 'update',
        'delete', 'destroy',
    ],
]);

Route::prefix('comments')->group(function () {
    Route::get(
        '{comment_order_no}/posts/{post_id}',
        'CommentController@byCommentOrderNoAndPostId'
    )
        ->name('show-comments-order-no-posts-id')
        ->where([
            'comment_order_no' => '[0-9]+',
            'post_id' => '[0-9]+',
        ])->middleware('api');
});

Route::prefix('favorites')->group(function () {
    Route::post('posts/{post_id}', 'FavoriteController@toggleFavoritePost')
        ->name('toggle-favorites-posts-id')
        ->where([
            'post_id' => '[0-9]+',
        ]);

    Route::post('comments/{comment_id}', 'FavoriteController@toggleFavoriteComment')
        ->name('toggle-favorites-comments-id')
        ->where([
            'comment_id' => '[0-9]+',
        ]);
});

Route::prefix('users')->group(function () {
    Route::post('reset-password', 'UserController@resetPassword')
        ->name('users-history');
    Route::get('favorites-posts', 'UserController@getFevoritePostPaginate')
        ->name('users-favorites-posts');
    Route::get('favorites-comments', 'UserController@getFevoriteCommentPaginate')
        ->name('users-favorites-comments');
    Route::get('history-posts', 'UserController@getHistoryPostPaginate')
        ->name('users-history-posts');
    Route::get('history-comments', 'UserController@getHistoryCommentPaginate')
        ->name('users-history-comments');
});
