<?php

// use Thaiissue\Models\Post;

// Route::get('favorite', function () {
    // $post = Post::find(50);
    // $post->addFavorite();
    // // $post->delFavorite();
// });

// Route::get('test', function () {
//     throw new \Exception("Error Processing Request", 1);

//     return 'here';
//     dd(\Passport::all());
// });

// Route::get('send-mail', function () {
//     $to      = 'u.atthaphon@gmail.com';
//     $subject = 'the subject';
//     $message = 'hello';
//     $headers = 'From: webmaster@example.com' . "\r\n" .
//     'Reply-To: webmaster@example.com' . "\r\n" .
//     'X-Mailer: PHP/' . phpversion();

//     mail($to, $subject, $message, $headers);
// });

Route::resource('/', 'HomeController')->only('index');

Route::prefix('categories')->group(function () {
    Route::get('{slug}', 'CategoryController@show')
        ->name('categories-show')
        ->where(['slug' => '[A-Za-z\_\-]+']);

    Route::get('{slug}/comments', 'CategoryController@showComments')
        ->name('categories-show-comments')
        ->where(['slug' => '[A-Za-z\_\-]+']);

    // Route::get('{slug}/links', 'CategoryController@showLinks')
    //     ->name('categories-show-links')
    //     ->where(['slug' => '[A-Za-z\_\-]+']);

    Route::get('{slug}/create-post', 'CategoryController@createPost')
        ->name('categories-create-post')
        ->where(['slug' => '[A-Za-z\_\-]+']);
});

Route::prefix('posts')->group(function () {
    Route::get('{id}/categories/{slug}', 'PostController@show')
        ->name('posts-show')
        ->where([
            'post_id' => '[0-9]+',
            'category_slug' => '[A-Za-z\_\-]+',
        ]);
    Route::post('store', 'PostController@store')->name('posts-store');
});

Route::prefix('comments')->group(function () {
    Route::get(
        '{comment_order_no}/post/{id}/categories/{slug}',
        'CommentController@show'
    )
        ->name('comments-show')
        ->where([
            'comment_order_no' => '[0-9]+',
            'post_id' => '[0-9]+',
            'category_slug' => '[A-Za-z\_\-]+',
        ]);
    Route::get(
        '{comment_order_no}/post/{id}',
        'CommentController@showWithoutCategorySlug'
    )
        ->name('comments-show-without-category-slug')
        ->where([
            'comment_order_no' => '[0-9]+',
            'post_id' => '[0-9]+',
        ]);
    Route::post('store', 'CommentController@store')->name('comments-store');
});

Route::prefix('ethices')->group(function () {
    Route::get('/', 'EthiceController@index')
        ->name('ethices-index');
});

Route::prefix('guidelines')->group(function () {
    Route::get('/', 'GuidelineController@index')
        ->name('guidelines-index');
});

Route::prefix('auth')->group(function () {
    Route::get('{provider}', 'SocialiteController@redirectToProvider')
        ->where(['provider' => '[A-Za-z\_\-]+']);
    Route::get('{provider}/callback', 'SocialiteController@handleProviderCallback')
        ->where(['provider' => '[A-Za-z\_\-]+']);
});

Route::get('sitemap.xml', 'SitemapController@index');
Route::get('sitemap-post.xml', 'SitemapController@indexPost');
Route::get('sitemap-comment.xml', 'SitemapController@indexComment');
