<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('test', function () {
    // dd(\Carbon\Carbon::parse('2018-09-13T10:35:24Z', 'UTC'));
    // $url1 = 'http://php.net';
    // $url2 = 'https://www.google.co.th/search?q=php+url+string&oq=php+url+string&aqs=chrome..69i57j69i64.7602j0j4&sourceid=chrome&ie=UTF-8';
    // $arr = [];
    // parse_str(parse_url($url2)['query'], $arr);
    // dd($arr);
    // dd(parse_url($url1), parse_url($url2));
//     return \Newsapi::fetchBusiness();
// });
