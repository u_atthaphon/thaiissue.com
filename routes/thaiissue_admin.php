<?php

Route::prefix('dashboard')->group(function () {
    Route::get('/', 'DashboardController@index')
        ->name('admin-dashboard-index');
});

Route::prefix('posts')->group(function () {
    Route::get('posts-datatables', 'PostController@postDatatables')
        ->name('admin-posts-datatables');
    Route::get('/', 'PostController@index')
        ->name('admin-posts-index');
    Route::get('edit/{post_id}', 'PostController@edit')
        ->name('admin-posts-edit');
    Route::patch('update/{post_id}', 'PostController@update')
        ->name('admin-posts-update');
    Route::delete('delete/{post_id}', 'PostController@destroy')
        ->name('admin-posts-destroy');
    Route::patch('restore/{post_id}', 'PostController@restore')
        ->name('admin-posts-restore');
});

Route::prefix('comments')->group(function () {
    Route::get('comments-datatables', 'CommentController@commentDatatables')
        ->name('admin-comments-datatables');
    Route::get('/', 'CommentController@index')
        ->name('admin-comments-index');
    Route::get('edit/{post_id}', 'CommentController@edit')
        ->name('admin-comments-edit');
    Route::patch('update/{post_id}', 'CommentController@update')
        ->name('admin-comments-update');
    Route::delete('delete/{post_id}', 'CommentController@destroy')
        ->name('admin-comments-destroy');
    Route::patch('restore/{post_id}', 'CommentController@restore')
        ->name('admin-comments-restore');
});

Route::prefix('bans')->group(function () {
    Route::get('bans-datatables', 'BanController@banDatatables')
        ->name('admin-bans-datatables');
    Route::get('/', 'BanController@index')
        ->name('admin-bans-index');
    Route::get('create', 'BanController@create')
        ->name('admin-bans-create');
    Route::post('store', 'BanController@store')
        ->name('admin-bans-store');
    Route::get('edit/{ban_id}', 'BanController@edit')
        ->name('admin-bans-edit');
    Route::patch('update/{ban_id}', 'BanController@update')
        ->name('admin-bans-update');
    Route::delete('delete/{ban_id}', 'BanController@destroy')
        ->name('admin-bans-destroy');
    Route::patch('restore/{post_id}', 'BanController@restore')
        ->name('admin-bans-restore');
});

Route::prefix('categories')->group(function () {
    Route::get('categories-datatables', 'CategoryController@categoryDatatables')
        ->name('admin-categories-datatables');
    Route::get('/', 'CategoryController@index')
        ->name('admin-categories-index');
    Route::get('create', 'CategoryController@create')
        ->name('admin-categories-create');
    Route::post('store', 'CategoryController@store')
        ->name('admin-categories-store');
    Route::post('store-sub', 'CategoryController@storeSub')
        ->name('admin-categories-store-sub');
    Route::get('edit/{category_id}', 'CategoryController@edit')
        ->name('admin-categories-edit');
    Route::patch('update/{category_id}', 'CategoryController@update')
        ->name('admin-categories-update');
    Route::delete('delete/{category_id}', 'CategoryController@destroy')
        ->name('admin-categories-destroy');
    Route::patch('restore/{category_id}', 'CategoryController@restore')
        ->name('admin-categories-restore');
});
