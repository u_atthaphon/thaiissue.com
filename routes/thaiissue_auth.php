<?php

Route::prefix('users')->group(function () {
    Route::get('me', 'UserController@me')
        ->name('users-me');
    Route::get('history', 'UserController@history')
        ->name('users-history');

    Route::prefix('favorites')->group(function () {
        Route::get('posts', 'UserController@favoritePosts')
            ->name('users-favorites-posts');
        Route::get('comments', 'UserController@favoriteComments')
            ->name('users-favorites-comments');
    });

    Route::prefix('history')->group(function () {
        Route::get('posts', 'UserController@historyPosts')
            ->name('users-history-posts');
        Route::get('comments', 'UserController@historyComments')
            ->name('users-history-comments');
    });
});
